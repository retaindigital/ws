��    :      �  O   �      �     �               '     5     A     R     h     q     w     �     �     �  8   �     �     �       q        �     �     �     �      �     �  
   �  
                        0     3     6     S     `  
   i     t     z  
   �     �  
   �     �     �     �  
   �     �     �     
          "     +     8     >     F     R  	   [  	   e  
   o  g  z     �
     �
     �
          #     /     @  	   W     a     r     �     �     �  7   �  	   �     �     �  �     
   �  "   �  	                  /  
   C  
   N     Y     ]  !   e     �     �  !   �     �     �     �     �     �     �     �                 
   '  	   2  %   <     b     q     }     �     �     �     �     �  
   �     �     �     �     ,          "   $                    
      -   &         4      )   .   *            3   7   9       %                 5                      0   +                 8   1   6       !             (   #   	          /                     2      :                        '                 All categories Apply coupon Back To Home Page Back to login Categories: Categories: %1$s Category: Categories: Checkout Clear Clear filters Continue Shopping Continue reading Continue reading %s Continue reading %s <span class="meta-nav">&rarr;</span> Coupon Coupon code Create an account Create an account to expedite future checkouts, track order history & receive emails, discounts, & special offers Home I am interested in... Instant search ... Login Login successful, redirecting... Lost your password? My Account My account New Newness No products in the cart. OR Or Our Shipping & Return Policy Out Of Stock Password Popularity Price Price: high Price: low Product QTY : %1$s Quantity Remember me Return to the Dashboard Search ... Ship to a different address? Shipping Details Sold out Start shopping Subtotal Tell me more Total Total:  Update cart Username View Cart Your Cart Your order Project-Id-Version: Wilkinson Sword Germany
Report-Msgid-Bugs-To: https://github.com/woocommerce/woocommerce/issues
PO-Revision-Date: 2020-06-17 13:39+0100
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ../../themes
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;esc_html_e;esc_html__;esc_html;esc_attr;esc_attr__;esc_attr_e;_n
X-Poedit-SearchPath-0: wilkinson-sword
X-Poedit-SearchPath-1: lebe
X-Poedit-SearchPathExcluded-0: wilkinson-sword/node_modules
 Blogs Anwenden Jetzt unsere Produkte entdecken Zurück zum Login Kategorien: Kategorien: %1$s Kategorie: Kategorien: Zur Kasse Auswahl aufheben Filter löschen Weiter Shoppen Weiterlesen Weiterlesen %s Weiterlesen %s <span class=“meta-nav”>&rarr;</span> Gutschein Gutscheincode Erstelle ein Konto Erstelle ein Konto um Deinen nächsten Kauf zu beschleunigen, den Bestellsatus von vergangenen Bestellungen zu überprüfen, E-Mails zu empfangen und Rabatte sowie spezielle Angebote nicht zu verpassen Startseite Jetzt dein Lieblingsthema wählen: Suche … Anmelden Login erfolgreich… Passwort vergessen? Mein Konto Mein Konto Neu Neuheit Noch keine Produkte im Warenkorb. oder oder Unsere Versand & Rückgabe Policy Ausverkauft Passwort Beliebtheit Preis Preis absteigend Preis aufsteigend Artikel Menge : %1$s Menge Kennung merken Übersicht Suche … Zu einer anderen Adresse verschicken? Versanddetails Ausverkauft Zum Shop Zwischensumme Erzähl mir mehr Gesamtsumme Gesamtsumme:  Warenkorb aktualisieren Nutzername Zum Warenkorb Dein Warenkorb Deine Bestellung 