��          �      \      �  F   �  ?     g   X  W   �          '     9  $   G  
   l     w  A   �     �     �     �     �     �          '     7  #  R  d   v  F   �  �   "  L   �     �      �          /     F     V  7   s     �     �  	   �     �     �     �     	  #   	               	                                                                                
    %1$sI confirm that I'm over 18 years of age %2$sWhy's this?</a></span> <i class="fad fa-check-square theme-ws-ok"></i> Order Confirmed <i class="fad fa-envelope theme-ws-ok"></i> We have emailed you an <strong>order confirmation.</strong> A product in your cart is restricted to customers aged 18+, please confirm you are 18+. All categories Back To Home Page Clear filters Click the link below to return home. Contact Us I am interested in... If you've got any queries, please don't hesitate to get in touch! Pages Payment Methods Posts Product Categories Products Thank you for ordering with us. Transaction ID: We couldn't find that page Project-Id-Version: 
Report-Msgid-Bugs-To: https://github.com/woocommerce/woocommerce/issues
PO-Revision-Date: 2020-06-17 14:43+0100
Last-Translator: 
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.4
X-Poedit-Basepath: ../../themes/wilkinson-sword
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;esc_html_e;esc_html__;esc_html;esc_attr;esc_attr__;esc_attr_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: node_modules
 %1$sIch bestätige, dass ich über 18 Jahre alt bin. %2$sWarum muss ich dies bestätigen?</a></span> <i class="fad fa-check-square theme-ws-ok"></i> Bestellung bestätigt! <i class="fad fa-envelope theme-ws-ok"></i> Die <strong>Bestellbestätigung</strong> wurde an die hinterlegte E-Mail-Adresse versandt. Ein Produkt im Warenkorb, darf nur von Kunden ab 18+ Jahren erworben werden. Wählen Jetzt unsere Produckte entdecken Filter löschen Zurück zur Startseite Kontaktiere uns Jetzt dein Lieblingsthema… Zögere nicht uns zu Kontaktieren, wenn Du Fragen hast! Seiten Bezahlmethoden Beiträge Produktkategorien Produkte Danke für Deinen Einkauf. Bestellnummer: Diese Seite existiert leider nicht. 