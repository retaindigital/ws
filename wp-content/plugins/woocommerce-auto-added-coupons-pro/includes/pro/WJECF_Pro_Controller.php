<?php

defined('ABSPATH') or die();

//require_once( 'wjecf-pro-evalmath.php' );

/**
 * Miscellaneous Pro functions
 */
class WJECF_Pro_Controller extends WJECF_Controller {

    public function __construct() {    
        parent::__construct();
    }

    /**
     * Singleton Instance
     *
     * @static
     * @return Singleton Instance
     */
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    protected static $_instance = null;    

    public function start() {
        parent::start();
        add_action('init', array( &$this, 'pro_controller_init' ));
    }

    public function pro_controller_init() {
        if ( ! class_exists('WC_Coupon') ) {
            return;
        }
        add_action( 'admin_init', array( $this, 'admin_init' ) );

        //Coupon columns
        add_filter( 'manage_shop_coupon_posts_columns', array( $this, 'admin_shop_coupon_columns' ), 20, 1 );
        add_action( 'manage_shop_coupon_posts_custom_column', array( $this, 'admin_render_shop_coupon_columns' ), 2 );

        //Frontend hooks
        add_action('woocommerce_coupon_loaded', array( $this, 'woocommerce_coupon_loaded' ), 10, 1);

        //PRO coupon validations
        add_action( 'wjecf_assert_coupon_is_valid', array( $this, 'wjecf_assert_coupon_is_valid' ), 10, 1 );
        add_action( 'wjecf_coupon_can_be_applied', array( $this, 'wjecf_coupon_can_be_applied' ), 10, 2 );
    }

/* ADMIN HOOKS */
    public function admin_init() {    
        //Admin hooks

        add_action('woocommerce_coupon_options_usage_restriction', array( $this, 'on_woocommerce_coupon_options_usage_restriction' ), 20, 1);
        add_action('woocommerce_coupon_options_usage_limit', array( $this, 'on_woocommerce_coupon_options_usage_limit' ), 20, 1);            
    }


    /**
     * Checks whether it's the first order for a customer.
     * 
     * @since 2.5.6
     * @return bool|null Returns null if the current user or billing email is not known!!!
     */
    public function is_first_purchase() {
        //Once we found out that the customer has ordered before; remember it in the session so that the customer can't trick us just by changing the email address
        if ( $this->get_session( 'has_purchased_before' ) ) return false;
        $known_user = false;

        $order_statuses = array( 'wc-completed', 'wc-processing', 'wc-on-hold' );

        $customer_id = WJECF_Wrap( WC()->customer )->get_id();
        if ( $customer_id )
        {
            $known_user = true;
            $orders = wc_get_orders( array(
                'limit' => 1,
                'customer_id' => $customer_id,
                'status' => $order_statuses
            ) );
            if ( count( $orders ) > 0 )
            {
                $this->set_session( 'has_purchased_before', true );
                return false;
            }
        }

        $email_addresses = $this->get_user_emails();
        //Remember entered email addresses, because WC does not remember them unit posted checkout form is valid.
        if ( empty( $email_addresses ) ) 
        {
            $email_addresses = $this->get_session( 'user_emails', array() );
        }
        else
        {
            $this->set_session( 'user_emails', $email_addresses );
        }

        foreach( $email_addresses as $email_address ) {
            $known_user = true;
            $orders = wc_get_orders( array(
                'limit' => 1,
                'customer' => $email_address,
                'status' => $order_statuses
            ) );
            if ( count( $orders ) > 0 )
            {
                $this->set_session( 'has_purchased_before', true );
                return false;
            }
        }

        return $known_user ? true : null;
    }


    /**
     * Extra validation rules for coupons. Throw an exception when not valid.
     * 
     * @param WC_Coupon $coupon 
     */
    public function wjecf_assert_coupon_is_valid( $coupon ) {
        if ( $this->is_valid_on_first_purchase_only( $coupon ) ) {
            $is_first_purchase = $this->is_first_purchase();
            if ( $is_first_purchase === false ) { //null if user data is not yet known
                throw new Exception( self::E_WC_COUPON_FIRST_PURCHASE_ONLY );
            }
        }
    }

    public function wjecf_coupon_can_be_applied( $can_be_applied, $coupon ) {
        //Don't auto apply unless we know it's the first purchase of the user
        if ( $this->is_valid_on_first_purchase_only( $coupon ) && ! $this->is_first_purchase() ) {
            return false;
        }
        return $can_be_applied;
    }

    public function is_valid_on_first_purchase_only( $coupon ) {
        return WJECF_Wrap( $coupon )->get_meta( '_wjecf_first_purchase_only' ) == 'yes';
    }

//Admin

    // //Tab 'extended features'
    // public function wjecf_coupon_metabox_products() {
    
    //since 2.5.0 moved to the 'Usage restriction' tab
    public function on_woocommerce_coupon_options_usage_restriction() {
        global $thepostid, $post;
        $thepostid = empty( $thepostid ) ? $post->ID : $thepostid;
        echo '<div class="options_group wjecf_hide_on_product_discount">';
        echo '<h3>' . __( 'Discount on cart with excluded products', 'woocommerce-jos-autocoupon') . '</h3>';

        //=============================
        //2.2.3 Allow even if excluded items in cart
        woocommerce_wp_checkbox( array(
            'id'          => '_wjecf_allow_cart_excluded',
            'label'       => __( 'Allow discount on cart with excluded items', 'woocommerce-jos-autocoupon' ),
            'description' => __( 'Check this box to allow a \'Cart Discount\' coupon to be applied even when excluded items are in the cart (see tab \'usage restriction\').', 'woocommerce-jos-autocoupon' ),
        ) );    
        echo '</div>';
    }

    public function on_woocommerce_coupon_options_usage_limit() {
        //=============================
        //2.5.6 First time customers only
        woocommerce_wp_checkbox( array(
            'id'          => '_wjecf_first_purchase_only',
            'label'       => __( 'First purchase only', 'woocommerce-jos-autocoupon' ),
            'description' => __( 'Check this box to limit this coupon to the first purchase of a customer only. (Verified by billing email address or user id)', 'woocommerce-jos-autocoupon' ),
        ) ); 
    }  

    public function admin_coupon_meta_fields( $coupon ) {
        //$fields = parent::admin_coupon_meta_fields();
        return array(
            //2.2.3
            '_wjecf_allow_cart_excluded' => 'yesno',
            //2.5.6
            '_wjecf_first_purchase_only' => 'yesno'
        );
    }

    private $inject_coupon_columns = array();
    /**
     * Inject custom columns on the Coupon Admin Page
     *
     * @param string $column_key The key to identify the column
     * @param string $caption The title to show in the header
     * @param callback $callback The function to call when rendering the column value ( Will be called with parameters $column_key, $post )
     * @param string $after_column Optional, The key of the column after which the column should be injected, if omitted the column will be placed at the end
     */
    public function inject_coupon_column( $column_key, $caption, $callback, $after_column = null ) {
        $this->inject_coupon_columns[ $column_key ] = array('caption' => $caption, 'callback' => $callback, 'after' => $after_column);
    }

    /**
     * Custom columns on coupon admin page
     *
     * @param array $columns
     */
    public function admin_shop_coupon_columns( $columns ) {
        $new_columns = array();
        foreach( $columns as $key => $column ) {
            $new_columns[$key] = $column;
            foreach( $this->inject_coupon_columns as $inject_key => $inject_column ) {
                if ( $inject_column['after'] == $key ) {
                    $new_columns[$inject_key] = $inject_column['caption'];
                }
            }
        }
        foreach( $this->inject_coupon_columns as $inject_key => $inject_column ) {
            if ( $inject_column['after'] == null || ! isset( $columns[ $inject_column['after'] ] ) ) {
                $new_columns[$inject_key] = $inject_column['caption'];
            }
        }
        return $new_columns;
    }

    /**
     * Output custom columns for coupons
     *
     * @param string $column
     */
    public function admin_render_shop_coupon_columns( $column ) {
        global $post;
        if ( isset( $this->inject_coupon_columns[$column]['callback'] ) ) {
            call_user_func( $this->inject_coupon_columns[$column]['callback'], $column, $post );
        }
    } 

//Frontend

    public function woocommerce_coupon_loaded ( $coupon ) {
        if ( $this->allow_overwrite_coupon_values() ) {
            $wrap_coupon = WJECF_Wrap( $coupon );
            //2.2.3 Allow coupon even if excluded products are not in the cart 
            //This way we can use the subtotal/quantity of matching products for a cart discount
            $allow_cart_excluded = $wrap_coupon->get_meta( '_wjecf_allow_cart_excluded' ) == 'yes';
            if ( $allow_cart_excluded && $wrap_coupon->is_type( WJECF_WC()->wc_get_cart_coupon_types() ) ) {
                //HACK: Overwrite the exclusions so WooCommerce will allow the coupon
                //These values are used in the WJECF_Controller->coupon_is_valid_for_product
                $wrap_coupon->set_excluded_product_ids( array() );
                $wrap_coupon->set_excluded_product_categories( array() );
                $wrap_coupon->set_exclude_sale_items( false );
            }
        }
    }

}
