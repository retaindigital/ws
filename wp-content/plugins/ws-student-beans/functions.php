<?php

define('WS_SB_PLUGIN_PATH', plugin_dir_path(__FILE__));

add_action( 'wp_enqueue_scripts', 'ws_sb_scripts', 20 );
function ws_sb_scripts() {
  // Scripts
  wp_enqueue_script('ws-sb-scripts',  trailingslashit(plugins_url()) . plugin_basename('ws-student-beans') . '/js/ws.sb.min.js', array ( 'jquery' ), 1.2, true);
}


/* Add custom tracking code to the thank-you page - Student Bean */
add_action( 'woocommerce_thankyou', 'ws_sb_thankyou' );
function ws_sb_thankyou( $order_id ) {
  
  // Lets grab the order
  $order = wc_get_order( $order_id );
  $post_meta = get_post_meta( $order_id );

  // adds the prefix to the order ID
  if( !empty( get_option('ws_order_prefix') ) )
    $order_ref = get_option('ws_order_prefix').$order_id;
  else
    $order_ref = $order_id;
  
  // Set the order details
  $order_currency = $order->get_currency();
  $order_shipping_total = $order->get_shipping_total();
  $order_shipping_tax = $order->get_shipping_tax();
  $order_tax_total = $order->get_total_tax();
  $order_number = $order->get_order_number();
  $currency = $order->get_order_currency();

  // Order Total (amount)
  $order_total = $order->get_total()-$order_shipping_total-$order_shipping_tax;

  // Loop the items
  $item_sku = array();
  foreach ($order->get_items() as $item) {

    // If it's a variation, load it a different way
    if( !empty( $item->get_variation_id() ) )
      $product = wc_get_product( $item->get_variation_id() );
    else
      $product = wc_get_product( $item->get_product_id() );

    $item_sku[] = $product->get_sku();
  }
  
  // Set the skus for SB (adv_sub3)
  $skus = implode("|",$item_sku);

  // Main URL Base
  $sb_url = 'https://studentbeansnetwork.go2cloud.org/aff_lsr?';
  // Transaction ID (set by SB and stored in a cookie)
  $sb_url .= 'transaction_id='.$_COOKIE['ws_sb_transaction'];
  // Order Value
  $sb_url .= '&amount='.$order_total;
  // Order ID
  $sb_url .= '&adv_sub='.$order_ref;
  // Order Items
  $sb_url .= '&adv_sub3='.$skus;


  // Postback to SB
  $sb = file_get_contents($sb_url);


  // Remove the cookies
  unset($_COOKIE['ws_sb_transaction']); 
  setcookie('ws_sb_transaction', null, -1, '/'); 

}

