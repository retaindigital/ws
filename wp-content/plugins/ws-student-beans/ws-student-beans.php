<?php
/**
 * Plugin Name: Student Beans
 * Plugin URI: http:/www.wilkinsonsword.com
 * Description: Custom Student Beans Integration for WS
 * Version: 1.0
 * Author: Mike Spence
 * Author URI: @mikespence
 */

$plugin = plugin_basename(__FILE__);
$pluginBase = plugin_dir_path(__FILE__);
$pluginURL = plugin_dir_url(__FILE__);

require($pluginBase . 'functions.php');