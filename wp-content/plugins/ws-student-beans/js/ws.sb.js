jQuery(document).ready(function($){
 
 	// Set Cookie through JS
	function setCookie(cname, cvalue, exdays) {
	  var d = new Date();
	  d.setTime(d.getTime() + (exdays*24*60*60*1000));
	  var expires = "expires="+ d.toUTCString();
	  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	// Retrieve Cookie through JS
	function getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}


	$.urlParam = function(name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
		if (results != null && results.length > 1) {
			return results[1] || 0;
		}

	}

	// Cookie Name
	var sbCookie = 'ws_sb_transaction';

 	// Check to see if we need to offer a different location (driving by ws plugin)
	if (getCookie(sbCookie) == '') {

		var sb_transaction = $.urlParam('aff_sub');

		// Create cookie
		setCookie(sbCookie, sb_transaction, 365);

	}

});
