<?php return array(
    'root' => array(
        'pretty_version' => 'dev-hotfix/5.5.3',
        'version' => 'dev-hotfix/5.5.3',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '2db3bd4bebf205b7866057765f54bb82af51f2e0',
        'name' => 'woocommerce/automatewoo',
        'dev' => false,
    ),
    'versions' => array(
        'woocommerce/automatewoo' => array(
            'pretty_version' => 'dev-hotfix/5.5.3',
            'version' => 'dev-hotfix/5.5.3',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '2db3bd4bebf205b7866057765f54bb82af51f2e0',
            'dev_requirement' => false,
        ),
    ),
);
