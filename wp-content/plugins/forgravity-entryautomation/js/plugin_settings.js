/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js/src/plugin_settings.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/src/plugin_settings.js":
/*!***********************************!*\
  !*** ./js/src/plugin_settings.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ./../../scss/plugin_settings.scss */ \"./scss/plugin_settings.scss\");\n\nvar FGEntryAutomationSettings = function () {\n\n\tvar self = this,\n\t    $ = jQuery;\n\n\tself.init = function () {\n\n\t\t// Add localized strings.\n\t\tself.strings = forgravity_entryautomation_plugin_settings_strings;\n\n\t\t// Initialize Extension Actions.\n\t\tself.runExtensionAction();\n\t};\n\n\t// # EXTENSIONS ----------------------------------------------------------------------------------------------------\n\n\t/**\r\n  * Handle Run Task Now button.\r\n  *\r\n  * @since 1.3\r\n  */\n\tself.runExtensionAction = function () {\n\n\t\t$(document).on('click', '#gaddon-setting-row-extensions a[data-action], #gform_setting_extensions a[data-action]', function (e) {\n\n\t\t\tvar $button = $(this),\n\t\t\t    action = $button.data('action'),\n\t\t\t    plugin = $button.data('plugin');\n\n\t\t\t// If this is the upgrade action, return.\n\t\t\tif ('upgrade' === action) {\n\t\t\t\treturn true;\n\t\t\t}\n\n\t\t\te.preventDefault();\n\n\t\t\t// Disable button.\n\t\t\t$button.attr('disabled', 'disabled');\n\n\t\t\t// Change button text.\n\t\t\t$button.html(self.strings.processing[action]);\n\n\t\t\t// Prepare request data.\n\t\t\tvar data = {\n\t\t\t\taction: 'fg_entryautomation_extension_action',\n\t\t\t\textension: {\n\t\t\t\t\taction: action,\n\t\t\t\t\tplugin: plugin\n\t\t\t\t},\n\t\t\t\tnonce: self.strings.nonce\n\t\t\t};\n\n\t\t\t// Run task.\n\t\t\t$.ajax({\n\t\t\t\turl: ajaxurl,\n\t\t\t\ttype: 'POST',\n\t\t\t\tdataType: 'json',\n\t\t\t\tdata: data,\n\t\t\t\tsuccess: function (response) {\n\n\t\t\t\t\t// If could not process action, display error message.\n\t\t\t\t\tif (!response.success) {\n\t\t\t\t\t\talert(response.data.error);\n\t\t\t\t\t}\n\n\t\t\t\t\t// Update button.\n\t\t\t\t\t$button.data('action', response.data.newAction);\n\t\t\t\t\t$button.html(response.data.newText);\n\n\t\t\t\t\t// Enable button.\n\t\t\t\t\t$button.removeAttr('disabled');\n\t\t\t\t}\n\n\t\t\t});\n\t\t});\n\t};\n\n\tself.init();\n};\n\njQuery(document).ready(function () {\n\tnew FGEntryAutomationSettings();\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9qcy9zcmMvcGx1Z2luX3NldHRpbmdzLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2pzL3NyYy9wbHVnaW5fc2V0dGluZ3MuanM/ODE3NyJdLCJzb3VyY2VzQ29udGVudCI6WyJyZXF1aXJlKCAnLi8uLi8uLi9zY3NzL3BsdWdpbl9zZXR0aW5ncy5zY3NzJyApO1xyXG5cclxudmFyIEZHRW50cnlBdXRvbWF0aW9uU2V0dGluZ3MgPSBmdW5jdGlvbigpIHtcclxuXHJcblx0dmFyIHNlbGYgPSB0aGlzLFxyXG5cdFx0JCAgICA9IGpRdWVyeTtcclxuXHJcblx0c2VsZi5pbml0ID0gZnVuY3Rpb24oKSB7XHJcblxyXG5cdFx0Ly8gQWRkIGxvY2FsaXplZCBzdHJpbmdzLlxyXG5cdFx0c2VsZi5zdHJpbmdzID0gZm9yZ3Jhdml0eV9lbnRyeWF1dG9tYXRpb25fcGx1Z2luX3NldHRpbmdzX3N0cmluZ3M7XHJcblxyXG5cdFx0Ly8gSW5pdGlhbGl6ZSBFeHRlbnNpb24gQWN0aW9ucy5cclxuXHRcdHNlbGYucnVuRXh0ZW5zaW9uQWN0aW9uKCk7XHJcblxyXG5cdH1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cdC8vICMgRVhURU5TSU9OUyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG5cdC8qKlxyXG5cdCAqIEhhbmRsZSBSdW4gVGFzayBOb3cgYnV0dG9uLlxyXG5cdCAqXHJcblx0ICogQHNpbmNlIDEuM1xyXG5cdCAqL1xyXG5cdHNlbGYucnVuRXh0ZW5zaW9uQWN0aW9uID0gZnVuY3Rpb24oKSB7XHJcblxyXG5cdFx0JCggZG9jdW1lbnQgKS5vbiggJ2NsaWNrJywgJyNnYWRkb24tc2V0dGluZy1yb3ctZXh0ZW5zaW9ucyBhW2RhdGEtYWN0aW9uXSwgI2dmb3JtX3NldHRpbmdfZXh0ZW5zaW9ucyBhW2RhdGEtYWN0aW9uXScsIGZ1bmN0aW9uKCBlICkge1xyXG5cclxuXHRcdFx0dmFyICRidXR0b24gPSAkKCB0aGlzICksXHJcblx0XHRcdFx0YWN0aW9uID0gJGJ1dHRvbi5kYXRhKCAnYWN0aW9uJyApLFxyXG5cdFx0XHRcdHBsdWdpbiA9ICRidXR0b24uZGF0YSggJ3BsdWdpbicgKTtcclxuXHJcblx0XHRcdC8vIElmIHRoaXMgaXMgdGhlIHVwZ3JhZGUgYWN0aW9uLCByZXR1cm4uXHJcblx0XHRcdGlmICggJ3VwZ3JhZGUnID09PSBhY3Rpb24gKSB7XHJcblx0XHRcdFx0cmV0dXJuIHRydWU7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcblx0XHRcdC8vIERpc2FibGUgYnV0dG9uLlxyXG5cdFx0XHQkYnV0dG9uLmF0dHIoICdkaXNhYmxlZCcsICdkaXNhYmxlZCcgKTtcclxuXHJcblx0XHRcdC8vIENoYW5nZSBidXR0b24gdGV4dC5cclxuXHRcdFx0JGJ1dHRvbi5odG1sKCBzZWxmLnN0cmluZ3MucHJvY2Vzc2luZ1sgYWN0aW9uIF0gKTtcclxuXHJcblx0XHRcdC8vIFByZXBhcmUgcmVxdWVzdCBkYXRhLlxyXG5cdFx0XHR2YXIgZGF0YSA9IHtcclxuXHRcdFx0XHRhY3Rpb246ICAgICdmZ19lbnRyeWF1dG9tYXRpb25fZXh0ZW5zaW9uX2FjdGlvbicsXHJcblx0XHRcdFx0ZXh0ZW5zaW9uOiB7XHJcblx0XHRcdFx0XHRhY3Rpb246IGFjdGlvbixcclxuXHRcdFx0XHRcdHBsdWdpbjogcGx1Z2luXHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHRub25jZTogICAgIHNlbGYuc3RyaW5ncy5ub25jZVxyXG5cdFx0XHR9O1xyXG5cclxuXHRcdFx0Ly8gUnVuIHRhc2suXHJcblx0XHRcdCQuYWpheChcclxuXHRcdFx0XHR7XHJcblx0XHRcdFx0XHR1cmw6ICAgICAgYWpheHVybCxcclxuXHRcdFx0XHRcdHR5cGU6ICAgICAnUE9TVCcsXHJcblx0XHRcdFx0XHRkYXRhVHlwZTogJ2pzb24nLFxyXG5cdFx0XHRcdFx0ZGF0YTogICAgIGRhdGEsXHJcblx0XHRcdFx0XHRzdWNjZXNzOiAgZnVuY3Rpb24oIHJlc3BvbnNlICkge1xyXG5cclxuXHRcdFx0XHRcdFx0Ly8gSWYgY291bGQgbm90IHByb2Nlc3MgYWN0aW9uLCBkaXNwbGF5IGVycm9yIG1lc3NhZ2UuXHJcblx0XHRcdFx0XHRcdGlmICggISByZXNwb25zZS5zdWNjZXNzICkge1xyXG5cdFx0XHRcdFx0XHRcdGFsZXJ0KCByZXNwb25zZS5kYXRhLmVycm9yICk7XHJcblx0XHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHRcdC8vIFVwZGF0ZSBidXR0b24uXHJcblx0XHRcdFx0XHRcdCRidXR0b24uZGF0YSggJ2FjdGlvbicsIHJlc3BvbnNlLmRhdGEubmV3QWN0aW9uICk7XHJcblx0XHRcdFx0XHRcdCRidXR0b24uaHRtbCggcmVzcG9uc2UuZGF0YS5uZXdUZXh0ICk7XHJcblxyXG5cdFx0XHRcdFx0XHQvLyBFbmFibGUgYnV0dG9uLlxyXG5cdFx0XHRcdFx0XHQkYnV0dG9uLnJlbW92ZUF0dHIoICdkaXNhYmxlZCcgKTtcclxuXHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdClcclxuXHJcblx0XHR9ICk7XHJcblxyXG5cdH1cclxuXHJcblx0c2VsZi5pbml0KCk7XHJcblxyXG59XHJcblxyXG5qUXVlcnkoIGRvY3VtZW50ICkucmVhZHkoIGZ1bmN0aW9uKCkgeyBuZXcgRkdFbnRyeUF1dG9tYXRpb25TZXR0aW5ncygpOyB9ICk7XHJcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7Ozs7O0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQU5BO0FBQ0E7QUFRQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFwQkE7QUF5QkE7QUFFQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFBQTtBQUFBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./js/src/plugin_settings.js\n");

/***/ }),

/***/ "./scss/plugin_settings.scss":
/*!***********************************!*\
  !*** ./scss/plugin_settings.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zY3NzL3BsdWdpbl9zZXR0aW5ncy5zY3NzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc2Nzcy9wbHVnaW5fc2V0dGluZ3Muc2Nzcz80MzI5Il0sInNvdXJjZXNDb250ZW50IjpbIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpbiJdLCJtYXBwaW5ncyI6IkFBQUEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./scss/plugin_settings.scss\n");

/***/ })

/******/ });