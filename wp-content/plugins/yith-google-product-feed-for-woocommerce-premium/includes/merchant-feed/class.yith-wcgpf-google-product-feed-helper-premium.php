<?php // phpcs:ignore WordPress.NamingConventions
/**
 * Class Helper Generate Product Feed
 *
 * @author  Yithemes
 * @package YITH\YITHGoogleProductFeedForWooCommerce\CLasses
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WCGPF_VERSION' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WCGPF_Helper_Premium' ) ) {
	/**
	 * YITH_WCGPF_Helper_Premium
	 *
	 * @since 1.0.0
	 */
	class YITH_WCGPF_Helper_Premium extends YITH_WCGPF_Helper {

		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WCGPF_Helper
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WCGPF_Helper
		 * @since 1.0.0
		 */
		public static function get_instance() {
			$self = __CLASS__ . ( class_exists( __CLASS__ . '_Premium' ) ? '_Premium' : '' );

			if ( is_null( $self::$instance ) ) {
				$self::$instance = new $self();
			}

			return $self::$instance;
		}
		/**
		 * Feed type
		 *
		 * @var $feed_type
		 */
		public $feed_type;
		/**
		 * Merchant
		 *
		 * @var $merchant
		 */
		public $merchant;
		/**
		 * Feed id
		 *
		 * @var $feed_id
		 */
		public $feed_id;

		/**
		 * Allowed_merchant
		 *
		 * @var array allowed_merchant
		 */
		public $allowed_merchant = array();

		/**
		 * __construct
		 *
		 * @return void
		 */
		public function __construct() { //phpcs:ignore
			parent::__construct();
		}

		/**
		 * Generate_feed
		 *
		 * @return void
		 */
		public function generate_feed() {
			if ( $this->is_url_for_generate_feed() ) {
				list( $type, $merchant, $id ) = $this->get_params_for_generating_feed();
				$premium_suffix               = defined( 'YITH_WCGPF_PREMIUM' ) && YITH_WCGPF_PREMIUM ? '_Premium' : '';
				$provider                     = 'YITH_WCGPF_Generate_Feed_' . $merchant . $premium_suffix;
				if ( class_exists( $provider ) ) {
					$limit         = isset( $_GET['limit'] ) ? wc_clean( wp_unslash( $_GET['limit'] ) ) : 0; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.NonceVerification
					$offset        = isset( $_GET['offset'] ) ? wc_clean( wp_unslash( $_GET['offset'] ) ) : 0; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.NonceVerification
					$generate_feed = new $provider( $id, $type, $merchant, $limit, $offset );
				}
			}
		}
	}
}
