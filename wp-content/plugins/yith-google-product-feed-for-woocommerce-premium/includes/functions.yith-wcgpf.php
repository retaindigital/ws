<?php // phpcs:ignore WordPress.NamingConventions

/**
 * Construc a get a select dropdown
 *
 * @param array $args args.
 */
function yith_wcgpf_get_dropdown( $args = array() ) {
	$default_args = array(
		'id'      => '',
		'name'    => '',
		'class'   => '',
		'style'   => '',
		'options' => array(),
		'value'   => '',
		'echo'    => false,
	);

	$args = wp_parse_args( $args, $default_args );
	extract( $args ); //phpcs:ignore WordPress.PHP.DontExtract
	/**
	 * Values
	 *
	 * @var string $id id.
	 * @var string $name name.
	 * @var string $class class.
	 * @var string $style style.
	 * @var array  $options options.
	 * @var string $value value.
	 * @var bool   $echo echo.
	 */
	$html = "<select id='$id' name='$name' class='$class' style='$style'>";

	foreach ( $options as $option_key => $option_label ) {
		$selected = selected( $option_key == $value, true, false );
		$html    .= "<option value='$option_key' $selected>$option_label</option>";
	}

	$html .= '</select>';

	if ( $echo ) {
		echo $html; // phpcs:ignore WordPress.Security.EscapeOutput
	} else {
		return $html;
	}
}

/**
 * Construc a get a input
 *
 * @param array $args args.
 */
function yith_wcgpf_get_input( $args = array() ) {
	$default_args = array(
		'id'    => '',
		'name'  => '',
		'class' => '',
		'type'  => '',
        // @codingStandardsIgnoreStart
        // 'checked' => '',
        // @codingStandardsIgnoreEnd
		'value' => '',
		'echo'  => false,
	);

	$args = wp_parse_args( $args, $default_args );
	extract( $args ); //phpcs:ignore WordPress.PHP.DontExtract
	/**
	 * Values
	 *
	 * @var string $id id.
	 * @var string $name name.
	 * @var string $class class.
	 * @var string $type type.
	 * @var string $checked checked.
	 * @var string $value value.
	 * @var bool   $echo echo.
	 */
	$value = esc_html( $value );
	$html  = "<input type='$type' id='$id' name='$name' class='$class' value='$value'>";

	if ( $echo ) {
		echo $html; // phpcs:ignore WordPress.Security.EscapeOutput
	} else {
		return $html;
	}
}
