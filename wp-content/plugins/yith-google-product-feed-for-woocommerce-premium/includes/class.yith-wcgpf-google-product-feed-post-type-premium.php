<?php // phpcs:ignore WordPress.NamingConventions
/**
 * Post Types Premium class
 *
 * @author  Yithemes
 * @package YITH\YITHGoogleProductFeedForWooCommerce\CLasses
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WCGPF_VERSION' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WCGPF_Post_Types_Feed_Premium' ) ) {
	/**
	 * YITH WCGPF Post Type Feed
	 *
	 * @since 1.0.0
	 */
	class YITH_WCGPF_Post_Types_Feed_Premium extends YITH_WCGPF_Post_Types_Feed {

		/**
		 * Main Instance
		 *
		 * @var YITH_WCGPF_Post_Types_Feed_Premium
		 * @since 1.0
		 * @access protected
		 */
		protected static $instance = null;

		/**
		 * Main plugin Instance
		 *
		 * @return YITH_WCGPF_Post_Types_Feed_Premium instance
		 * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
		 */
		public static function get_instance() {
			$self = __CLASS__ . ( class_exists( __CLASS__ . '_Premium' ) ? '_Premium' : '' );

			if ( is_null( $self::$instance ) ) {
				$self::$instance = new $self();
			}

			return $self::$instance;
		}

		/**
		 * Construct
		 *
		 * @return YITH_WCGPF_Post_Types_Feed_Premium
		 * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
		 */
		public function __construct() { //phpcs:ignore
			parent::__construct();
		}


		/**
		 * Add template metabox .
		 *
		 * @param mixed $post post.
		 */
		public function configuration_template_metabox( $post ) {

			if ( file_exists( YITH_WCGPF_TEMPLATE_PATH . 'admin/make-tab/configuration-feed-premium.php' ) ) {
				include_once YITH_WCGPF_TEMPLATE_PATH . 'admin/make-tab/configuration-feed-premium.php';
			}
		}


		/**
		 * Save post data.
		 *
		 * @param mixed $post_id post id.
		 */
		public function save_post_data( $post_id ) {
			if ( isset( $_POST['yith_wcgpf_config_feed_premium_nonce'] ) && wp_verify_nonce( sanitize_key( wp_unslash( $_POST['yith_wcgpf_config_feed_premium_nonce'] ) ), 'yith_wcgpf_config_feed_premium' ) ) {

				if ( ! isset( $_POST['yith-merchant'] ) || ! isset( $_POST['yith-feed-type'] ) ) {
					return;
				}

				$merchant            = wc_clean( wp_unslash( $_POST['yith-merchant'] ) ); // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$feed_type           = wc_clean( wp_unslash( $_POST['yith-feed-type'] ) ); // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$template_feed       = 'personalized';
				$category_option     = isset( $_POST['yith_category_incl_excl_selector'] ) ? wc_clean( wp_unslash( $_POST['yith_category_incl_excl_selector'] ) ) : false; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$categories_selected = isset( $_POST['yith-feed-category'] ) ? wc_clean( wp_unslash( $_POST['yith-feed-category'] ) ) : false; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$tags_selected       = isset( $_POST['yith-feed-tags'] ) ? wc_clean( wp_unslash( $_POST['yith-feed-tags'] ) ) : false; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$include_products    = isset( $_POST['yith-feed-include-product'] ) ? wc_clean( wp_unslash( $_POST['yith-feed-include-product'] ) ) : false; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$exclude_products    = isset( $_POST['yith-feed-exclude-product'] ) ? wc_clean( wp_unslash( $_POST['yith-feed-exclude-product'] ) ) : false; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

				$filters = array(
					'category_option'     => $category_option,
					'categories_selected' => $categories_selected,
					'tags_selected'       => $tags_selected,
					'include_products'    => $include_products,
					'exclude_products'    => $exclude_products,
				);

				$values = array(
					'merchant'      => $merchant,
					'post_id'       => $post_id,
					'feed_type'     => $feed_type,
					'template_feed' => $template_feed,
				);

				$values = array_merge( $values, $filters );

				// Feed template !
				$attributes = isset( $_POST['yith-wcgpf-attributes'] ) ? wc_clean( wp_unslash( $_POST['yith-wcgpf-attributes'] ) ) : array(); // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$prefix     = isset( $_POST['yith_wcgpf_prexif'] ) ? wc_clean( wp_unslash( $_POST['yith_wcgpf_prexif'] ) ) : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$value      = isset( $_POST['yith-wcgpf-value'] ) ? wc_clean( wp_unslash( $_POST['yith-wcgpf-value'] ) ) : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				$suffix     = isset( $_POST['yith_wcgpf_sufix'] ) ? wc_clean( wp_unslash( $_POST['yith_wcgpf_sufix'] ) ) : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized

				$count = count( $attributes );
				for ( $i = 0; $i < $count; $i++ ) {
					if ( '' !== $attributes[ $i ] ) {
						$feed_template[ $i ]['attributes'] = $attributes[ $i ];
						$feed_template[ $i ]['prefix']     = $prefix[ $i ];
						$feed_template[ $i ]['value']      = $value[ $i ];
						$feed_template[ $i ]['suffix']     = $suffix[ $i ];
					}
				}

				$feed_template = array(
					'feed_template' => ( isset( $feed_template ) ? $feed_template : array() ),
				);
				$values        = array_merge( $values, $feed_template );

				if ( ! empty( $feed_template ) && ! empty( $values ) ) {

					$functions          = YITH_Google_Product_Feed()->functions;
					$feed               = $functions->create_feed( $merchant, $values );
					$values['feed_url'] = $feed;

					update_post_meta( $post_id, 'yith_wcgpf_save_feed', $values );

					do_action( 'yith_wcgpf_save_feed_file', $post_id, $values );

				}
			}
		}

		/**
		 * Add_metaboxes
		 *
		 * @param  mixed $post post.
		 * @return void
		 */
		public function add_metaboxes( $post ) {
			parent::add_metaboxes( $post );
			add_meta_box( 'yith_wcgpf_filters_and_conditions', esc_html__( 'Filters and conditions', 'yith-google-product-feed-for-woocommerce' ), array( $this, 'filters_and_conditions_metabox' ), 'yith-wcgpf-feed', 'side', 'low' );
		}

		/**
		 * Filters_and_conditions_metabox
		 *
		 * @param  mixed $post post.
		 * @return void
		 */
		public function filters_and_conditions_metabox( $post ) {
			if ( file_exists( YITH_WCGPF_TEMPLATE_PATH . 'admin/make-tab/filter-and-conditions.php' ) ) {
				include_once YITH_WCGPF_TEMPLATE_PATH . 'admin/make-tab/filter-and-conditions.php';
			}
		}
	}
}
