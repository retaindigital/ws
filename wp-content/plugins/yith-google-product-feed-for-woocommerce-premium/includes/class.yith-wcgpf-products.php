<?php // phpcs:ignore WordPress.NamingConventions
/**
 * Product class
 *
 * @author  Yithemes
 * @package YITH\YITHGoogleProductFeedForWooCommerce\CLasses
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WCGPF_VERSION' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WCGPF_Products' ) ) {
	/**
	 * YITH_WCGPF_Products
	 *
	 * @since 1.0.0
	 */
	class YITH_WCGPF_Products {

		/**
		 * Main Instance
		 *
		 * @var YITH_WCGPF_Products
		 * @since 1.0
		 * @access protected
		 */
		protected static $instance = null;

		/**
		 * Main plugin Instance
		 *
		 * @return instance
		 * @var YITH_WCGPF_Products instance
		 * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
		 */
		public static function get_instance() {
			$self = __CLASS__ . ( class_exists( __CLASS__ . '_Premium' ) ? '_Premium' : '' );

			if ( is_null( $self::$instance ) ) {
				$self::$instance = new $self();
			}

			return $self::$instance;
		}

		/**
		 * Construct
		 *
		 * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
		 * @since 1.0
		 */
		protected function __construct() {
			add_filter( 'yith_wcgpf_product_properties_wc', array( $this, 'add_wc_product_attributes' ), 10 );
		}

		/**
		 * Return list of post type product and product_variation
		 *
		 * @author Carlos Rodríguez <carlos.rodriguez@yourinspiration.it>
		 * @since 1.0
		 *
		 * @param  mixed $filters filters.
		 * @param  mixed $limit limit.
		 * @param  mixed $offset offset.
		 * @return $posts
		 */
		public function get_products( $filters = '', $limit = '', $offset = '' ) {
			$params = array(
				'post_type'      => array( 'product' ),
				'posts_per_page' => 5,
				'fields'         => 'ids',
			);
			$posts  = get_posts( $params );

			return $posts;
		}
	}
}
