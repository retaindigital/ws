<?php // phpcs:ignore WordPress.NamingConventions
/**
 * Class Helper generate feed file
 *
 * @author  Yithemes
 * @package YITH\YITHGoogleProductFeedForWooCommerce\CLasses
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WCGPF_VERSION' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WCGPF_File_Helper' ) ) {
	/**
	 * YITH_WCGPF_Helper
	 *
	 * @since 1.0.0
	 */
	class YITH_WCGPF_File_Helper {


		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WCGPF_File_Helper
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WCGPF_File_Helper
		 * @since 1.0.0
		 */
		public static function get_instance() {
			$self = __CLASS__ . ( class_exists( __CLASS__ . '_Premium' ) ? '_Premium' : '' );

			if ( is_null( $self::$instance ) ) {
				$self::$instance = new $self();
			}

			return $self::$instance;
		}

		/**
		 * Feed_type
		 *
		 * @var $feed_type
		 */
		public $feed_type;
		/**
		 * Merchant
		 *
		 * @var $merchant
		 */
		public $merchant;
		/**
		 * Feed_id
		 *
		 * @var $feed_id
		 */
		public $feed_id;
		/**
		 * Limit
		 *
		 * @var $limit
		 */
		public $limit;
		/**
		 * Offset
		 *
		 * @var $offset
		 */
		public $offset;
		/**
		 * Url
		 *
		 * @var $url
		 */
		public $url;

		/**
		 * Allowed_merchant
		 *
		 * @var array
		 */
		public $allowed_merchant = array();

		/**
		 * __construct
		 *
		 * @return void
		 */
		public function __construct() {

			add_action( 'wp_ajax_yith_wcgpf_generate_feed_file', array( $this, 'yith_wcgpf_generate_feed_file' ) );
			add_action( 'wp_ajax_nopriv_yith_wcgpf_generate_feed_file', array( $this, 'yith_wcgpf_generate_feed_file' ) );

		}

		/**
		 * Save_feed
		 *
		 * @param  mixed $values values.
		 * @param  mixed $mode mode.
		 * @param  mixed $feed feed.
		 * @return void
		 */
		public function save_feed( $values, $mode, $feed = '' ) {
			$upload_dir = wp_upload_dir();
			$base       = $upload_dir['basedir'];
			$title      = get_the_title( $values['post_id'] );
			$filename   = $title ? $title : $values['post_id'];
			$filename   = str_replace( ' ', '', $filename );
			$type       = $values['feed_type'];
			// Save File !
			$path = $base . '/yith-wcgpf-feeds/' . $values['merchant'] . '/' . $type;
			$file = $path . '/' . $filename . '.' . $type;

			if ( $feed ) {
				$save = new YITH_Google_Product_Feed_Save_Feed();
				if ( 'txt' === $type ) {
					$save->save_feed_txt_file( $path, $file, $feed, $mode );
				} else {
					$save->save_feed_file( $path, $file, $feed, $mode );
				}
			}

			$this->url = $upload_dir['baseurl'] . '/yith-wcgpf-feeds/' . $values['merchant'] . '/' . $type . '/' . $filename . '.' . $type;
		}

		/**
		 * Yith_wcgpf_generate_feed_file
		 *
		 * @return void
		 */
		public function yith_wcgpf_generate_feed_file() {

			$this->offset    = isset( $_POST['offset'] ) ? intval( $_POST['offset'] ) : ''; //phpcs:ignore WordPress.Security.NonceVerification
			$this->limit     = isset( $_POST['limit'] ) ? intval( $_POST['limit'] ) : ''; //phpcs:ignore WordPress.Security.NonceVerification
			$this->feed_id   = isset( $_POST['post_id'] ) ? intval( $_POST['post_id'] ) : ''; //phpcs:ignore WordPress.Security.NonceVerification
			$this->feed_type = isset( $_POST['type'] ) ? sanitize_text_field( wp_unslash( $_POST['type'] ) ) : ''; //phpcs:ignore WordPress.Security.NonceVerification

			if ( 0 === intval( $this->limit ) ) {
				$this->limit = apply_filters( 'yith_wcgpf_limit_generate_feed', 150 );
			}

			$values        = get_post_meta( $this->feed_id, 'yith_wcgpf_save_feed', true );
			$generate_feed = YITH_Google_Product_Feed()->generate_file;
			$products      = YITH_Google_Product_Feed()->products;
			$ids_product   = $products->get_products( $values, $this->limit, $this->offset );
            // @codingStandardsIgnoreStart
            // $product_ids = array_slice($ids_product,$this->offset,$this->limit);
            // @codingStandardsIgnoreEnd

			$product_ids = $ids_product;

			if ( 0 === intval( $this->offset ) ) {
				if ( 'xml' === $this->feed_type ) {

					$head = $generate_feed->get_header_xml();

				} else {

					$head = $generate_feed->get_header_txt( $values );
				}
				$mode = 'wb';
				$this->save_feed( $values, $mode, $head );
			}
			if ( $ids_product && $product_ids ) {

				$feed_body = $generate_feed->create_feed( $this->feed_id, $this->feed_type, $product_ids );
				$mode      = 'a';
				$this->save_feed( $values, $mode, $feed_body );

				$data = array(
					'limit'    => $this->limit,
					'offset'   => $this->offset + $this->limit,
					'products' => count( $ids_product ),
					'post_id'  => $this->feed_id,
					'type'     => $this->feed_type,
				);
				wp_send_json_success( $data );

			} else { // Close the file !
				if ( 'xml' === $this->feed_type ) {

					$footer = $generate_feed->get_footer_xml();
					$mode   = 'a';
					$this->save_feed( $values, $mode, $footer );
				} else {
					$mode = 'a';
					$this->save_feed( $values, $mode );
				}

				$values['feed_file'] = $this->url;
				update_post_meta( $this->feed_id, 'yith_wcgpf_save_feed', $values );

				$data = array(
					'limit'    => $this->limit,
					'offset'   => $this->offset + $this->limit,
					'products' => 0,
					'post_id'  => 0,
					'type'     => $this->feed_type,
				);
				wp_send_json_success( $data );
			}
		}
	}
}
