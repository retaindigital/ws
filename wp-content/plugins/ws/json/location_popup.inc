<div class="modal fade" id="geoSwitcher" tabindex="-1" role="dialog" aria-labelledby="geoSwitcherLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      x </button>
      <div class="modal-inner row">
        <div class="size-guide-content col-lg-12">
          <div class="size-guide-inner">
            <div class="vc_row wpb_row vc_row-fluid">
              <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                  <div class="wpb_wrapper">
                    <div class="wpb_single_image wpb_content_element vc_align_center">
                      <p>{{VISITING_FROM}}</p>
                      <div class="geo-current">
                        <i class="fad fa-map-marked-alt fa-2x"></i><br>
                        <strong>{{CURRENT}}</strong>
                      </div>
                      <p>{{SWITCH_TO}}</p>
                      <a href="{{GO_TO_URL}}" class="btn btn-primary">{{GO_BUTTON_TEXT}}</a>
                      <button type="button" class="btn-sm" data-dismiss="modal" aria-label="Close">{{NO_THANKS_TEXT}}</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>