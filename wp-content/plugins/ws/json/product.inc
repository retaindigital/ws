<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "{{PRODUCT_TYPE}}",
  "description": "{{PRODUCT_DESC}}",
  "sku": "{{PRODUCT_SKU}}",
  "brand": "{{BRAND_NAME}}",
  "name": "{{PRODUCT_NAME}}",
  "image": "{{PRODUCT_IMG}}",
  "offers": {
    "@type": "Offer",
    "availability": "http://schema.org/InStock",
    "url": "{{PRODUCT_URL}}",
    "price": "{{PRODUCT_PRICE}}",
    "priceCurrency": "{{PRODUCT_CURRENCY}}"
  }
}
</script>