<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Article",
  "mainEntityOfPage": {
	"@type": "WebPage",
	"@id": "{{ART_URL}}"
  },
  "headline": "{{ART_TITLE}}",
  "image": [	"{{ART_IMG}}"
   ],
  "datePublished": "{{ART_DATE_PUB}}",
  "dateModified": "{{ART_DATE_MOD}}",
  "author": {
	"@type": "Person",
	"name": "{{ART_AUTHOR}}"
  },
   "publisher": {
	"@type": "Organization",
	"name": "Wilkinson Sword",
	"logo": {
  	"@type": "ImageObject",
  	"url": "{{ART_BRAND_LOGO}}"
	}
  },
  "description": "{{ART_SHORT_DESC}}"
}
</script>