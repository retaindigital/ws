<script type="application/ld+json">
	{ "@context": "https://schema.org",
	"@type": "Organization",
	"name": "Wilkinson Sword",
	"legalName" : "Wilkinson Sword Ltd",
	"url": "https://www.wilkinsonsword.com/en-gb/",
	"logo": "{{LOGO_URL}}",
	"foundingDate": "1889",
	"founders": [
	{
		"@type": "Person",
		"name": "Henry Nock"
	},
	{
		"@type": "Person",
		"name": "James Wilkinson"
	} ],
	"address": {
	"@type": "PostalAddress",
	"streetAddress": "Totteridge Rd",
	"addressLocality": "High Wycombe",
	"addressRegion": "Buckinghamshire",
	"postalCode": "HP13 6DG",
	"addressCountry": "UK"
},
"contactPoint": {
"@type": "ContactPoint",
"contactType": "customer support",
"telephone": "[+44 1494 216894]"
},
"sameAs": [ 
"https://www.facebook.com/WilkinsonSwordMenUK",
"https://twitter.com/WSMenUK",
"https://www.instagram.com/wilkinsonswordmenuk/",
"https://www.youtube.com/user/WilkinsonSwordUK"
]}
</script>