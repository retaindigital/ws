<?php
// Cart and checkout
$hide_standard_shipping_name 	= 'ws_hide_standard_shipping';

$free_shipping_spend_name		= 'ws_free_shipping_spend';
$free_shipping_message_line_1_name		= 'ws_free_shipping_message_line_1';
$free_shipping_message_line_2_name		= 'ws_free_shipping_message_line_2';


// $warehouse_handling_days_name	= 'ws_warehouse_handing_days';
// $shipping_delivery_days_name	= 'ws_shipping_delivery_days';
$allow_saturday_delivery_name	= 'ws_allow_saturday_delivery';
$shipping_cutoff_time_name		= 'ws_shipping_cutoff_time';

$hide_notes_box_name					=	'ws_hide_notes';
$hide_company_name_name				=	'ws_hide_company_name';

// Handle the different shipping and warehouse days
$warehouse_handling_days_name_prefix = 'ws_warehouse_handing_days_';
$shipping_delivery_days_name_prefix = 'ws_shipping_delivery_days_';
$if_free_prefix = 'ws_hide_shipping_method_if_free_';

$warehouse_handling_days_name = array();
$shipping_delivery_days_name = array();
$hide_shipping = array();


$delivery_zones = WC_Shipping_Zones::get_zones();
foreach ((array) $delivery_zones as $key => $the_zone ) {

	$default_zone = new WC_Shipping_Zone($the_zone['zone_id']);
	$default_zone_shipping_methods = $default_zone->get_shipping_methods();

	foreach ($default_zone_shipping_methods  as $key => $value) {
		$warehouse_handling_days_name[$value->instance_id]['name']	= $warehouse_handling_days_name_prefix.$value->instance_id;
		$warehouse_handling_days_name[$value->instance_id]['label']	= $value->title.' ('.$the_zone['zone_name'].')';
		$warehouse_handling_days_name[$value->instance_id]['type']	= $value->id;

		$shipping_delivery_days_name[$value->instance_id]['name'] 	= $shipping_delivery_days_name_prefix.$value->instance_id;
		$shipping_delivery_days_name[$value->instance_id]['label'] 	= $value->title.' ('.$the_zone['zone_name'].')';
		$shipping_delivery_days_name[$value->instance_id]['type'] 	= $value->id;

		$hide_shipping_name[$value->instance_id]['name']						= $if_free_prefix.$value->instance_id;
		$hide_shipping_name[$value->instance_id]['label']						= $value->title.' ('.$the_zone['zone_name'].')';
		$hide_shipping_name[$value->instance_id]['type']						= $value->id;
	}

}


// Orders
$order_prefix_name						=	'ws_order_prefix';
$order_suffix_name						=	'ws_order_suffix';

// General
$hidden_field_name 						= 'ws_hidden';