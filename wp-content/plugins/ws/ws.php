<?php
/**
 * Plugin Name: WS
 * Plugin URI: http:/www.wilkinsonsword.com
 * Description: Custom WS functions
 * Version: 1.0
 * Author: Mike Spence
 * Author URI: @mikespence
 */

$plugin = plugin_basename(__FILE__);
$pluginBase = plugin_dir_path(__FILE__);
$pluginURL = plugin_dir_url(__FILE__);

require($pluginBase . 'functions.php');
require($pluginBase . 'acf_fields.php');


add_action('admin_menu', 'ws_admin_menu');

function ws_admin_menu() {
    add_options_page('Wilkinson Sword', 'Wilkinson Sword', 'manage_options', 'ws-options', 'ws_options');
}

function ws_options() {
	require $pluginBase.'ws_admin.php';
}