<?php

// Functions needed after Picasso
require($pluginBase . 'active_functions.php');


// Only load inactive if EPC isn't live
$theme = wp_get_theme(); // gets the current theme

if ( 'EPC Theme' != $theme->name ) {
  
  // Functions replaced in Picasso
  require($pluginBase . 'inactive_functions.php');

}
