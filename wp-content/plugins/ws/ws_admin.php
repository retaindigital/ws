<?php

	require($pluginBase . 'field_names.php');

  $posted_hidden = filter_input(INPUT_POST, $hidden_field_name);
  if (($posted_hidden) && ($posted_hidden == 'Y')) {

  	// Free Shipping Line 1
		$free_shipping_message_line_1_value = filter_input(INPUT_POST, $free_shipping_message_line_1_name, FILTER_SANITIZE_STRING);
		update_option($free_shipping_message_line_1_name, $free_shipping_message_line_1_value);
    
    // Free Shipping Line 2
    $free_shipping_message_line_2_value = filter_input(INPUT_POST, $free_shipping_message_line_2_name, FILTER_SANITIZE_STRING);
    update_option($free_shipping_message_line_2_name, $free_shipping_message_line_2_value);

    // Free Shipping Value
    $free_shipping_spend_value = filter_input(INPUT_POST, $free_shipping_spend_name, FILTER_SANITIZE_STRING);
    update_option($free_shipping_spend_name, $free_shipping_spend_value);

  	// Save Hide Standard Shipping
	  foreach ($hide_shipping_name as $key => $value) {
			$hide_shipping_name_value = filter_input(INPUT_POST, $value['name'], FILTER_SANITIZE_STRING);
			update_option($value['name'], $hide_shipping_name_value);
	  }

		// Save Shipping Days
	  foreach ($shipping_delivery_days_name as $key => $value) {
			$shipping_delivery_days_value = filter_input(INPUT_POST, $value['name'], FILTER_SANITIZE_STRING);
			update_option($value['name'], $shipping_delivery_days_value);
	  }

		// Save Warehouse Handling Days
	  foreach ($warehouse_handling_days_name as $key => $value) {
			$warehouse_handling_days_value = filter_input(INPUT_POST, $value['name'], FILTER_SANITIZE_STRING);
			update_option($value['name'], $warehouse_handling_days_value);
	  }

  	// Allow Saturday Delivery
		$allow_saturday_delivery_value = filter_input(INPUT_POST, $allow_saturday_delivery_name, FILTER_SANITIZE_STRING);
		update_option($allow_saturday_delivery_name, $allow_saturday_delivery_value);

		// Cutoff time
		$shipping_cutoff_time_value = filter_input(INPUT_POST, $shipping_cutoff_time_name, FILTER_SANITIZE_STRING);
		update_option($shipping_cutoff_time_name, $shipping_cutoff_time_value);

		// Hide Notes Box
		$hide_notes_box_value = filter_input(INPUT_POST, $hide_notes_box_name, FILTER_SANITIZE_STRING);
		update_option($hide_notes_box_name, $hide_notes_box_value);

		// Hide Company Name
		$hide_company_name_value = filter_input(INPUT_POST, $hide_company_name_name, FILTER_SANITIZE_STRING);
		update_option($hide_company_name_name, $hide_company_name_value);

		// Order Prefix
		$order_prefix_value = filter_input(INPUT_POST, $order_prefix_name, FILTER_SANITIZE_STRING);
		update_option($order_prefix_name, $order_prefix_value);

		// Order Suffix
		$order_suffix_value = filter_input(INPUT_POST, $order_suffix_name, FILTER_SANITIZE_STRING);
		update_option($order_suffix_name, $order_suffix_value);

  }

  $free_shipping_spend_value		= get_option($free_shipping_spend_name);
  
  $hide_standard_shipping_value		= get_option($hide_standard_shipping_name);

	$hide_shipping_name_value = array();
  foreach ($hide_shipping_name as $key => $value) {
  	$hide_shipping_name_value[$key]['field_name'] = $value['name'];
  	$hide_shipping_name_value[$key]['value'] = get_option($value['name']);
  	$hide_shipping_name_value[$key]['label'] = $value['label'];
  	$hide_shipping_name_value[$key]['type'] = $value['type'];
  }

	$shipping_delivery_days_value = array();
  foreach ($shipping_delivery_days_name as $key => $value) {
  	$shipping_delivery_days_value[$key]['hide_if_free_field_name'] = $value['if_free_name'];
  	$shipping_delivery_days_value[$key]['hide_if_free_value'] = get_option($value['if_free_name']);

  	$shipping_delivery_days_value[$key]['field_name'] = $value['name'];
  	$shipping_delivery_days_value[$key]['value'] = get_option($value['name']);
  	$shipping_delivery_days_value[$key]['label'] = $value['label'];
  	$shipping_delivery_days_value[$key]['type'] = $value['type'];
  }

	$warehouse_handling_days_value = array();
  foreach ($warehouse_handling_days_name as $key => $value) {
  	$warehouse_handling_days_value[$key]['hide_if_free_field_name'] = $value['if_free_name'];
  	$warehouse_handling_days_value[$key]['hide_if_free_value'] = get_option($value['if_free_name']);

  	$warehouse_handling_days_value[$key]['field_name'] = $value['name'];
  	$warehouse_handling_days_value[$key]['value'] = get_option($value['name']);
  	$warehouse_handling_days_value[$key]['label'] = $value['label'];
  	$warehouse_handling_days_value[$key]['type'] = $value['type'];
  }

  // foreach ($shipping_delivery_days_name as $key => $value) {
  // 	$shipping_delivery_days_value.$key['name'] = get_option($value['name']);
  // 	$shipping_delivery_days_value.$key['title'] = $value['label'];
  // }

  $free_shipping_message_line_1_value	= get_option($free_shipping_message_line_1_name);
  $free_shipping_message_line_2_value = get_option($free_shipping_message_line_2_name);
  $allow_saturday_delivery_value      = get_option($allow_saturday_delivery_name);
  $shipping_cutoff_time_value			    = get_option($shipping_cutoff_time_name);
  $hide_notes_box_value   				    = get_option($hide_notes_box_name);
  $hide_company_name_value				    = get_option($hide_company_name_name);
  $order_prefix_value							    = get_option($order_prefix_name);
  $order_suffix_value							    = get_option($order_suffix_name);

?>
  
<div class="wrap">
  <h1>Wilkinson Sword Options</h1>
  <form name="form1" method="post" action="">
    <table class="form-table">
      <tbody>
        <tr>
          <th scope="row">Shipping Methods</th>
          <td>
            <fieldset>
              <p>Hide the following Shipping Methods when <i>Free Shipping</i> is available </p>
              <br>
							<?php foreach ($hide_shipping_name_value as $key => $value): ?>
								<?php if ($value['type'] == 'free_shipping') continue; ?>
		            <input type="checkbox" id="<?php echo $value['field_name']; ?>" name="<?php echo $value['field_name']; ?>" value="1" <?php echo ($value['value'] == 1 ? 'checked' : '') ?>>
		            <label for="<?php echo $value['field_name']; ?>"><?php echo $value['label']; ?></label><br>
							<?php endforeach; ?>
            </fieldset>
          </td>
        </tr>
        <tr>
          <th scope="row">Shipping Info Box</th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span>Shipping Info Box</span></legend>
              <span class="flaticon-package"></span><input type="text" id="<?php echo $free_shipping_message_line_1_name; ?>" name="<?php echo $free_shipping_message_line_1_name; ?>" value="<?php echo $free_shipping_message_line_1_value; ?>">  <br>
              <input type="text" id="<?php echo $free_shipping_message_line_2_name; ?>" name="<?php echo $free_shipping_message_line_2_name; ?>" value="<?php echo $free_shipping_message_line_2_value; ?>"> <?php print get_woocommerce_currency_symbol(); ?><input type="number" min="0" step="1" id="<?php echo $free_shipping_spend_name; ?>" name="<?php echo $free_shipping_spend_name; ?>" value="<?php echo $free_shipping_spend_value; ?>">
            </fieldset>
          </td>
        </tr>
          <tr>
          <th scope="row">Shipping Estimate</th>
          <td>
            <fieldset>
						<p><code>Handling Days + Shipping Days + 1 day (if after cutoff time)</code></p>
              <legend class="screen-reader-text"><span>Shipping Estimate</span></legend>
								<h4>Warehouse Handling</h4>
	              <p class="description">How many days should we add for handling?</p>
	              <br>
			        	<?php foreach ($warehouse_handling_days_value as $key => $value): ?>
	                <input required type="number" id="<?php echo $value['field_name']; ?>" name="<?php echo $value['field_name']; ?>" value="<?php echo $value['value']; ?>">
		            	<label for="<?php echo $value['field_name']; ?>"><?php echo $value['label']; ?></label><br>
			        	<?php endforeach; ?>								
								<h4>Shipping Days</h4>
	                <p class="description">How many days should we add for shipping?</p>
                	<br>
			        	<?php foreach ($shipping_delivery_days_value as $key => $value): ?>
	                <input required type="number" id="<?php echo $value['field_name']; ?>" name="<?php echo $value['field_name']; ?>" value="<?php echo $value['value']; ?>">
		            	<label for="<?php echo $value['field_name']; ?>"><?php echo $value['label']; ?></label><br>
			        	<?php endforeach; ?>
                <br>
                <input type="time" id="<?php echo $shipping_cutoff_time_name; ?>" name="<?php echo $shipping_cutoff_time_name; ?>" value="<?php echo $shipping_cutoff_time_value; ?>">
                <label for="<?php echo $shipping_cutoff_time_name; ?>">Cutoff time for pick/pack same day</label><br><br>
                <input type="checkbox" id="<?php echo $allow_saturday_delivery_name; ?>" name="<?php echo $allow_saturday_delivery_name; ?>" value="1" <?php echo ($allow_saturday_delivery_value == 1 ? 'checked' : '') ?>>
               <label for="<?php echo $allow_saturday_delivery_name; ?>">Allow <i>Delivery Day</i> to be Saturday</label><br>
            </fieldset>
          </td>
        </tr>
        <tr>
          <th scope="row">Checkout</th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span>Checkout</span></legend>
                <input type="checkbox" id="<?php echo $hide_notes_box_name; ?>" name="<?php echo $hide_notes_box_name; ?>" value="1" <?php echo ($hide_notes_box_value == 1 ? 'checked' : '') ?>>
               <label for="<?php echo $hide_notes_box_name; ?>">Hide <i>Order notes</i> box</label><br>
                <input type="checkbox" id="<?php echo $hide_company_name_name; ?>" name="<?php echo $hide_company_name_name; ?>" value="1" <?php echo ($hide_company_name_value == 1 ? 'checked' : '') ?>>
               <label for="<?php echo $hide_company_name_name; ?>">Hide <i>Company Name</i> field</label><br>
            </fieldset>
          </td>
        </tr>
        <tr>
          <th scope="row">Orders</th>
          <td>
            <fieldset>
              <legend class="screen-reader-text"><span>Orders</span></legend>
                <input type="text" id="<?php echo $order_prefix_name; ?>" placeholder="Custom Prefix" name="<?php echo $order_prefix_name; ?>" value="<?php echo $order_prefix_value; ?>">
                <label for="<?php echo $order_prefix_name; ?>">Order Prefix</label><br>
                <input type="text" id="<?php echo $order_suffix_name; ?>" placeholder="Custom Suffix" name="<?php echo $order_suffix_name; ?>" value="<?php echo $order_suffix_value; ?>">
                <label for="<?php echo $order_suffix_name; ?>">Order Suffix</label><br>
            </fieldset>
          </td>
        </tr>
      </tbody>
    </table>
    <input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y"/>
    <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
  </form>
</div>