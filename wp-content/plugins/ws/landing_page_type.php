
// Register CPT for Landing pages
// function ws_landing_pages_init() {
//     $labels = array(
//         'name'                  => _x( 'Landing pages', 'Post type general name', 'textdomain' ),
//         'singular_name'         => _x( 'landing page', 'Post type singular name', 'textdomain' ),
//         'menu_name'             => _x( 'Landing pages', 'Admin Menu text', 'textdomain' ),
//         'name_admin_bar'        => _x( 'landing page', 'Add New on Toolbar', 'textdomain' ),
//         'add_new'               => __( 'Add New', 'textdomain' ),
//         'add_new_item'          => __( 'Add New landing page', 'textdomain' ),
//         'new_item'              => __( 'New landing page', 'textdomain' ),
//         'edit_item'             => __( 'Edit landing page', 'textdomain' ),
//         'view_item'             => __( 'View landing page', 'textdomain' ),
//         'all_items'             => __( 'All Landing pages', 'textdomain' ),
//         'search_items'          => __( 'Search Landing pages', 'textdomain' ),
//         'parent_item_colon'     => __( 'Parent Landing pages:', 'textdomain' ),
//         'not_found'             => __( 'No Landing pages found.', 'textdomain' ),
//         'not_found_in_trash'    => __( 'No Landing pages found in Trash.', 'textdomain' ),
//         // 'featured_image'        => _x( 'landing page Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
//         // 'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
//         // 'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
//         // 'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
//         'archives'              => _x( 'Landing page archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
//         'insert_into_item'      => _x( 'Insert into landing page', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
//         'uploaded_to_this_item' => _x( 'Uploaded to this landing page', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
//         'filter_items_list'     => _x( 'Filter Landing pages list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
//         'items_list_navigation' => _x( 'Landing pages list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
//         'items_list'            => _x( 'Landing pages list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
//     );

//     $args = array(
//         'labels'             => $labels,
//         'public'             => true,
//         'publicly_queryable' => true,
//         'show_ui'            => true,
//         'show_in_menu'       => true,
//         'menu_icon'          => 'dashicons-media-document',
//         'query_var'          => true,
//         '_builtin'           => false,
//         'rewrite'            => array( 'with_front' => false, 'slug' => '/' ),
//         'capability_type'    => 'post',
//         'has_archive'        => true,
//         'hierarchical'       => true,
//         'menu_position'      => null,
//         'supports'           => array( 'title', 'editor', 'author', 'thumbnail', ),
//     );

//     register_post_type( 'landing_page', $args );
// }

// function wpse_filter_handler($data, $postarr) {
//     if(!is_numeric(substr($data['post_name'], 0, 4))) {
//         $catids = $postarr['post_category'];
//         if(count($catids) > 1) {
//             $catobj = get_category($catids[1]);
//             $data['post_name'] = $catobj->slug . '-' . $data['post_name'];
//         }
//     }
//     return $data;
// }
// add_filter( 'wp_insert_post_data', 'wpse_filter_handler', 10, 2 );  

// add_action( 'init', 'ws_landing_pages_init' );