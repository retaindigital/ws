<?php

// Check if we're allowed to access the admin panel
// REPLACED BY EPC PLUGIN
function ws_check_admin_ip() {
  if( is_admin() && is_user_logged_in() ){

    // List of IPs allowed & who they are associated to 
    $allowed_ips = array(
      '127.0.0.1', # Localhost
      '::1', # Localhost
      '81.19.217.59', # Retain VPN
      '217.138.195.104', # Retain UK VPN
      '89.197.181.42', # P9 Hove
      '193.117.179.26', # Edgewell UK
      '80.241.69.149', #Edgewell DE
      '169.128.32.99', #Edgewell VPN
      '62.249.230.236', # Venture Blue
      '78.141.59.151', # Distinctly
      '109.123.90.13', # Distinctly VPN
      '185.28.78.5', # Content Fleet VPN
    );

    // Check if our current IP is allowed
    if( !in_array($_SERVER['REMOTE_ADDR'], $allowed_ips) ){
      // If it's not - send us home
      header('Location: '.get_home_url());
      exit;
    }
  }
}
// add_action( 'init', 'ws_check_admin_ip' );

// Geolocation must be enabled @ Woo Settings
function ws_use_geolocation(){

  // UK Test
  // $_SERVER['REMOTE_ADDR'] = '217.79.169.33';

  // DE Test
  // $_SERVER['REMOTE_ADDR'] = '91.207.172.78';

  // US Test
  // $_SERVER['REMOTE_ADDR'] = '104.206.206.115';

  // TESTING Set the cookie name 
  // $geo_location_cookie_name = 'ws_geo_location'.date('H:i:s');



  // return an error to stop the search happening 
  return array('status' => 'error');
  
  // Get the locations
  $location = WC_Geolocation::geolocate_ip();
  $country = $location['country'];

  // Set the current site location
  $current_site = ws_return_lang_loc();

  // Prep switch
  $switch = array();

  // Checks the country and sets the text for that country
  switch ($country) {
    case "GB":
      if( $current_site !== 'en-gb' ){
        $switch['url'] = 'en-gb';
        $switch['current'] = 'United Kingdom';
        $switch['to'] = 'UK';
        $switch['visiting_from_text'] = 'It looks like you may be visiting from';
        $switch['switch_to_text'] = 'Would you like to use to our<br>' . $switch['to'] . ' website instead?';
        $switch['go_button_text'] = 'Go to our ' . $switch['to'] . ' site';
        $switch['no_thanks_text'] = 'No thanks';
      }
      break;
    case "DE":
      if( $current_site !== 'de-de' ){
        $switch['url'] = 'de-de';
        $switch['current'] = 'Germany';
        $switch['to'] = 'German';
        $switch['visiting_from_text'] = 'It looks like you may be visiting from';
        $switch['switch_to_text'] = 'Would you like to use to our<br>' . $switch['to'] . ' website instead?';
        $switch['go_button_text'] = 'Go to our ' . $switch['to'] . ' site';
        $switch['no_thanks_text'] = 'No thanks';
      }
      break;
    default:
      $switch['url'] = 'en-gb';
      $switch['current'] = '';
      $switch['to'] = 'UK';
      $switch['visiting_from_text'] = '';
      $switch['switch_to_text'] = 'Visit our <br>' . $switch['to'] . ' website instead?';
      $switch['go_button_text'] = 'Go to our ' . $switch['to'] . ' site';
      $switch['no_thanks_text'] = 'No thanks';
      break;
  }


  // Main Multisite URL minus trailing slash 
  $network_url = rtrim(network_home_url(), '/');

  // Current Site Home URL
  $this_home_url = home_url();


  // If we have something set in $switch OR we're on the top-level odmain
  if( !empty($switch) || ($network_url == $this_home_url) ):
    // Replace Data
    $popup_template = file_get_contents(__DIR__.'/json/location_popup.inc');
    $popup_template = str_replace('{{VISITING_FROM}}', $switch['visiting_from_text'], $popup_template);
    $popup_template = str_replace('{{CURRENT}}',$switch['current'], $popup_template);
    
    if( $network_url !== $this_home_url )
      $popup_template = str_replace('{{SWITCH_TO}}', $switch['switch_to_text'], $popup_template);
    else
      $popup_template = str_replace('{{SWITCH_TO}}', '', $popup_template);

    $go_to_url = trailingslashit($network_url).trailingslashit($switch['url']);

    $popup_template = str_replace('{{GO_BUTTON_TEXT}}', $switch['go_button_text'], $popup_template);
    $popup_template = str_replace('{{NO_THANKS_TEXT}}', $switch['no_thanks_text'], $popup_template);

    return array('status' => 'success', 'offer' => $switch['url'], 'go_to_url' => $go_to_url, 'popup_code' => $popup_template);
  else:
    return array('status' => 'error');
  endif;

}


// Hide Free Shipping
function ws_hide_shipping_when_free_is_available( $rates ) {
  require($pluginBase . 'field_names.php');

  $to_show = array();

  foreach ( $rates as $rate_id => $rate ) {
    if ( 'free_shipping' === $rate->method_id ) {
      $free_active = 1;  
    } elseif ($free_active == 1 && get_option($if_free_prefix.$rate->instance_id) == 1){
      continue;
    }

    $to_show[ $rate_id ] = $rate;
  }

  return ! empty( $to_show ) ? $to_show : $rates;
}
// HIDE WHEN LIVE
add_filter( 'woocommerce_package_rates', 'ws_hide_shipping_when_free_is_available', 10, 1 );


// Delivery Estimate
function ws_print_delivery_estimate(){
  date_default_timezone_set('Europe/London');

  require($pluginBase . 'field_names.php');
  $method = WC()->session->get( 'chosen_shipping_methods' );
  $method_parts = explode(':', $method[0]);
  $shipping_method = $method_parts[1];

  // Get our custom options
  $wh_days = get_option($warehouse_handling_days_name_prefix.$shipping_method);
  $sh_days = get_option($shipping_delivery_days_name_prefix.$shipping_method);
  $deliver_on_sat = get_option($allow_saturday_delivery_name);
  $shipping_cutoff = get_option($shipping_cutoff_time_name);

  // Add our handling and shipping days together
  $total_days = $wh_days+$sh_days;
  
  // Check the cutoff time and add one day
  if( isset($shipping_cutoff) && strtotime(date('H:i')) > strtotime($shipping_cutoff))
    $total_days++;

  // Set the first delivery date
  $date = date('d-m-Y', strtotime("+ " . $total_days . " days"));

  // Check the delivery DAY of the week
  $day_for_delivery = date('w', strtotime($date));

  // If the day is Sunday or we're not allowing Saturday deliverey, shift to the Monday
  if( $day_for_delivery == 0 || ($deliver_on_sat != 1 && $day_for_delivery == 6) ){
    $date = date('d-m-Y', strtotime($date."+ 1 weekday"));
  }

  // Have we landed on a non delivery day?
  $finished_date = 0;
  $non_delivery_days = array(
    '25-12-2019',
    '26-12-2019',
    '27-12-2019',
    '01-01-2020'
  );

  // Check the date against non delivery days
  while ( $finished_date == 0 ) {
    // check if we have the date in the non delivery array and add one weekday if we do
    if( in_array($date, $non_delivery_days)  ){
      $date = date('d-m-Y', strtotime($date."+ 1 weekday"));
      $finished_date = 0;
    // if the date is ok, break the loop
    } else {
      $finished_date = 1;
    }
  }

  echo '<div class="estimated-delivery">' . esc_html__('Estimated Delivery Date','ws') . '<br><span>' . date_i18n(get_option('date_format'), strtotime($date), false) . '</span></div>';
}
// HIDE WHEN LIVE
add_action('woocommerce_before_shipping_calculator', 'ws_print_delivery_estimate', 10, 2);
add_action('woocommerce_review_order_before_submit', 'ws_print_delivery_estimate', 10, 2);



// Add order Prefix and/or Suffix
function change_woocommerce_order_number( $order_id ) {
  require($pluginBase . 'field_names.php');
  
  $order_prefix = get_option($order_prefix_name);
  $order_suffix = get_option($order_suffix_name);

  $prefix = $order_prefix;
  $suffix = $order_suffix;
  
  //remove # before from order id
  $order_id = str_ireplace("#", "", $order_id); 
  $new_order_id = $prefix . $order_id . $suffix;

  return $new_order_id;
}
// HIDE WHEN LIVE
add_filter( 'woocommerce_order_number', 'change_woocommerce_order_number' );



// Add free delivery banner on product page
function ws_before_add_to_cart(){
  require($pluginBase . 'field_names.php');
  $free_shipping_spend_value = get_option($free_shipping_spend_name);
  $free_shipping_message_line_1_value = get_option($free_shipping_message_line_1_name);
  $free_shipping_message_line_2_value = get_option($free_shipping_message_line_2_name);

  $free_shipping_spend_value_print = '';
  if($free_shipping_spend_value != ''){
    $free_shipping_spend_value_print = wc_price($free_shipping_spend_value, array('decimals' => 0));
  }

  echo '<div class="lebe-iconbox style-01 free-shipping">
  <div class="iconbox-inner">
  <div class="icon"> 
  <span class="flaticon-package"></span>
  </div>
  <div class="content">
  <h4 class="title">
  '.$free_shipping_message_line_1_value.'
  </h4>
  <div class="desc">
  '.$free_shipping_message_line_2_value.' ' . $free_shipping_spend_value_print . '
  </div>
  </div>
  </div>
  </div>';
}
// HIDE WHEN LIVE
add_action( 'woocommerce_after_add_to_cart_button', 'ws_before_add_to_cart' );



// Add Free delivery banner to mini cart
function ws_after_mini_cart(){
  require($pluginBase . 'field_names.php');
  $free_shipping_spend_value = get_option($free_shipping_spend_name);
  $free_shipping_message_line_1_value = get_option($free_shipping_message_line_1_name);
  $free_shipping_message_line_2_value = get_option($free_shipping_message_line_2_name);

  $free_shipping_spend_value_print = '';
  if($free_shipping_spend_value != ''){
    $free_shipping_spend_value_print = wc_price($free_shipping_spend_value, array('decimals' => 0));
  }

  print '<div class="delivery-banner">' . $free_shipping_message_line_1_value . ' ' . $free_shipping_message_line_2_value . ' ' . $free_shipping_spend_value_print . '</div>';
}
// HIDE WHEN LIVE
add_action( 'woocommerce_widget_shopping_cart_before_buttons', 'ws_after_mini_cart' );


// Add Product JSON
function ws_json_before_single_product_summary(){
  global $product;

  // Set Data
  $product_id = $product->get_ID();
  $product_short_description = strip_tags($product->get_short_description());
  $product_sku = $product->get_sku();
  $product_name = $product->get_name();
  $product_image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ));
  $product_URL = get_permalink( $product_id );
  $product_price = $product->get_price();
  $product_type = 'Product';

  // Replace Data
  $product_json_template = file_get_contents(__DIR__.'/json/product.inc');

  switch (ws_return_lang_loc()) {
    case 'en-gb':
      $product_json_template = str_replace('{{PRODUCT_CURRENCY}}', 'GBP', $product_json_template);
      break;
    case 'de-de':
      $product_json_template = str_replace('{{PRODUCT_CURRENCY}}', 'EUR', $product_json_template);
      break;
  }
  
  $product_json_template = str_replace('{{PRODUCT_TYPE}}', $product_type, $product_json_template);
  $product_json_template = str_replace('{{PRODUCT_DESC}}', $product_short_description, $product_json_template);
  $product_json_template = str_replace('{{PRODUCT_SKU}}', $product_sku, $product_json_template);
  $product_json_template = str_replace('{{BRAND_NAME}}', 'Wilkinson Sword', $product_json_template);
  $product_json_template = str_replace('{{PRODUCT_NAME}}', $product_name, $product_json_template);
  $product_json_template = str_replace('{{PRODUCT_IMG}}', $product_image[0], $product_json_template);
  $product_json_template = str_replace('{{PRODUCT_URL}}', $product_URL, $product_json_template);
  $product_json_template = str_replace('{{PRODUCT_PRICE}}', $product_price, $product_json_template);
  


  
  // Print JSON
  print $product_json_template;

}
// HIDE WHEN LIVE
add_action( 'woocommerce_before_single_product_summary', 'ws_json_before_single_product_summary');


// UNSET A SHIPPING METHOD FOR PACKAGE BASED ON THE SHIPPING CLASS(es) OF ITS CONTENTS
// HIDE WHEN LIVE
add_filter( 'woocommerce_package_rates', 'ws_hide_shipping_method_based_on_shipping_class', 10, 2 );
function ws_hide_shipping_method_based_on_shipping_class( $rates, $package )
{
    if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
        return;
    }

    foreach( $package['contents'] as $package_item ){ // Look at the shipping class of each item in package

        $product_id = $package_item['product_id']; // Grab product_id
        $_product   = wc_get_product( $product_id ); // Get product info using that id

        if( $_product->get_shipping_class_id() == 235 ){ // If we DO find this shipping class ID
            unset($rates['flat_rate:14']); // Then remove this shipping method
            break; // Stop the loop, since we've already removed the shipping method from this package
        }
    }
    return $rates;
}
// Show from price instead of price range on variable products
function wc_varb_price_range( $wcv_price, $product ) {
    $prefix = ( class_exists( 'WC_Subscriptions_Product' ) && WC_Subscriptions_Product::is_subscription( $product ) ) ?
      sprintf('%s', __('', 'ws')) ://prevent subscription product showing from twice.
      sprintf('%s: ', __('From', 'ws'));

    $wcv_reg_min_price = $product->get_variation_regular_price( 'min', true );
    $wcv_min_sale_price    = $product->get_variation_sale_price( 'min', true );
    $wcv_max_price = $product->get_variation_price( 'max', true );
    $wcv_min_price = $product->get_variation_price( 'min', true );

    $wcv_price = ( $wcv_min_sale_price == $wcv_reg_min_price ) ?
        wc_price( $wcv_reg_min_price ) :
        '<del>' . wc_price( $wcv_reg_min_price ) . '&nbsp;</del>' . '<ins>' . wc_price( $wcv_min_sale_price ) . '</ins>';

    return ( $wcv_min_price == $wcv_max_price ) ?
        $wcv_price :
        sprintf('%s%s', $prefix, $wcv_price);
}
// HIDE WHEN LIVE
add_filter( 'woocommerce_variable_sale_price_html', 'wc_varb_price_range', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'wc_varb_price_range', 10, 2 );

// Populate primary tag field based on tags attached to product
function acf_load_primary_tag_field_choices( $field ) {
    $product        = get_queried_object();
   // reset choices
    $field['choices'] = array();

    // get the textarea value from options page without any formatting
    $product_terms = get_the_terms($product, 'product_tag');
    if ($product_terms) {
      foreach ($product_terms as $product_term) {
        $choices[] = $product_term->name;
      }
      // loop through array and add to field 'choices'
      if( is_array($choices) ) {
          foreach( $choices as $choice ) {
              $field['choices'][ $choice ] = $choice;
          }
      }
    }

    // return the field
    return $field;

}

add_filter('acf/load_field/name=primary_product_tag', 'acf_load_primary_tag_field_choices');

// Get promo banner info
function get_promo_banner_info($productID){
  $product_terms          = get_the_terms($productID, 'product_tag');
  if ( isset($product_terms) && !empty($product_terms) ){
  
    $product_banner_colour  = get_field('promotional_banner_colour',$productID);
    $primary_tag_name       = get_field('primary_product_tag',$productID);
    $primary_tag            = get_term_by('name',$primary_tag_name,'product_tag',OBJECT);
    // Check if the primary tag is one of the product terms set for the product and use that. Otherwise just use the product terms first tag. 
    $promo_banner_info      = ( in_array($primary_tag,$product_terms) ) ?
                            get_field('promotional_banner', $primary_tag) :
                            get_field('promotional_banner', $product_terms[0]) ;

    $promo_banner_colour = ( isset($product_banner_colour) && !empty($product_banner_colour) ) ?
                            $product_banner_colour :
                            $promo_banner_info['promotional_banner_colour'] ;

    $promo_banner_copy  = ( $promo_banner_info['show_promotinonal_banner'] == 1 ) ?
                          '<span class="promo_banner" style="background-color:' . $promo_banner_colour . '">' . $promo_banner_info['banner_text'] . '</span>' :
                          '' ;
  }
  return $promo_banner_copy ? $promo_banner_copy : '';

}
//used instead of wc_get_gallery_image_html in order to add in the promo banner code in the right place. LL 11-25-19
function wc_get_gallery_image_with_banner_html( $attachment_id, $productID, $main_image = false ) {
  $promo_banner_copy = get_promo_banner_info($productID);
  $flexslider        = (bool) apply_filters( 'woocommerce_single_product_flexslider_enabled', get_theme_support( 'wc-product-gallery-slider' ) );
  $gallery_thumbnail = wc_get_image_size( 'gallery_thumbnail' );
  $thumbnail_size    = apply_filters( 'woocommerce_gallery_thumbnail_size', array( $gallery_thumbnail['width'], $gallery_thumbnail['height'] ) );
  $image_size        = apply_filters( 'woocommerce_gallery_image_size', $flexslider || $main_image ? 'woocommerce_single' : $thumbnail_size );
  $full_size         = apply_filters( 'woocommerce_gallery_full_size', apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' ) );
  $thumbnail_src     = wp_get_attachment_image_src( $attachment_id, $thumbnail_size );
  $full_src          = wp_get_attachment_image_src( $attachment_id, $full_size );
  $image             = wp_get_attachment_image( $attachment_id, $image_size, false, array(
      'title'                   => get_post_field( 'post_title', $attachment_id ),
      'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
      'data-src'                => $full_src[0],
      'data-large_image'        => $full_src[0],
      'data-large_image_width'  => $full_src[1],
      'data-large_image_height' => $full_src[2],
      'class'                   => $main_image ? 'wp-post-image' : '',
  ) );
  $imageTitle         = '<span>' . esc_html( get_the_title($attachment_id) ) . '</span>';

  return '<div data-thumb="' . esc_url( $thumbnail_src[0] ) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url( $full_src[0] ) . '">' . $image . $promo_banner_copy . '</a></div>';
}
// define the woocommerce_before_shop_loop_item_title callback
function action_woocommerce_before_shop_loop_item_title(  ) {
    global $product;

    $productID       = $product->get_id();
    echo get_promo_banner_info($productID);
};
add_action( 'woocommerce_before_shop_loop_item_title', 'action_woocommerce_before_shop_loop_item_title', 10, 0 );

function ws_acf_admin_enqueue_scripts() {

    // register script
    wp_register_script( 'ws-acf-input-js', get_stylesheet_directory_uri() . '/js/ws-acf-input.js', false, '1.0.0');
    wp_enqueue_script( 'ws-acf-input-js' );

}

add_action( 'acf/input/admin_enqueue_scripts', 'ws_acf_admin_enqueue_scripts' );

//optimise images
function get_image_sizes($imageID) {
  $thumbSmall = wp_get_attachment_image_src( $imageID, 'medium' );
  $thumbMedium = wp_get_attachment_image_src( $imageID, 'large' );
  $thumbLarge = wp_get_attachment_image_src( $imageID, 'large' );

  $urlSmall = $thumbSmall['0'];
  $urlMedium = $thumbMedium['0'];
  $urlLarge = $thumbLarge['0'];

  return array($urlSmall,$urlMedium,$urlLarge);
}


// // Add title into wc category page category product_short_description function woocommerce_taxonomy_archive_description() {
//   function woocommerce_taxonomy_archive_description() {
//     if ( is_product_taxonomy() && 0 === absint( get_query_var( 'paged' ) ) ) {
//       $term = get_queried_object();

//       if ( $term && ! empty( $term->description ) ) {
//         echo '<h3 class="term_name">' . $term->name . '</h3>';
//         echo '<div class="term-description">' . wc_format_content( $term->description ) . '</div>'; 
//       }
//     }
//   }

add_action( 'init', 'remove_lebe_countdown',30 );
function remove_lebe_countdown() {
  remove_action( 'woocommerce_single_product_summary', 'lebe_function_shop_loop_item_countdown', 21 );

}

// Filter for blog posts
// Register the custom query var so WP recognizes it
function ws_category_permalink_rewrite_add_query_vars( $vars ) {
    $vars[] = 'blog_cat';
    return $vars;
}
add_filter( 'query_vars', 'ws_category_permalink_rewrite_add_query_vars' );

function filter_blog_posts_by_category( $query ) {

 if ( $query->is_home() && $query->is_main_query() && ! is_admin() ) {
      $blog_cat    = get_query_var('blog_cat');

      // Let's change the query for category archives.
        // $query->set( 'category_name', $category );  
      $query->set( 'category_name', $blog_cat );

  }
}
add_action( 'pre_get_posts', 'filter_blog_posts_by_category' );

function get_the_blog_style(){
    $term = get_query_var( 'blog_cat' )  ? get_category_by_slug( get_query_var( 'blog_cat' )) : get_queried_object();

      if ( $term->parent > 0 ) {
          $term = get_term_by('id', $term->parent, 'category');
       } else {
          $term = get_term_by('id', $term->term_id, 'category');
       }


    $ws_blog_style  = get_field('blog_style','category_' . $term->term_id);

    return $ws_blog_style;
}
// Split text into 2 lines

function ws_split_text( $text ){
  $string1 = '';
  $string2 = '';

  $middle = strrpos(substr($text, 0, floor(strlen($text) / 2)), ' ') + 1;
  if ($middle >= 2){
    $string1 = substr($text, 0, $middle);
    $string2 = substr($text, $middle);
  } else {
    $string1 = $text;
  }

  $html = '<div class="panel_header">';
  $html .= '<span>' . $string1 . '</span>';
  $html .= $string2 ? '<span>' . $string2 . '</span>' : '';
  $html .= '</div>';

  return $html;

}
