<?php

add_action( 'rest_api_init', 'ws_custom_api' );
function ws_custom_api() {
  register_rest_route( 'wc/v2', '/url-by-sku', array( 'methods' => 'GET', 'callback' => 'ws_get_url_by_sku' ));
  register_rest_route( 'wc/v2', '/url-by-id', array( 'methods' => 'GET', 'callback' => 'ws_get_url_by_id' ));
  register_rest_route( 'wc/v2', '/get-location', array( 'methods' => 'GET', 'callback' => 'ws_use_geolocation' ));
}

function ws_get_url_by_sku( $request_data ) {
  $product_id = wc_get_product_id_by_sku($request_data['sku']);
  $product = wc_get_product($product_id);
  if( $product_id != '' && $product->get_status() == 'publish' ){
    return array('url' => get_permalink($product_id));
  }
  return array('error' => 'No Product Found');
}


function ws_get_url_by_id( $request_data ) {
  $id = $request_data['id'];

  // Check for an ID
  if( $id == '' )
    return array('error' => 'No ID');

  // Check if we're a post 
  if( !empty(get_post_type($id)) && get_post_status($id) == 'publish' )
    return array('url' => get_permalink($id));

  // Check if we're a term
  if( !empty(get_term($id)) )
    return array('url' => get_category_link($id));

  // If we've got nothing, return error
  return array('error' => 'Nothing Found');
}


/* Get multisite lang-location */
function ws_return_lang_loc(){
  if ( is_multisite() ){
    $url = network_site_url();
    $home = home_url();
    $location = str_replace($url, '', $home);

    // Check if we're on the top level of the domain
    if($home != $location)
      $location_data = rtrim($location, "/");
    else
      $location_data = 'top-level';
  } 
  return $location_data;  
}

// Revisions
function ws_modify_product_post_type( $args ) {
  $args['supports'][] = 'revisions';
  return $args;
}
add_filter( 'woocommerce_register_post_type_product', 'ws_modify_product_post_type' );



// Hide Checkout Fields
function ws_override_checkout_fields($fields) {
  require($pluginBase . 'field_names.php');
  $hide_notes = get_option($hide_notes_box_name);
  $hide_company_name = get_option($hide_company_name_name);
  
  if( $hide_notes == '1' )
    unset($fields['order']['order_comments']);


  if( $hide_company_name == '1' ) {
    unset($fields['billing']['billing_company']);
    unset($fields['shipping']['shipping_company']);
  }
  
  return $fields;
}
add_filter('woocommerce_checkout_fields','ws_override_checkout_fields');


// This might not be active!
function ws_hreflang_metadata() {
  
  // Get current blog ID
  $current_blog_id = get_current_blog_id();

  // Switch to the main blog ID
  switch_to_blog(1);

  // Check to see if we're allowing the plugin to output
  if ( get_field('field_ia_output_hreflang', 'option') == 1 ) {
    switch_to_blog($current_blog_id);
    return;
  }

  // Set us back to the original blog ID
  switch_to_blog($current_blog_id);

  if(is_paged() || !is_multisite() )
    return;

  // Get all sites
  $sites = get_sites();

  // Holder
  $site_info = array();

  // Loop sites
  foreach ($sites as $key => $value) {

    // Skip the main site
    if($value->path == '/')
      continue;

    // Set the multisite main URL
    $site_url = get_site_url($value->blog_id);
    
    // Set the location
    $site_info[$key]['blog_id'] = str_replace('/', '', $value->blog_id);

    // Set the location
    $site_info[$key]['lang'] = str_replace('/', '', $value->path);

    // Set the URL
    if( is_product() ){

      // Initiate product variable
      global $product;

      // Set the API URL for getting the other location URLs
      $site_info[$key]['url'] = $site_url . '/wp-json/wc/v2/url-by-sku?sku=' . str_replace(' ', '%20', $product->get_sku());

    } else {

      // If we're currently looping the requesting site, add the URL
      if( $site_info[$key]['lang'] == ws_return_lang_loc() ){

        // If we're viewing a taxonomy or main posts page
        if( is_tax() || is_product_category() || is_category() ) {
          // Create a final url
          $site_info[$key]['final_url'] = get_term_link(get_queried_object_id());
        } else {
          // Create a final url
          $site_info[$key]['final_url'] = get_permalink(get_queried_object_id());
        }

      } else {

        // Get the external post id from ACF and set the URL
        $field_name = 'hreflang_'.str_replace('-', '_', $site_info[$key]['lang']);
        
        // If we're viewing a taxonomy or main posts page
        if( is_tax() || is_product_category() || is_category() || is_home() ) {
          $external_post_id = get_field($field_name, get_queried_object());
        } else {
          $external_post_id = get_field($field_name);
        }

        // Don't add it if it's blank
        if( $external_post_id != '' ){
          $site_info[$key]['url'] = $site_url . '/wp-json/wc/v2/url-by-id?id=' . str_replace(' ', '%20', $external_post_id);
        } else {
          unset($site_info[$key]);
        }
      }
    }
  }

  // Only loop through and print hreflang tags if we have more than one entry
  if( count($site_info) > 1):

    // Loop all the site info
    foreach ($site_info as $key => $value):

      $url = $value['url'];

      // Check if we're on DEV
      if (strpos( $_SERVER['SERVER_NAME'], 'retain.dev' ) !== false) {

        // Ignore SSL - remove after testing
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

      }
      
      // If we're not looking at a final URL, go and get the final URL from the API URL
      if( !isset( $value['final_url']) ) {     
        
        // Grabs the URL info
        $get_url_json = file_get_contents($url,false, stream_context_create($arrContextOptions));

        // Decode the info
        $returned_json_object = json_decode($get_url_json);

        // Set the Final URL
        $final_url = $returned_json_object->url;

      } else {
        // Set the Final URL
        $final_url = $value['final_url'];
      }

      // If there's an error, skip this one
      if(isset($returned_json_object->error)){
        $href_langs = '';
        break;
      }

      $href_langs .= '<!-- WS IA -->';

      // Print links
      $href_langs .= '<link rel="alternate" hreflang="' .  $value['lang'] . '" href="' . $final_url . '" />';

      // make en-gb default
      if($value['lang'] == 'en-gb')
        $href_langs .= '<link rel="alternate" hreflang="x-default" href="' . $final_url . '" />';

    endforeach;

    print $href_langs;

  endif;
}
add_action( 'wp_head', 'ws_hreflang_metadata' );
