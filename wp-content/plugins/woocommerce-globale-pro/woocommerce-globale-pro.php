<?php
/**
 * Plugin Name: WooCommerce GlobalE PRO Integration
 * Plugin URI:
 * Description: WooCommerce GlobalE PRO Integration
 * Version: 1.0.28
 *
 */
error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));

if (!defined('GLOBALE_PRO_FILE')) {
	define('GLOBALE_PRO_FILE', __FILE__);
}

if ( version_compare( PHP_VERSION, '5.6.0', '>=' ) ) {
	require __DIR__ . '/includes/GlobaleProAutoloader.php';
//	if ( ! \Globale\Pro\GlobaleProAutoloader::init() ) {
//		return;
//	}
}

if ( ! class_exists( 'WC_GlobalE_Pro' ) ) :

class WC_GlobalE_Pro {

	/**
	* Construct the plugin.
	*/
	public function __construct() {
		add_action( 'plugins_loaded', array( $this, 'init' ) );
    }

	/**
	* Initialize the plugin.
	*/
	public function init() {

		// Checks if WooCommerce is installed.
		if ( class_exists( 'WC_Integration' ) ) {
			// Include our integration class.
			include_once 'includes/GlobaleProIntegration.php';

			// Register the integration.
			add_filter( 'woocommerce_integrations', array( $this, 'add_integration' ) );

			$this->init_session();
		} else {
			// throw an admin error if you like
		}
	}

	/**
	 * Add a new integration to WooCommerce.
	 */
	public function add_integration( $integrations ) {
		//$integrations[] = 'WC_GlobalE_Pro_Integration';
		$integrations[] = \Globale\Pro\GlobaleProIntegration::class;
		return $integrations;
	}

	private function init_session()
	{
		function woocommerce_loaded(){
			// Force setting of cart cookie, to ensure that session data is loaded
			if (has_action('woocommerce_set_cart_cookies','qwc_set_cart_cookies') ) {
				$prio = has_action('woocommerce_set_cart_cookies','qwc_set_cart_cookies');
				remove_action( 'woocommerce_set_cart_cookies', 'qwc_set_cart_cookies' );
				do_action('woocommerce_set_cart_cookies', true);
				if (is_int($prio)){
					add_action('woocommerce_set_cart_cookies', 'qwc_set_cart_cookies', $prio);
				} else {
					add_action('woocommerce_set_cart_cookies', 'qwc_set_cart_cookies');
				}
				// skip preload of cookie at first start, not ot die in \qwc_set_cart_cookies() on missing cart (plugin: qtranslate-xt)
			} else {
				do_action('woocommerce_set_cart_cookies', true);
			}
		}
		add_action('woocommerce_init', 'woocommerce_loaded');
	}


}

$WC_GlobalE_Pro = new WC_GlobalE_Pro( __FILE__ );

endif;
