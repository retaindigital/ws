<?php
namespace Globale\Pro;

use \Globale\Pro\Helper\Session;
use \Globale\Pro\Helper\Data;
use \Globale\Pro\Api\Cart;
use \Globale\Pro\Api\Order;
use \Globale\Pro\Admin\Order as AdminOrder;

class GlobalePro {

	public static $config = null;

    public static function init (){

		add_action( 'woocommerce_api_pro-cart-info',           \Globale\Pro\GlobalePro::class.'::apiCartInfo');
		add_action( 'woocommerce_api_pro-cart-clear',          \Globale\Pro\GlobalePro::class.'::apiCartClear');

		add_action( 'woocommerce_api_pro-order-create-update', \Globale\Pro\GlobalePro::class.'::apiUpdateCreate');
		add_action( 'woocommerce_api_pro-order-payment',       \Globale\Pro\GlobalePro::class.'::apiOrderPayment');
		add_action( 'woocommerce_api_pro-order-status',        \Globale\Pro\GlobalePro::class.'::apiOrderStatus');
		add_action( 'woocommerce_api_pro-order-shipping',      \Globale\Pro\GlobalePro::class.'::apiOrderShipping');
        add_action('wc_price_based_country_before_frontend_init', __CLASS__ . '::preSetCustomerLocation');

		if (self::isGlobaleRestApi()) {

			//add_filter('woocommerce_cookie', \Globale\Pro\Helper\Session::class.'::injectCookieForRestCall', 1, 1);
            \Globale\Pro\Helper\Session::injectCookieForRestCall(false);
            add_filter('woo_discount_rules_apply_rules', self::class.'::enableWooDiscountRules', 1, 1);
			self::activateCompatibilityFilters();

		} else {

			add_action( 'add_meta_boxes', AdminOrder::class.'::globaleBox' );
			add_action( 'woocommerce_order_number', \Globale\Pro\Model\Order::class.'::globaleOrderNumber', 10, 2);

			add_action('woocommerce_get_price_html', \Globale\Pro\Model\Product::class.'::productPriceHtml', 10, 2);

			add_filter('woocommerce_hidden_order_itemmeta',         \Globale\Pro\Model\Order\Item::class.'::hideOrderItemsMeta',  10, 1);
			add_filter('woocommerce_order_item_display_meta_key',   \Globale\Pro\Model\Order\Item::class.'::renameOrderItemMeta', 10, 3);
			add_filter('woocommerce_order_item_display_meta_value', \Globale\Pro\Model\Order\Item::class.'::formatOrderItemMeta', 10, 3);

            add_filter('woo_discount_rules_apply_rules', self::class.'::enableWooDiscountRules', 1, 1);

			self::activateEmailFilters();
			self::activateCompatibilityFilters();

		}
    }

    public static function initAfterWc()
	{
		if (self::isGlobaleRestApi() || self::isRestApi()) {

		} else {
			if (!Data::isAdminArea()) {
				// see function description
				Session::clearCart();
				Session::setCartCoookie();
			}
			//Session::setCartCoookie();
		}
	}

	public static function isRestApi()
	{
		$prefix = rest_get_url_prefix( );
		if (defined('REST_REQUEST') && REST_REQUEST // (#1)
			|| isset($_GET['rest_route']) // (#2)
			&& strpos( trim( $_GET['rest_route'], '\\/' ), $prefix , 0 ) === 0)
			return true;
		// (#3)
		global $wp_rewrite;
		if ($wp_rewrite === null) $wp_rewrite = new WP_Rewrite();

		// (#4)
		$rest_url = wp_parse_url( trailingslashit( rest_url( ) ) );
		$current_url = wp_parse_url( add_query_arg( array( ) ) );
		return strpos( $current_url['path'], $rest_url['path'], 0 ) === 0;
	}

	/**
	 * Checks if current request is an globale API call
	 * @return bool
	 */
    public static function isGlobaleRestApi()
    {
        $apiUrl = (strpos($_SERVER['REQUEST_URI'],'wc-api') !== false);
        $apiRequest = false;
        if($apiUrl) {
			if (isset($_GET['hash'])) {
				if ($_GET['hash']) {
					$apiRequest = true;
				}
			}
		}

        return $apiUrl && $apiRequest;
    }

    public static function isSessionApi()
	{
		$apiUrl = (strpos($_SERVER['REQUEST_URI'],'wc-api') !== false);
		$apiRequest = false;
		if($apiUrl) {
			if (!$_GET['hash'] && in_array($_GET['wc-api'], ['pro-cart-info', 'pro-cart-clear']) && $_COOKIE['wp_woocommerce_session_'.COOKIEHASH]) {
				$apiRequest = true;
			}
		}
		return $apiUrl && $apiRequest;
	}

	public static function isApplyGlobaleModifications()
	{
		return (
			(!self::isGlobaleRestApi() || ($_REQUEST['action'] == 'get_result_popup') || ($_REQUEST['action'] == 'get_results')) // search
			&& !Data::isAdminArea()
			&& !is_404()
			&& ($_SERVER['REQUEST_METHOD'] != 'PURGE')) ;
	}

	// Rest API
	public static function apiCartInfo(){
    	$cart = new Cart();
    	if (self::isGlobaleRestApi()) {
			$cart->info();
		} elseif (self::isSessionApi()) {
    		$cart->info(false);
		}
	}

	public static function apiCartClear(){
		$cart = new Cart();
		if (self::isGlobaleRestApi()) {
			$cart->clear();
		} elseif (self::isSessionApi()) {
			$cart->clear(false);
		}
	}

	public static function apiUpdateCreate(){
		$order = new Order();
		$order->create();
	}
	public static function apiOrderPayment(){
		$order = new Order();
		$order->payment();
	}
	public static function apiOrderStatus(){
		$order = new Order();
		$order->status();
	}
	public static function apiOrderShipping(){
		$order = new Order();
		$order->shipping();
	}

	// eof Rest API

	/**
	 * Before \wcpbc_get_woocommerce_country() starts - pre-set customer country from GlobalE cookie
	 * for fixed prices plugin
	 */
	public static function preSetCustomerLocation(){
		$customerCountry = \Globale\Pro\Helper\Session::getCountryCookie();
		if (null !== $customerCountry) {
			$customer = wc()->customer;
			$customer->set_billing_country($customerCountry);
			$customer->set_shipping_country($customerCountry);
		}
	}

	static public function activateEmailFilters()
	{
		add_filter( 'woocommerce_email_enabled_new_order', 					\Globale\Pro\GlobalePro::class.'::doSendEmail', 10, 2 );
		add_filter( 'woocommerce_email_enabled_cancelled_order', 			\Globale\Pro\GlobalePro::class.'::doSendEmail', 10, 2 );
		add_filter( 'woocommerce_email_enabled_customer_completed_order', 	\Globale\Pro\GlobalePro::class.'::doSendEmail', 10, 2 );
		add_filter( 'woocommerce_email_enabled_customer_invoice', 			\Globale\Pro\GlobalePro::class.'::doSendEmail', 10, 2 );
		add_filter( 'woocommerce_email_enabled_customer_note', 				\Globale\Pro\GlobalePro::class.'::doSendEmail', 10, 2 );
		add_filter( 'woocommerce_email_enabled_customer_on_hold_order', 	\Globale\Pro\GlobalePro::class.'::doSendEmail', 10, 2 );
		add_filter( 'woocommerce_email_enabled_customer_processing_order', 	\Globale\Pro\GlobalePro::class.'::doSendEmail', 10, 2 );
		add_filter( 'woocommerce_email_enabled_customer_refunded_order', 	\Globale\Pro\GlobalePro::class.'::doSendEmail', 10, 2 );
		add_filter( 'woocommerce_email_enabled_failed_order', 				\Globale\Pro\GlobalePro::class.'::doSendEmail', 10, 2 );

	}

    static public function doSendEmail( $enabled, $object ) {

		if ($object instanceof \WC_Order) {
			$globaleOrderId = get_post_meta($object->get_id(), 'globale_OrderId', true);
			if ($globaleOrderId) {

				$sendEmails = get_post_meta($object->get_id(), '_globale_Customer_SendConfirmation', true);
				if ($sendEmails == false) {
					$enabled = false;
				}

			}
		}

		return $enabled;
	}

	static public function activateCompatibilityFilters()
	{
		/** Aelia  */
		// \Aelia\WC\PricesByCountry\WC_Aelia_Prices_By_Country::get_customer_country()  --> wc_aelia_pbc_customer_country
		add_filter('wc_aelia_pbc_customer_country', __CLASS__.'::getCountry', 10, 1);
		add_filter('wc_aelia_cs_customer_country', __CLASS__.'::getCountry', 10, 1);
		// \Aelia\WC\CurrencySwitcher\WC_Aelia_CurrencySwitcher::get_selected_currency() --> wc_aelia_cs_selected_currency
		add_filter('wc_aelia_cs_selected_currency', __CLASS__.'::getCurrency', 10, 1);
		/** eof Aelia */
	}

	public static function getCountry($country)
	{

		if (\Globale\Pro\Helper\Session::getCountryCookie()) {
			$_COOKIE['aelia_customer_country'] = \Globale\Pro\Helper\Session::getCountryCookie();
		}
		$country = \Globale\Pro\Helper\Session::getCountryCookie() ? \Globale\Pro\Helper\Session::getCountryCookie() : $country;
		return $country;
	}

	public static function getCurrency($currency)
	{


		$currencyToSet = $currency;

		$globaleCurrency = \Globale\Pro\Helper\Session::getCurrencyCookie() ? \Globale\Pro\Helper\Session::getCurrencyCookie() : null;

		if ($globaleCurrency) {

			$aelia = $GLOBALS[\Aelia\WC\CurrencySwitcher\Definitions::PLUGIN_SLUG];
			$enabledCurrencies = $aelia->enabled_currencies();
			if (in_array($globaleCurrency, $enabledCurrencies )) {
				$currencyToSet = $globaleCurrency;
			} else {
				$currencyToSet = $aelia->base_currency();
			}
		}

		return $currencyToSet;

	}

	public static function enableWooDiscountRules($stat){
	    return true;
    }
}

add_action('woocommerce_init', [\Globale\Pro\GlobalePro::class, 'init']);
add_action('wp_loaded', [\Globale\Pro\GlobalePro::class, 'initAfterWc']);
add_action('woocommerce_loaded', [\Globale\Pro\GlobalePro::class, 'initAfterWc']);
add_filter('woocommerce_is_rest_api_request', [\Globale\Pro\GlobalePro::class, 'isRestApi']);
