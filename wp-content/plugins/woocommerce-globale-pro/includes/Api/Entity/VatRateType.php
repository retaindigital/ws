<?php
namespace Globale\Pro\Api\Entity;

use Globale\Pro\Api\Entity\Common;

/**
 * Class VatRateType
 *
 * @method getVATRateTypeCode()
 * @method getName()
 * @method getRate()
 * @method $this setVATRateTypeCode($VATRateTypeCode)
 * @method $this setName($Name)
 * @method $this setRate($Rate)
 */
class VatRateType extends Common {

	const UNKNOWN_VAT_RATE_TYPE_CODE = 'unknown';
	const UNKNOWN_VAT_RATE_NAME = 'Unknown';
	const UNKNOWN_VAT_RATE_RATE = null;

	/**
	 * Vat rate type code
	 * @var string
	 * @access public
	 */
	public $VATRateTypeCode;
	/**
	 * Vat rate name
	 * @var string
	 * @access public
	 */
	public $Name;
	/**
	 * Vat rate amount
	 * @var float
	 * @access public
	 */
	public $Rate;

	/**
	 * Pre-set with unknown defaults
	 * @return $this
	 */
	public function setUnknown(){
		$this->setVATRateTypeCode(self::UNKNOWN_VAT_RATE_TYPE_CODE);
		$this->setName(self::UNKNOWN_VAT_RATE_NAME);
		$this->setRate(self::UNKNOWN_VAT_RATE_RATE);
		return $this;
	}

}