<?php
namespace Globale\Pro\Api\Entity;

use Globale\Pro\Api\Entity\Common;

/**
 * Class GetCart
 *
 * @method getIsFreeShipping()
 * @method getUserId()
 * @method getFreeShippingCouponCode()
 * @method getShippingOptionsList()
 * @method @deprecated getCartHash()
 * @method getMerchantCartHash()
 *
 * @method $this setIsFreeShipping($IsFreeShipping)
 * @method $this setUserId($UserId)
 * @method $this setFreeShippingCouponCode($FreeShippingCouponCode)
 * @method $this setShippingOptionsList($ShippingOptionsList)
 * @method @deprecated $this setCartHash($CartHash)
 * @method $this setMerchantCartHash($CartHash)
 *
 */
class GetCart extends Common {

	/**
	 * @var Product[]
	 */
	public $productsList;

	/**
	 * @var Address
	 */
	public $shippingDetails;

	/**
	 * @var Address
	 */
	public $billingDetails;

	/**
	 * @var Discount[]
	 */
	public $discountsList;

	/**
	 * @var boolean
	 */
	public $IsFreeShipping;

	/**
	 * @var int
	 */
	public $UserId;


	/**
	 * @var string
	 * @deprecated - please use merchantCartHash
	 */
	public $CartHash;

	/**
	 * @var string
	 */
	public $merchantCartHash;

	/**
	 * @var string
	 */
	public $FreeShippingCouponCode;

	/**
	 * Shipping Option List
	 * @var ShippingOption
	 * @access public
	 */
	public $ShippingOptionsList;


	/**
	 * @return Product[]
	 */
	public function getProductsList()
	{
		return $this->productsList;
	}

	/**
	 * @param Product[] $productsList
	 * @return $this
	 */
	public function setProductsList($productsList)
	{
		$this->productsList = $productsList;
		return $this;
	}

	/**
	 * @return Address
	 */
	public function getShippingDetails()
	{
		return $this->shippingDetails;
	}

	/**
	 * @param Address $shippingDetails
	 * @return $this
	 */
	public function setShippingDetails($shippingDetails)
	{
		$this->shippingDetails = $shippingDetails;
		return $this;
	}

	/**
	 * @return Address
	 */
	public function getBillingDetails()
	{
		return $this->billingDetails;
	}

	/**
	 * @param Address $billingDetails
	 * @return $this
	 */
	public function setBillingDetails($billingDetails)
	{
		$this->billingDetails = $billingDetails;
		return $this;
	}

	/**
	 * @return Discount[]
	 */
	public function getDiscountsList()
	{
		return $this->discountsList;
	}

	/**
	 * @param Discount[] $discountsList
	 * @return $this
	 */
	public function setDiscountsList($discountsList)
	{
		$this->discountsList = $discountsList;
		return $this;
	}





}