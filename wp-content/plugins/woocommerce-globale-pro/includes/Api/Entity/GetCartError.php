<?php
namespace Globale\Pro\Api\Entity;

use Globale\Pro\Api\Entity\Common;

class GetCartError extends Common {

    /**
     * @var boolean
     */
    public $success = false;

    /**
     * @var string
     */
    public $errorMessage;

	/**
	 * GetCartError constructor.
	 * @param string $errorMessage
	 */
	public function __construct($errorMessage = '')
	{
		$this->setSuccess(false);
		$this->setErrorMessage($errorMessage);
	}

	/**
     * @return bool
     */
    public function isSuccess()
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return $this
     */
    public function setSuccess($success)
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     * @return $this
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
        return $this;
    }
}