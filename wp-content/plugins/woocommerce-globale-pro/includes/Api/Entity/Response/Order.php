<?php
/**
 * Created by PhpStorm.
 * User: SergeyGershkovich
 * Date: 28/03/2019
 * Time: 14:05
 */

namespace Globale\Pro\Api\Entity\Response;


use Globale\Pro\Api\Entity\Common;

/**
 * Class Order
 * @method getSuccess()
 * @method getMessage()
 * @method getOrderId()
 * @method getInternalOrderId()
 *
 *
 * @method $this setSuccess($Success)
 * @method $this setMessage($Message)
 * @method $this setOrderId($OrderId)
 * @method $this setInternalOrderId($InternalOrderId)
 *
 * @package Globale\Pro\Model\Entity\Response
 */
class Order extends Common
{
	/**
	 * Method success/unsuccess
	 * @var bool
	 */
	public $Success;

	/**
	 * Response message
	 * @var string
	 */
	public $Message;

	/**
	 * Order id
	 * @var string
	 */
	public $OrderId;

	/**
	 * Internal order id
	 * @var string
	 */
	public $InternalOrderId;

}