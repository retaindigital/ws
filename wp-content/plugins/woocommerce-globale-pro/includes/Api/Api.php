<?php
namespace Globale\Pro\Api;

class Api
{

	private $request;
	private $response;
	private $method;

	public function __construct(){
		$this->initRequestData();
	}

	public function log(){
		$tolog = array(
			'request' => $this->request,
			'response' => $this->response,
			'method' => $this->method,
		);

//        if(Config::getStoreConfig(Config::XML_PATH_LOG_ORDERS_ENABLED)) {
//            $logger = new Logger(Config::getStoreConfig(Config::XML_PATH_LOG_ORDERS_FILENAME));
//            $logger->log($tolog, 'globale-to-nununu');
//        }
	}

	public function initRequestData()
	{
		$post_data = file_get_contents("php://input");
		$decoded_post_data = json_decode($post_data);
		if($decoded_post_data){
			$data = $decoded_post_data;
		}
		else{
			parse_str($post_data, $data);
		}

		$this->request = array(
			'time' => date("Y-m-d H:i:s"),
			'GET' => $_GET,
			'POST' => $data
		);

	}

	public function sendResponse($response){
		$this->response = $response;
		ob_end_clean();

		header('Content-Type: application/json');
		echo json_encode($response);

		$this->log();
		die();
	}

	public function getRequestData()
	{
// TODO : fix merge of array GET with stdclass POST
		if (is_array($this->request['POST'])) {
			return array_merge($this->request['GET'], $this->request['POST']);
		} else {
			return $this->request['POST'];
		}
	}

	public function setMethod($method)
	{
		$this->method = $method;
	}

	/**
	 * @return mixed
	 */
	public function getMethod()
	{
		return $this->method;
	}

	public function getFromRequest($key)
	{
		$value = null;

		if (array_key_exists($key, $this->request['GET'])) {
			$value = $this->request['GET'][$key];
		} elseif (is_array($this->request['POST']) && array_key_exists($key, $this->request['POST'])) {
			$value = $this->request['POST'][$key];
		} elseif ($this->request['POST'] instanceof \stdClass && property_exists($this->request['POST'], $key)) {
			$value = $this->request['POST']->$key;
		}

		return $value;
	}
}