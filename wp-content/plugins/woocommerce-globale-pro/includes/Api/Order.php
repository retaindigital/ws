<?php
namespace Globale\Pro\Api;

use \Globale\Pro\Api\Api;
use \Globale\Pro\Config;
use \Globale\Pro\Model\Order as OrderModel;

class Order extends Api
{

	/**
	 * @param $req
	 */
	private function validateRequest($req)
	{

		if(empty($req)){
			$this->sendResponse([
                'Success' => 'false',
                'Message' => 'something went wrong with POST parameters',
                'ErrorCode'=>'System'
            ]);
		}

		if(!Config::validGUID($req->MerchantGUID)){
			$this->sendResponse([
			    'Success'         => 'false',
                'Message'         => "Merchant GUID is not valid",
                'ErrorCode'       => 'NotAuthorized',
                'MerchantOrderId' => $req->MerchantOrderId,
                'OrderId'         => $req->OrderId
            ]);
		}

	}

    public function create()
	{

		$this->setMethod('order-create');

		$req = $this->getRequestData();

		$this->validateRequest($req);

		$orderModel = new OrderModel();
		try {
			$orderModel->createOrderFromRequest($req);
			$orderModel->prepareOrderFromRequest($req);
		} catch (\Exception $e) {
			$this->sendResponse([
				'Success'         => 'false',
				'GlobalEOrderId'  => $req->OrderId,
				'Message'		  => $e->getMessage(),
			]);
		}

		$this->sendResponse([
            'Success'         => 'true',
            'InternalOrderId' => $orderModel->getWcOrder()->get_id(),
            'OrderId'         => $orderModel->getOrderId(),
            'GlobalEOrderId'  => $req->OrderId
        ]);

	}


    public function payment()
    {

        $this->setMethod('order-payment');
        $req = $this->getRequestData();

        $this->validateRequest($req);

		$orderModel = new OrderModel();
		try {
			$orderModel->initOrderFromRequest($req);
		} catch (\Exception $e) {
			$this->sendResponse([
				'Success' => 'false',
				'Message' => $e->getMessage()
			]);
		}

        // Log request
        update_post_meta( $orderModel->getOrderId(), '_globale_request_'.time().'_'.$this->getMethod(), $req );

        if (!$req->PaymentDetails) {
            $this->sendResponse([
                'Success' => 'false',
                'Message' => "Order->PaymentDetails is missing"
            ]);
        }

        if(!$orderModel->getWcOrder()->get_id()){
            $this->sendResponse([
                'Success' => 'false',
                'Message' => "Order update failed"
            ]);
        }

        $orderModel->doPayment($req);

        $this->sendResponse([
            'Success'         => 'true',
            'InternalOrderId' => $orderModel->getOrderId(),
            'OrderId'         => $orderModel->getOrderId(),
            'GlobalEOrderId'  => $req->OrderId,
            'Message'         => 'Order payment status updated'
        ]);
    }

    public function status()
    {

        $this->setMethod('order-status-update');
		$req = $this->getRequestData();

		$this->validateRequest($req);

		$orderModel = new OrderModel();
		$orderModel->initOrderFromRequest($req);

        // Log request
        update_post_meta( $orderModel->getOrderId(), '_globale_request_'.time().'_'.$this->getMethod(), $req );

        if (!$req->StatusCode) {
			$this->sendResponse([
                'Success'=>'false',
                'Message'=>"Order->StatusCode is missing"
            ]);
        }

		if (false === \Globale\Pro\Model\Order\Status::convertOrderStatus($req->StatusCode)){
			$this->sendResponse([
                'Success' => 'false',
                'Message' => "Order->StatusCode [".$req->StatusCode."] is invalid"
            ]);
        }

        $orderModel->getWcOrder()->update_status(
			\Globale\Pro\Model\Order\Status::convertOrderStatus($req->StatusCode),
			'Status updated by GlobalE'
        );

        $this->sendResponse([
            'Success'         => 'true',
            'InternalOrderId' => $orderModel->getOrderId(),
            'OrderId'         => $orderModel->getOrderId(),
            'GlobalEOrderId'  => $req->OrderId,
            'Message'         => 'Order status updated'
        ]);
    }



    public function shipping()
    {

        $this->setMethod('order-shipping-info-update');
		$req = $this->getRequestData();

		$this->validateRequest($req);

		$orderModel = new OrderModel();
		$orderModel->initOrderFromRequest($req);

        // Log request
        update_post_meta( $orderModel->getOrderId(), '_globale_request_'.time().'_'.$this->getMethod(), $req );

        if($req->InternationalDetails) {
            $orderModel->updateTracking($req);
        }else{
            $this->sendResponse([
                'Success' => 'false',
                'Message' => "Order->InternationalDetails is missing"
            ]);
        }

        $this->sendResponse([
            'Success'         => 'true',
            'InternalOrderId' => $orderModel->getOrderId(),
            'OrderId'         => $orderModel->getOrderId(),
            'GlobalEOrderId'  => $req->OrderId,
            'Message'         => 'Shipping updated'
        ]);
    }

}