<?php
namespace Globale\Pro\Api;

use \Globale\Pro\Api\Api;

use \Globale\Pro\Api\Entity\Address;
use \Globale\Pro\Api\Entity\GetCart;
use \Globale\Pro\Helper\Data;

use \Globale\Pro\Model\Cart as CartModel;


class Cart extends Api
{

	/**
	 * @param bool $rest
	 */
	public function info($rest = true){

		try {
			$cartModel = new CartModel($rest);
		} catch (\Exception $e) {
			$this->sendResponse([
				'Error' => true,
				'Code' => $e->getCode(),
				'Message' => $e->getMessage()
			]);
		}

		$cartInfo = new GetCart();
		$cartInfo->setProductsList($cartModel->buildCartProductsList());

		$cartInfo->setDiscountsList($cartModel->buildCartDiscounts());

		list ($freeShipping, $coupon) = $cartModel->getFreeShipping();
		$cartInfo->setIsFreeShipping($freeShipping);
		$cartInfo->setFreeShippingCouponCode($coupon);

		$cartInfo->setMerchantCartHash($cartModel->generateMerchantCartHash());
		$cartInfo->setShippingOptionsList();

		$userId = \WC()->session->get_customer_id();
		$cartInfo->setUserId($userId);
		if($userId) {
			$billing = $this->getAddress($userId, 'billing');
			$shipping = $this->getAddress($userId, 'shipping');
			$this->mixAddresses($billing, $shipping);
			$cartInfo->setBillingDetails($billing);
			$cartInfo->setShippingDetails($shipping);
		}

		$this->sendResponse($cartInfo);

	}

	/**
	 * @param bool $rest
	 */
	public function clear($rest = true)
	{

		try {
			$cartModel = new CartModel($rest);
			$cartModel->getWcCart()->empty_cart();
		} catch (\Exception $e) {
			$this->sendResponse([
				'Error' => true,
				'Code' => $e->getCode(),
				'Message' => $e->getMessage()
			]);
		}
		$this->sendResponse([
			'Success'         => 'true'
		]);
	}

	protected function getAddress($userId, $addressType) {

		$address = new Address();

		$address->setUserId($userId);

		$customer = new \WC_Customer( $userId, true );
		$address->setEmail($customer->get_email()?: null);
		$address->setFirstName($customer->get_first_name()?: null);
		$address->setLastName($customer->get_last_name()?: null);

		$firstName = get_user_meta( $userId, $addressType . '_first_name', true )?: null;
		if ($firstName) {
			$address->setFirstName($firstName);
		}

		$lastName = get_user_meta( $userId, $addressType . '_last_name', true )?: null;
		if ($lastName) {
			$address->setLastName($lastName);
		}

		$email = get_user_meta($userId, $addressType . '_email', true) ?: null;
		if($email) {
			$address->setEmail($email);
		}

		$address->setCompany(get_user_meta( $userId, $addressType . '_company', true )?: null);
		$address->setAddress1(get_user_meta( $userId, $addressType . '_address_1', true )?: null);
		$address->setAddress2(get_user_meta( $userId, $addressType . '_address_2', true )?: null);
		$address->setCity(get_user_meta( $userId, $addressType . '_city', true )?: null);
		$address->setCountryCode(get_user_meta( $userId, $addressType . '_country', true )?: null);
		$address->setStateOrProvince(get_user_meta( $userId, $addressType . '_state', true )?: null);
		$address->setZip(get_user_meta( $userId, $addressType . '_postcode', true )?: null);
		$address->setPhone1(get_user_meta($userId, 'billing_phone', true) ?: null);


		if ($addressType == 'billing'){
			$address->setIsBilling(true);
		} elseif ($addressType ==  'shipping') {
			$address->setIsShipping('true');
		}


		return $address;
	}

	protected function mixAddresses(Address $billing, Address $shipping) {

		$toMix = [
			'UserId', 'Email', 'FirstName', 'LastName', 'Company', 'Address1', 'Address2', 'City', 'CountryCode', 'StateOrProvince', 'Zip', 'Phone1'
		];

		foreach ($toMix as $prop) {
			$getter = 'get'.$prop;
			$setter = 'set'.$prop;
			if ($billing->$getter() && !$shipping->$getter()) {
				$shipping->$setter($billing->$getter());
			} elseif (!$billing->$getter() && $shipping->$getter()) {
				$billing->$setter($shipping->$getter());
			}
		}

	}

}