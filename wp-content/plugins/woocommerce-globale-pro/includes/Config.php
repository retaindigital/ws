<?php
namespace Globale\Pro;

class Config {

	/** Managed settings */
	const ENABLE_GEM_INCLUDE           = 'api_enabled';
    const MERCHANT_ID                  = 'api_merchant_id';
    const MERCHANT_GUID                = 'api_merchant_guid';
    const API_BASE_URL                 = 'api_path';
    const EXTRA_ATTR_LIST              = 'products_extra_attributes';
    const ATTRIBUTE_MAP                = 'products_attributes_map';
    const SKIP_CART_HASH_VALIDATION    = 'order_skip_cart_hash_validation';
    const INCLUDE_MODE                 = 'include_mode';
    const SHIPPING_MAPPING             = 'shipping_mapping';
    const SAVE_COUNTRY_NAME_AS         = 'save_country_name_as';
    const EXTRA_META_TO_ORDER_ITEM     = 'extra_meta_to_product_item';

    const INCLUDE_MODE_PRO = 'pro';
    const INCLUDE_MODE_GEM = 'gem';

    const SHIPPING_MAPPING_NONE    = 'none';
    const SHIPPING_MAPPING_PLUGIN  = 'plugin';
    const SHIPPING_MAPPING_GLOBALE = 'globale';

    const SAVE_COUNTRY_NAME_AS_NAME = 'name';
    const SAVE_COUNTRY_NAME_AS_CODE = 'code';

    /** System settings */
	const COOKIE_NAME_GEM_DATA = 'GlobalE_Gem_Data';
    const COOKIE_NAME_DEFAULT_GLOBALE_DATA = 'GlobalE_Data';
    const COOKIE_NAME_GLOBALE_DATA_KEY = 'cookie_name_globale_data_key';

    // ----
	const API_CACHE_FOLDER  = 'api_cache_folder';
	const API_CACHE_ENABLED = 'api_cache_enabled';
	/*
	LOG_API_ENABLED
	LOG_API_FILENAME
	API_SSL_REQUIRED
	CLIENT_SCRIPT_URL
	 */
    /**
     * Config
     * @var Config
     */
    private static $config;

    private static $attributesMap = null;

    protected static $shippingMethodMap = [
        'default' => 'globale'
    ];

    protected static $extraMetaToOrderItems = null;

    public function __construct(){}

    public static function initFromOptions($options = [])
	{
		self::$config = $options;
		self::$config[self::API_CACHE_FOLDER] = plugin_dir_path(dirname(__FILE__))  . 'cache/';
		self::$config[self::API_CACHE_ENABLED] = true;
	}

    public static function getMerchantId(){
    	return self::$config[self::MERCHANT_ID];
    }

    public static function getMerchantGuid(){
		return self::$config[self::MERCHANT_GUID];
    }

    public static function getApiBaseUrl(){
		return self::$config[self::API_BASE_URL];
    }

    public static function getEnableGemInclude(){
		return self::$config[self::ENABLE_GEM_INCLUDE];
    }

    public static function getExtraAttributeList(){
    	$extraAttributes = [];
    	$extraAttributesString = str_replace(' ', '', self::$config[self::EXTRA_ATTR_LIST]);
    	if (strlen($extraAttributesString)) {
    		$extraAttributes = explode(',', $extraAttributesString);
		}
		return $extraAttributes;
    }

    public static function getSkipCartValidation(){
		return self::$config[self::SKIP_CART_HASH_VALIDATION];
    }

    public static function getApiCacheFolder(){
		return self::$config[self::API_CACHE_FOLDER];
	}

	public static function getApiCacheEnabled(){
		return self::$config[self::API_CACHE_ENABLED];
	}

	public static function validGUID($guid)
	{

		if(mb_strtolower($guid,  mb_detect_encoding($guid)) === mb_strtolower(self::getMerchantGuid(),  mb_detect_encoding(self::getMerchantGuid()))){
		//if(strtolower($guid) === strtolower(self::getMerchantGuid())){
			return true;
		} else {
			error_log(
				"Global-e create order : validGUID : ".
				"got ".mb_detect_encoding($guid)." '".mb_strtolower($guid,  mb_detect_encoding($guid))."' not equal to config ".
				mb_detect_encoding(self::getMerchantGuid())." '".mb_strtolower(self::getMerchantGuid(),  mb_detect_encoding(self::getMerchantGuid()))."'"
			);
		}
		return false;
	}

    public static function getIncludeMode(){
        return self::$config[self::INCLUDE_MODE] ?
            self::$config[self::INCLUDE_MODE] :
            self::INCLUDE_MODE_PRO;
    }

    public static function isGemMode()
    {
        return self::getIncludeMode() == self::INCLUDE_MODE_GEM;
    }

    public static function getAttributesMap(){

        $map = [];
        if (self::$attributesMap !== null) {
            $map = self::$attributesMap;
        } else {
            $map = json_decode(self::$config[self::ATTRIBUTE_MAP], true);
            if (!(json_last_error() == JSON_ERROR_NONE)) {
                $map = [];
            }
            self::$attributesMap = $map;
        }
        return $map;
    }

    public static function useShippingMapping()
    {
        return in_array(self::$config[self::SHIPPING_MAPPING], [self::SHIPPING_MAPPING_PLUGIN, self::SHIPPING_MAPPING_GLOBALE]);
    }

    public static function isShippingMappingPlugin()
    {
        return self::$config[self::SHIPPING_MAPPING] == self::SHIPPING_MAPPING_PLUGIN;
    }

    public static function isShippingMappingNone()
    {
        return self::$config[self::SHIPPING_MAPPING] == self::SHIPPING_MAPPING_NONE;
    }

    public static function isShippingMappingGlobale()
    {
        return self::$config[self::SHIPPING_MAPPING] == self::SHIPPING_MAPPING_GLOBALE;
    }

    public static function mapShipmentMethod($method) {

        if (array_key_exists($method,  self::$shippingMethodMap)) {
            $method = self::$shippingMethodMap[$method];
        } else {
            $method = self::$shippingMethodMap['default'];
        }
        return $method;
    }

    public static function getGlobaleDataCookieName() {
        return self::$config[self::COOKIE_NAME_GLOBALE_DATA_KEY];
    }

    public static function getSaveCountryAs(){
        return self::$config[self::SAVE_COUNTRY_NAME_AS];
    }

    public static function getSaveCountryNameAsCode(){
        return self::getSaveCountryAs() == self::SAVE_COUNTRY_NAME_AS_CODE;
    }

    public static function getExtraMetaToOrderItem(){

        $extraMeta = [];
        if (self::$extraMetaToOrderItems !== null) {
            $extraMeta = self::$extraMetaToOrderItems;
        } else {
            $extraMeta = json_decode(self::$config[self::EXTRA_META_TO_ORDER_ITEM], true);
            if (!(json_last_error() == JSON_ERROR_NONE)) {
                $extraMeta = [];
            }
            self::$extraMetaToOrderItems = $extraMeta;
        }
        return $extraMeta;
    }

}