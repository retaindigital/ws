<?php
/**
 * Created by PhpStorm.
 * User: Vitali.Kashyn
 * Date: 13-Nov-19
 * Time: 17:17
 */

namespace Globale\Pro\Admin;


class Order
{

	public static function globaleBox()
	{
		add_meta_box(
			'woocommerce-globale-box',
			'GlobalE',
			self::class.'::createBox',
			'shop_order',
			'side',
			'default'
		);
	}

	public static function createBox()
	{

		global $post_id;

		$wcOrder = new \WC_Order($post_id);
		$items = $wcOrder->get_items();
		$subtotal = 0;
		$discountedSubtotal = 0;
		foreach($items as $item) {
		    $subtotal += ((float)$item->get_meta('_globale_InternationalPrice') * (float)$item->get_quantity());
		    $discountedSubtotal += ((float)$item->get_meta('_globale_InternationalDiscountedPrice') * (float)$item->get_quantity());
        }
        // $items[6]->get_meta('_globale_InternationalPrice')

		$globaleOrder = get_post_meta( $post_id, 'globale', true);

		$globaleOrderId        = get_post_meta( $post_id, 'globale_OrderId', true );
		$internationalPrice    = get_post_meta( $post_id, '_globale_InternationalDetails_TotalPrice', true );
		$internationalCurrency = get_post_meta( $post_id, '_globale_InternationalDetails_CurrencyCode', true );
		$internationalShipping = get_post_meta( $post_id, '_globale_InternationalDetails_TotalShippingPrice', true );
		$internationalShippingD = get_post_meta( $post_id, '_globale_InternationalDetails_DiscountedShippingPrice', true );
		$internationalDiscount = get_post_meta( $post_id, '_globale_DiscountForCustomer', true );
		$transactionCurrency = get_post_meta( $post_id, '_globale_InternationalDetails_TransactionCurrencyCode', true );
		$transactionAmount   = get_post_meta( $post_id, '_globale_InternationalDetails_TransactionTotalPrice', true );

		if( $globaleOrderId ) {
			?>
            <script type="text/javascript">
                function showHide(){
                    if (document.getElementById("debug-area").classList.contains('gle-off')){
                        document.getElementById("debug-area").classList.remove('gle-off');
                        document.getElementById("debug-area").classList.add('gle-on');
                        document.getElementById("show-link").classList.remove('gle-on');
                        document.getElementById("show-link").classList.add('gle-off');
                        document.getElementById("hide-link").classList.remove('gle-off');
                        document.getElementById("hide-link").classList.add('gle-on');
                    } else {
                        document.getElementById("debug-area").classList.remove('gle-on');
                        document.getElementById("debug-area").classList.add('gle-off');
                        document.getElementById("show-link").classList.remove('gle-off');
                        document.getElementById("show-link").classList.add('gle-on');
                        document.getElementById("hide-link").classList.remove('gle-on');
                        document.getElementById("hide-link").classList.add('gle-off');
                    }
                }
            </script>
            <style type="text/css">
                table.globale{
                    padding: 3px 3px 3px 3px;
                }
                tr.globale{
                    padding: 3px 3px 3px 3px;
                }
                textarea.globale{
                    min-width: 220px;
                }
                .gle-on{
                    display: inherit;
                }
                .gle-off{
                    display: none;
                }
                a.globale {
                    cursor: pointer;
                }
            </style>
			<table class="globale">
				<tr class="globale">
                    <td><h5>GlobalE Order #</h5></td>
                    <td><h5><?php echo $globaleOrderId; ?></h5></td>
				</tr>
                <tr class="globale">
                    <td><h5>International Price:</h5></td>
                    <td><h5><?php echo wc_price($internationalPrice, ['currency' => $internationalCurrency ]); ?></h5>
                        <!-- [ ST: <?=$subtotal;?> : DiscST <?=$discountedSubtotal;?>] --> </td>
                </tr>
                <tr class="globale">
                    <td><h5>Shipping Price:</h5></td>
                    <td><h5><?php echo wc_price($internationalShippingD, ['currency' => $internationalCurrency ]); ?></h5></td>
                </tr>
                <tr class="globale">
                    <td><h5>Discount for Customer:</h5></td>
                    <td><h5><?php echo wc_price($internationalDiscount, ['currency' => $internationalCurrency ]); ?></h5></td>
                </tr>
<!--                <tr class="globale">-->
<!--                    <td><h5>Total Payed:</h5></td>-->
<!--                    <td><h5>--><?php //echo wc_price($transactionAmount, ['currency' => $internationalCurrency ]); ?><!--</h5></td>-->
<!--                </tr>-->

                <tr class="globale">
					<td colspan="2">
                        <nobr>
                        <h5>GlobalE Request (debug) <a id="show-link" class="globale gle-on" onclick="showHide()">show</a><a id="hide-link" class="globale gle-off" onclick="showHide()">hide</a></h5>
                        </nobr>
                    </td>
                </tr>
                <tr class="globale">
                    <td colspan="2">
						<textarea style="font-size: 10px;" rows="10" class="globale gle-off" id="debug-area">
                            <?php echo $globaleOrder; ?>
                        </textarea>
					</td>
				</tr>
			</table>
			<?php
		}
	}
}