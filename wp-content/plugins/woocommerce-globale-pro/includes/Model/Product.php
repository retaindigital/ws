<?php
namespace Globale\Pro\Model;

use \Globale\Pro\Helper\Data;

class Product
{
    private $_rate_applied = array();

    /**
	 * Not taxable "Tax status"
	 * @var array
	 */
    protected static $notTaxable = ['shipping', 'none'];
    /*
     * @var \WC_Product
     */
    public $wcProduct;

    function __construct($productId, $sku=false){
        $this->load($productId, $sku?'sku':null);
    }

   /*
    * @TODO: fixme?
   */
    public function getDoNotAdjust(){
        return false;
    }

    public function getCategories(){
        return get_the_terms( $this->wcProduct->get_id(), 'product_cat' );
    }

    public function getTags(){
        $tags_array = array();

        $tags = get_the_terms( $this->wcProduct->get_id(), 'product_tag' );
        if (!empty($tags)) {
			foreach ($tags as $tag) {
				$tags_array[] = $tag->name;
			}
		}

        return implode(',', $tags_array);
    }

    public function isOnSale(){
        return $this->wcProduct->is_on_sale();
    }

    public function wcProductLoad($productId, $field)
    {
        if($field == 'sku'){
            $productId = wc_get_product_id_by_sku($productId);
        }

        $_pf = new \WC_Product_Factory();
        return $_pf->get_product($productId);
    }

    public function load($productId,$field=null)
    {
        $product = $this->wcProductLoad($productId,$field);

        $this->wcProduct = $product;

        return $product;
    }

    /**
     * Check if product special price is valid for the current date
     */
    public function SpecialPriceIsValid()
    {
        return $this->isOnSale();
    }


    public function isSuperGroup()
    {
        return $this->wcProduct->is_type('grouped');
    }

    public function isConfigurable()
    {
        return $this->wcProduct->is_type('variable');
    }

    public function isVariation()
    {
        return $this->wcProduct->is_type('variation');
    }

    public function isBundle()
    {
        if ($this->wcProduct
        && get_class($this->wcProduct) == "WC_Product_Yith_Bundle")
            // do not use ` instance of WC_Product_Yith_Bundle ` because it might nor be defined
            // or we can use method_exists($this->wc_product,"get_bundled_items")
        {

            $v = $this->wcProduct->get_bundled_items();
            return !empty($v);
        }
        return false;
    }

    public function getShortDescription()
    {
        return $this->wcProduct->get_post_data()->post_excerpt;
    }

    public function getSku()
    {
        return $this->wcProduct->get_sku();
    }

    // @TODO: fixme
    private function setShortDescription($short_desc)
    {
        // modify description
        // $this->wc_product...
//        add_filter( 'woocommerce_short_description', function( $short_desc ) { return '<b>' . $title . '</b>'; } );

    }

    public function has_child()
    {
        return $this->wcProduct->has_child();
    }

    /**
     * @return Product[]
     */
    public function get_children()
    {
        $children = array();

        $product_children = $this->wcProduct->get_children();

        foreach($product_children as $product_child){
            $children[] = new Product($product_child);
        }

        return $children;
    }

    public function getId()
    {
        return $this->wcProduct->get_id();
    }

    public function getVariationId()
    {
        return $this->wcProduct->get_variation_id();
    }

    public function getName()
    {
        return $this->wcProduct->get_title();
    }

    public function getProductUrl()
    {
        return $this->wcProduct->get_permalink();
    }

    public function getWeight()
    {
        return $this->wcProduct->get_weight();
    }

    public function getHeight()
    {
        return $this->wcProduct->get_height();
    }

    public function getWidth()
    {
        return $this->wcProduct->get_width();
    }

    public function getLength()
    {
        return $this->wcProduct->get_length();
    }

    public function getImageURL()
    {
    	$imgSrc = false;
    	if ($this->isVariation()) {
			$imgSrc = wp_get_attachment_image_src($this->wcProduct->get_image_id());
		}
		if (!$imgSrc) {
			$postId = $this->wcProduct->get_post_data()->ID;
			$imgSrc = wp_get_attachment_url(get_post_thumbnail_id($postId));
		}
        return $imgSrc;
    }

    public function getImageData()
    {
    	$imgId = false;
		if ($this->isVariation()) {
			$imgId = $this->wcProduct->get_image_id();
		}
		if (!$imgId) {
			$postId = $this->wcProduct->get_post_data()->ID;
			$imgId = get_post_thumbnail_id($postId);
		}
        return wp_get_attachment_image_src( $imgId);
    }

    /**
     * @return float|string
     */
    public function getListPrice()
    {
        $taxDisplayMode = get_option('woocommerce_tax_display_shop');

        return 'incl' === $taxDisplayMode
            ? wc_get_price_including_tax($this->wcProduct, ['price' => $this->wcProduct->get_regular_price()])
            : wc_get_price_excluding_tax($this->wcProduct, ['price' => $this->wcProduct->get_regular_price()]);
    }

    public function getOriginalListPrice()
    {
	    if($this->isVariation() ) {
		    $post_id = $this->getVariationId();
	    } else {
		    $post_id = $this->getId();
	    }

	    return get_post_meta($post_id, '_regular_price', true);
    }

    /**
     * Stupid, but quick
     * as woo-discount-ruled do not have any "get discounted price" functionality, which can be triggered directly,
     * only "html-based" hook `woocommerce_get_price_html` - will use this one, for quick solution
     * support for woo-discount-rules
     */
    public function checkForWooDiscount($price) {

        $dirty = apply_filters('woocommerce_get_price_html', $price, $this->wcProduct, 1);
        $delimiter = wc_get_price_decimal_separator();

        $dirty = strstr($dirty, $price);
        $doubled = preg_replace("/[^0-9".$delimiter."]/", "", $dirty);
        $single = substr_replace($doubled, '', 0, strlen($price));

        if((float)$single > 0 && (float)$price != (float)$single) $price = (float)$single;

        return $price;
    }

    /**
     * @return float|string
     */
    public function getSalePrice()
    {
        $taxDisplayMode = get_option('woocommerce_tax_display_shop');
        $sale_price = 'incl' === $taxDisplayMode
            ? wc_get_price_including_tax($this->wcProduct, ['price' => $this->wcProduct->get_sale_price()])
            : wc_get_price_excluding_tax($this->wcProduct, ['price' => $this->wcProduct->get_sale_price()]);
        /**
         * We can get $sale_price as valid float>0 or '' (empty string), 0 (integer zero), null, "0" (string zero)
         * Here we need to support both cases:
         * 1) defined in admin "0" sale price (some goods can be sold for free)
         * 2) when sale price is not defined (but not 0), sale price is to be taken from list price
         */
        if (!($this->SpecialPriceIsValid() && ((string)$sale_price === "0" || (float)$sale_price > 0))) {
            $sale_price = $this->getListPrice();
        }

        $sale_price = $this->checkForWooDiscount($sale_price);

        return $sale_price;
    }

    public function getOriginalSalePrice()
    {
	    if ( $this->isVariation() ) {
		    $post_id = $this->getVariationId();
	    } else {
		    $post_id = $this->getId();
	    }
	    $sale_price = get_post_meta( $post_id, '_sale_price', true );

//	    if ( ! ( $sale_price > 0 && $this->SpecialPriceIsValid() ) ) {
//		    $sale_price = $this->getOriginalListPrice();
//	    }
		if (!$this->SpecialPriceIsValid()) {
			$sale_price = $this->getOriginalListPrice();
		}

	    return $sale_price;
    }

    public function getSalePriceBeforeRounding()
    {
        $sale_price = $this->getOriginalSalePrice();

        return $sale_price;
    }

    public function getAttributeRawValue($attr_name)
    {
        return $this->wcProduct->get_attribute($attr_name);
    }

    public function getTaxClass(){
    	return $this->wcProduct->get_tax_class();
	}

	public function getTaxRate(){
		$productTaxClass = $this->wcProduct->get_tax_class();
		$taxRates = \WC_Tax::get_rates($productTaxClass);
		return current($taxRates);
	}

	public function isTaxable(){
		$taxStatus = $this->wcProduct->get_tax_status();
		$taxable = true;
		if(in_array($taxStatus, self::$notTaxable)){
			$taxable = false;
		}
		return $taxable;
	}

	public function isVirtual(){
		$isVirtual = null;
		$isVirtual = $this->wcProduct->get_virtual();
		return $isVirtual;
	}

	/**
	 * <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">&pound;</span>30.00</span>
	 * @param $price
	 * @param $product
	 * @return mixed
	 */
	public static function productPriceHtml($price, $product)
	{
		$vat = 0;
		$taxStatus = $product->get_tax_status();
		if(!in_array($taxStatus, self::$notTaxable)){
			$taxClass = $product->get_tax_class();
			$taxRates = \WC_Tax::get_rates($taxClass);
			$rate = current($taxRates);
			if (!empty($rate)) {
				$vat = $rate['rate'];
			}
		}

		$price = str_replace("<span class=\"woocommerce-Price-amount", '<span data-product-vat="'.$vat.'" class="woocommerce-Price-amount', $price);
		return $price;
	}
}

