<?php
namespace Globale\Pro\Model;

use \Globale\Pro\Config;
use \Globale\Pro\Model\Order\Status;

class Order
{

	const ORDER_META_MERCHANT_CART_HASH = '_globale_merchant_cart_hash';

	/** @var  \WC_Order */
	protected $wcOrder;

	protected $orderId;

	protected $acceptedDiscountTypes = [1];

	/**
	 * @return \WC_Order
	 */
	public function getWcOrder()
	{
		return $this->wcOrder;
	}

	/**
	 * @return mixed
	 */
	public function getOrderId()
	{
		return $this->orderId;
	}


	/**
	 * @param $req
	 * @return $this
	 * @throws \Exception
	 */
	public function initOrderFromRequest($req)
	{
		$orderId = $req->MerchantOrderId;
		if(!$orderId){
			$orderId = $req->CartId;
		}

		$this->orderId = $orderId;

		$wcOrder = new \WC_Order($orderId);

		$globaleOrderId = get_post_meta($wcOrder->get_id(), 'globale_OrderId', true);
		if ($globaleOrderId != $req->OrderId) {
			$globaleOrderIds = get_post_meta($wcOrder->get_id(), 'globale_OrderId');
			throw new \Exception(
				"Error loading order for ['".$req->OrderId."'', '".$orderId."']".
				" got ['" . $globaleOrderId . ":" . json_encode($globaleOrderIds) . "', '".$wcOrder->get_id()."']");
		}

		$this->wcOrder = $wcOrder;

		return $this;
	}

	/**
	 * @param $req
	 * @return $this
	 * @throws \Exception
	 */
	public function createOrderFromRequest($req)
	{
		$orderId = $req->MerchantOrderId;

//      cart will be deleted before create order request will arrive
//		$cartModel = new Cart();
//		$cartModel->initRest();
//		if (!$cartModel->validateMerchantCartHash($req->CartId)){
//			throw new \Exception('Invalid Merchant Cart Hash'."  vs ".$cartModel->getWcCart()->get_cart_hash());
//		}

		$this->orderId = $orderId;
		$orderData = [
			//'order_id' => $orderId,
			//'customer_id' => \WC()->session->get_customer_id(),
			'customer_id' => $req->UserId
			//'cart_hash' => null,
		];

		if(isset($req->CustomerComments)) {
			$orderData['customer_note'] = $req->CustomerComments;
		}

		$wcOrder = wc_create_order($orderData);
		update_post_meta( $wcOrder->get_id(), 'globale_OrderId', $req->OrderId );

		if($wcOrder instanceof \WC_Order) {
			$this->wcOrder = $wcOrder;
		} else if ($wcOrder instanceof \WP_Error) {
			throw new \Exception($wcOrder->get_error_message());
		}

		return $this;
	}

	/**
	 * @return $this
	 */
	public function clearOrder()
	{
		$this->wcOrder->remove_order_items();
		return $this;
	}

	public function prepareOrderFromRequest($req)
	{
		// Add to MailChimp maillist
		if(isset($req->AllowMailsFromMerchant) && $req->AllowMailsFromMerchant){
			$subscribe = 'yes';
		}else{
			$subscribe = 'no';
		}
		update_post_meta( $this->wcOrder->get_id(), 'ss_wc_mailchimp_opt_in', $subscribe );

		// Status
		$statusCode = $req->StatusCode;
		$wcStatus = Status::convertOrderStatus($statusCode);
		if($wcStatus !== false){
			$this->wcOrder->update_status($wcStatus, 'Status updated during GlobalE order creation');
		}

		// Payment
		if(isset($req->PaymentDetails)){
			$this->doPayment($req);
		}

		if(isset($req->InternationalDetails)) {
			$this->updateTracking($req);
			$this->updatePayment($req);
		}

		if(isset($req->CustomerComments)) {
			$this->wcOrder->add_order_note($req->CustomerComments, 1);
		}

		// Address
		if($req->Customer->IsEndCustomerPrimary){
			$billing  = $req->PrimaryBilling;
			$shipping = $req->PrimaryShipping;
		}
		else{
			$billing  = $req->SecondaryBilling;
			$shipping = $req->SecondaryShipping;
		}
		$billing_address = $this->getAddress($billing);
		$shipping_address = $this->getAddress($shipping);

		$this->wcOrder->set_address( $billing_address, 'billing' );
		$this->wcOrder->set_address( $shipping_address, 'shipping' );

		$this->processOrderItems($req)
			->processDiscounts($req)
			->processFreeShipping($req)
			->processMeta($req);

		if(isset($req->ClearCart)) {
			list ($sessionKey, $hash) = explode('-', $req->CartId);
			update_post_meta($this->wcOrder->get_id(), '_globale_ClearCart', $req->ClearCart);
			update_post_meta($this->wcOrder->get_id(), self::ORDER_META_MERCHANT_CART_HASH, $hash );
		}

		return $this;
	}

	/**
	 * @param $req
	 * @return $this
	 */
	public function doPayment($req)
	{
		$this->wcOrder->payment_complete();
		update_post_meta( $this->wcOrder->get_id(), '_globale_paymentDetails', $req->PaymentDetails);
		$this->updatePayment($req);
		return $this;
	}

	/**
	 * @param $req
	 * @return $this
	 */
	private function updatePayment($req)
	{
		$code = $req->InternationalDetails->PaymentMethodCode;
		$title = $req->InternationalDetails->PaymentMethodName;

		update_post_meta( $this->wcOrder->get_id(), '_payment_method', $code );
		update_post_meta( $this->wcOrder->get_id(), '_payment_method_title', $title );

		return $this;
	}

	/**
	 * @param $req
	 * @return $this
	 */
	public function updateTracking($req)
	{
		if(isset($req->InternationalDetails->OrderTrackingNumber)){
			update_post_meta( $this->wcOrder->get_id(), '_order_trackno', $req->InternationalDetails->OrderTrackingNumber );
			update_post_meta( $this->wcOrder->get_id(), 'tracking_number', $req->InternationalDetails->OrderTrackingNumber );
		}

		if(isset($req->InternationalDetails->OrderTrackingURL)){
			update_post_meta( $this->wcOrder->get_id(), '_order_trackurl', urldecode($req->InternationalDetails->OrderTrackingURL));
		}

		return $this;
	}

	/**
	 * @param $addressData
	 * @return array
	 */
	private function getAddress($addressData)
	{
		foreach($addressData as $key => $value){
			$addressData->$key = urldecode($value);
		}

		$address = array(
			'first_name'  => $addressData->FirstName,
			'last_name'   => $addressData->LastName,
			'middle_name' => $addressData->MiddleName,
			'salutation'  => $addressData->Salutation,
			'company'     => $addressData->Company,
			'address_1'   => $addressData->Address1,
			'address_2'   => $addressData->Address2,
			'city'        => $addressData->City,
			'state_code'  => $addressData->StateCode,
			'state'       => $addressData->StateOrProvince,
			'postcode'    => $addressData->Zip,
			'email'       => $addressData->Email,
			'phone'       => $addressData->Phone1,
			'phone_2'     => $addressData->Phone2,
			'fax'         => $addressData->Fax,
			'country'     => Config::getSaveCountryNameAsCode() ? $addressData->CountryCode : $addressData->CountryName,
			'country_code'=> $addressData->CountryCode
		);

		return $address;
	}

	/**
	 * @param $req
	 * @return $this
	 */
	private function processOrderItems($req)
	{
        $productsData = $req->Products;

		// looping products to calculate totals and taxes
		foreach ($productsData as $product) {
			// calculate taxes
			$_tax = 1 + $product->VATRate / 100;

			$args = array();

            $singleProductTax = $product->Price * 100 / (100 + $product->VATRate);
            $productTax = $singleProductTax + $product->Quantity;

            $args['totals']['subtotal_tax'] = $singleProductTax;
            $args['totals']['tax'] = $singleProductTax;

            $rowTotal = $product->Price * 100 / (100 + $product->VATRate ) * $product->Quantity;
            $args['totals']['total']    = $rowTotal;
            $args['totals']['subtotal'] = $rowTotal;

			/**
			 * @var WC_Product_Variation|WC_Product_Simple $wcProduct
			 */
			$product->Sku = urldecode($product->Sku);
			$wcProduct = wc_get_product(wc_get_product_id_by_sku($product->Sku));
			if (!($wcProduct instanceof \WC_Product)) {
				$wcProduct = wc_get_product($product->Sku);
			}
			if (!($wcProduct instanceof \WC_Product)) {
				throw new \Exception("Can't finalize order creation, because unable to load requested product by SKU:'".$product->Sku."'");
			}

			$itemId = $this->wcOrder->add_product($wcProduct, $product->Quantity, $args);

			wc_add_order_item_meta($itemId, '_globale_product_data', $product);
			wc_add_order_item_meta($itemId, '_globale_InternationalPrice', $product->InternationalPrice);
			wc_add_order_item_meta($itemId, '_globale_InternationalDiscountedPrice', $product->InternationalDiscountedPrice);

			if (Config::getExtraMetaToOrderItem()) {
			    foreach(Config::getExtraMetaToOrderItem() as $key => $value) {
                    wc_add_order_item_meta($itemId, $key, $value);
                }
            }

			if ($wcProduct->is_type('variation')) {
				$attr = $wcProduct->get_variation_attributes();

				foreach ($attr as $attrName => $attrValue) {
					$attrNewName = preg_replace('/^attribute_/', '', $attrName);

					if ($attrNewName) {
						wc_add_order_item_meta($itemId, $attrNewName, $attrValue, true);
					}
				}
			}
		}

        $shipping = new \WC_Order_Item_Shipping();

		if(Config::isShippingMappingGlobale()) {
            $shipping->set_method_title($req->InternationalDetails->ShippingMethodCode);
        } elseif (Config::isShippingMappingPlugin()) {
            $shipping->set_method_title(Config::mapShipmentMethod($req->ShippingMethodCode));
        } elseif (Config::isShippingMappingNone()) {
            $shipping->set_method_title('Global-e : ' . $req->InternationalDetails->ShippingMethodName);
        } else {
            $shipping->set_method_title(Config::mapShipmentMethod($req->ShippingMethodCode));
        }
		$shipping->set_method_id('globale');
        $this->wcOrder->add_item($shipping);

        $this->wcOrder->add_item($shipping);

		// TODO: following call Will remove taxes data
		$this->wcOrder->calculate_totals();
		return $this;
	}

    /**
     * @param $code
     * @param int $value discount amount excl tax
     * @param int $vat vat rate percent
     * @throws \WC_Data_Exception
     */
	private function addCouponToOrder($code, $value = 0, $vat = 0)
	{
		$item = new \WC_Order_Item_Coupon();
		$item->set_props([
			'code'         => $code,
			'discount'     => $value,
			'discount_tax' => $vat,
			'order_id'     => $this->wcOrder->get_id()
		]);
		$item->save();
		$this->wcOrder->add_item($item);

        $this->wcOrder->set_total($this->wcOrder->get_total() - $value * (1 + $vat / 100));

		$this->wcOrder->save();
	}

	/**
	 * @param $req
	 * @return $this
	 */
	private function processDiscounts($req)
	{
		// Discounts
		$discountsExc = 0;
        $discountsInc = 0;
		$discountsInternational = 0; // excl
        $discountsInternationalInc = 0;

		$vat = 0;
		if(!empty($req->Discounts)){
			// looping on discounts
			foreach ($req->Discounts as $discount) {

				if(!in_array($discount->DiscountType, $this->acceptedDiscountTypes))
					continue;

				// if shipping discount or other discount
				if($discount->DiscountType == 2){
					$vat = $discount->VATRate;
				} else {
					if(!empty($discount->LocalVATRate)){
						$vat = $discount->LocalVATRate;
					} else {
						$vat = $discount->VATRate;
					}

				}

				$discountsExc += $discount->Price / ($vat / 100 + 1);
                $discountsInc += $discount->Price;
				$discountsInternational += $discount->InternationalPrice / ($vat / 100 + 1);
                $discountsInternationalInc += $discount->InternationalPrice;
				if($discount->CouponCode){
                    $this->addCouponToOrder($discount->CouponCode, $discountsInc, 0);
				}
			}
		} else {
			$discountsExc = 0;
			$discountsInc = 0;
			$discountsInternational = 0;
			$discountsInternationalInc = 0;
		}

		// Onetime coupon discount
		if($req->OTVoucherCode && $req->OTVoucherAmount && $req->OTVoucherCurrencyCode){
			$this->addCouponToOrder($req->OTVoucherCode, $req->OTVoucherAmount, 0);
		}

        update_post_meta( $this->wcOrder->get_id(), '_cart_discount', $discountsInc );
		update_post_meta( $this->wcOrder->get_id(), '_cart_discount_tax', 0 );
		update_post_meta( $this->wcOrder->get_id(), '_globale_DiscountForCustomer', $discountsInternational );

		return $this;
	}

	private function processFreeShipping($req)
	{
		if($req->IsFreeShipping == true
  			&& strlen($req->FreeShippingCouponCode)
  			&& $req->DiscountedShippingPrice == 0 ){
			// add coupon to order
			$this->addCouponToOrder($req->FreeShippingCouponCode);
		}
		return $this;
	}
	/**
	 * @param $req
	 * @return $this
	 */
	private function processMeta($req)
	{
		update_post_meta( $this->wcOrder->get_id(), 'globale', json_encode($req) ); // for admin debug

		// Log request
		update_post_meta( $this->wcOrder->get_id(), '_globale_request_'.time().'_order-update-create', $req );
		update_post_meta( $this->wcOrder->get_id(), 'globale_OrderId', $req->OrderId );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_CurrencyCode', $req->InternationalDetails->CurrencyCode );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_TotalPrice', $req->InternationalDetails->TotalPrice );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_TransactionCurrencyCode', $req->InternationalDetails->TransactionCurrencyCode );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_TransactionTotalPrice', $req->InternationalDetails->TransactionTotalPrice );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_TotalShippingPrice', $req->InternationalDetails->TotalShippingPrice );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_DiscountedShippingPrice', $req->InternationalDetails->DiscountedShippingPrice );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_ConsignmentFee', $req->InternationalDetails->ConsignmentFee );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_SizeOverchargeValue', $req->InternationalDetails->SizeOverchargeValue );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_RemoteAreaSurcharge', $req->InternationalDetails->RemoteAreaSurcharge );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_SameDayDispatchCost', $req->InternationalDetails->SameDayDispatchCost );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_TotalDutiesPrice', $req->InternationalDetails->TotalDutiesPrice );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_TotalCCFPrice', $req->InternationalDetails->TotalCCFPrice );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_ShippingMethodName', $req->InternationalDetails->ShippingMethodName );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_ShippingMethodTypeCode', $req->InternationalDetails->ShippingMethodTypeCode );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_ShippingMethodTypeName', $req->InternationalDetails->ShippingMethodTypeName );
		update_post_meta( $this->wcOrder->get_id(), '_globale_InternationalDetails_PaymentMethodName', $req->InternationalDetails->PaymentMethodName );
		if (isset($req->Customer->SendConfirmation) && $req->Customer->SendConfirmation == false) {
			update_post_meta($this->wcOrder->get_id(), '_globale_Customer_SendConfirmation', false);
		} else {
			update_post_meta($this->wcOrder->get_id(), '_globale_Customer_SendConfirmation', true);
		}

		// For stock hold and pending payment order cancelling to work:
		update_post_meta( $this->wcOrder->get_id(), '_created_via', 'checkout' );

		return $this;
	}

    /**
     * @param $id
     * @param \WC_Order $order
     * @return mixed
     */
	public static function globaleOrderNumber($id, $order){
		$globaleOrderId = get_post_meta( $order->get_id(), 'globale_OrderId', true );
		if(isset($globaleOrderId) && !empty($globaleOrderId)){
			return $globaleOrderId;
		}

		return $id;
	}
}