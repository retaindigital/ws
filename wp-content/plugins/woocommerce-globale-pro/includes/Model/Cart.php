<?php
namespace Globale\Pro\Model;

use \Globale\Pro\Api\Entity\Product as ProductEntity;
use \Globale\Pro\Api\Entity\Attribute as AttributeEntity;
use \Globale\Pro\Api\Entity\VatRateType as VatRateTypeEntity;
use \Globale\Pro\Api\Entity\Discount as DiscountEntity;

use \Globale\Pro\Model\Product as ProductModel;

use \Globale\Pro\Helper\Data;
use \Globale\Pro\Config;

class Cart
{

	const DISCOUPT_TYPE_PERCENT_1 = 'percent';
	const DISCOUPT_TYPE_PERCENT_2 = 'percent_product';
	const DISCOUPT_TYPE_ONE_TIME  = 'onetime_coupon';

	/** @var  \WC_Cart */
	protected $wcCart = null;

	/**
	 * Cart constructor.
	 * @param bool $rest
	 */
	public function __construct($rest = true)
	{
		if ($rest) {
			$this->initRest();
		} else {
			$this->initSession();
		}
		return $this;
	}

	public function initSession(){
		\WC()->cart->get_cart();
		$this->wcCart = \WC()->cart;
		return $this;
	}
	/**
	 * @return $this
	 */
	public function initRest(){

		if ($this->wcCart instanceof \WC_Cart) {
			return $this;
		}
		/** \WooCommerce  */
		\WC()->session = new \WC_Session_Handler();
		\WC()->session->init();
		\WC()->customer = new \WC_Customer( get_current_user_id(), true );
		\WC()->cart = new \WC_Cart();
		\WC()->cart->get_cart_from_session();
		\WC()->cart->get_cart_contents();

		$this->wcCart = \WC()->cart;

		return $this;
	}

	/**
	 * @return null|\WC_Cart
	 * @throws \Exception
	 */
	public function getWcCart()
	{
		if ( $this->wcCart === null) {
			$this->initRest();
		}

		if ($this->wcCart === false || $this->wcCart == null){//} || $this->wcCart->get_cart_contents_count() == 0) {
			throw new \Exception('Failed to load cart.');
		}

		return $this->wcCart;

	}

	/**
	 * @return ProductEntity[]
	 */
	public function buildCartProductsList()
	{
		$wcCartItems = $this->getWcCart()->get_cart();

		$productsList = array();
		$i = 0;

		foreach($wcCartItems as $cart_item_key => $cart_item)
		{

			$product = $this->buildProductFromCartItem($cart_item, $cart_item_key);
			$product->setOrderedQuantity($cart_item['quantity']);

			$attributes = [];

			if ($cart_item['variation']) {
				// in case of variation we collect ans send all attributes

				$item_variations = $cart_item['variation'];
				foreach ($item_variations as $variation => $val) {

					$attribute = new AttributeEntity();
					$attribute->setName($val);
					$attribute->setAttributeCode($variation);
					$typeCode = $variation;
                    if (array_key_exists($typeCode, Config::getAttributesMap())) {
                        $typeCode = Config::getAttributesMap()[$typeCode];
                    }
					$attribute->setAttributeTypeCode($typeCode);
					$attributes[] = $attribute;

				}
			} else {
				// in case of simple product we collect requested in admin attributes

				$extraAttributes = \Globale\Pro\Config::getExtraAttributeList();

				if(!empty($extraAttributes)) {

					$wcAttributes = $cart_item['data']->get_attributes();
					/** @var \WC_Product_Attribute $wcAttribute */
					foreach ($wcAttributes as $attrCode => $wcAttribute) {

						$nakedName = str_replace('pa_', '', $attrCode);
						if(!in_array($nakedName, $extraAttributes)) continue;
						if (!($wcAttribute instanceof \WC_Product_Attribute)) continue;

						$term = wc_get_product_terms($cart_item['data']->get_id(), $wcAttribute->get_name(), array('fields' => 'all'))[0];
						if($term instanceof \WP_Term) {
							$attribute = new AttributeEntity();
							$attribute->setName($term->slug);
							$attribute->setAttributeCode('attribute_' . $attrCode);
                            $typeCode = 'attribute_' . $attrCode;
                            if (array_key_exists($typeCode, Config::getAttributesMap())) {
                                $typeCode = Config::getAttributesMap()[$typeCode];
                            }
                            $attribute->setAttributeTypeCode($typeCode);
							$attributes[] = $attribute;
						}
					}

				}
			}

			$product->setAttributes($attributes);

			$productsList[$i] = $product;

			$i++;
		}
		return $productsList;
	}

	/**
	 * @param $cart_item
	 * @param $cart_item_key
	 * @return ProductEntity
	 */
	protected function buildProductFromCartItem($cart_item, $cart_item_key)
	{

		$product_id = !empty($cart_item['variation_id']) ? $cart_item['variation_id'] : $cart_item['product_id'];
		//$wcProduct = $this->wcProductLoad($product_id);
		$wcProduct = new ProductModel($product_id);
		$product  = new ProductEntity();

		if (!$wcProduct->getSku()) {
			$product->setProductCode($wcProduct->isVariation() ? $wcProduct->getVariationId() : $wcProduct->getId());
		} else {
			$product->setProductCode($wcProduct->getSku());
		}
		$product->setProductGroupCode($cart_item['product_id']);//$wcProduct->getId());
		$product->setIsVirtual($wcProduct->isVirtual());

//		if($wcProduct->isVariation()){
//			$product->setProductCode($wcProduct->getVariationId());
//			$product->setProductGroupCode($wcProduct->getId());
//		}else{
//			$product->setProductCode($wcProduct->getId());
//		}

		if (isset($cart_item['bundled_by'])){
			$product->setParentCartItemId($cart_item['bundled_by']);
		} else if($wcProduct->isBundle()) {
			$product->setIsBundle($wcProduct->isBundle());
		}

		$product->setCartItemId($cart_item_key);
		$product->setName($wcProduct->getName());
		$product->setDescription('');//self::_buildProductDescription($wcProduct);
		$product->setKeywords($wcProduct->getTags());
		$product->setURL($wcProduct->getProductUrl());
		$product->setWeight(!$wcProduct->getWeight() ? null : $wcProduct->getWeight());
		$product->setHeight($wcProduct->getHeight());
		$product->setWidth($wcProduct->getWidth());
		$product->setLength($wcProduct->getLength());
		$imgData = $wcProduct->getImageData();
		$product->setImageURL($imgData[0]);
		$product->setImageWidth($imgData[1]);
		$product->setImageHeight($imgData[2]);

		//$is_blocked_for_globale = '';//self::GetProductAttributeValue($wcProduct,'blocked_for_globale');
		//$item['IsBlockedForGlobalE'] = ($is_blocked_for_globale == 1)?true:false;

		$brandInfo = [];//self::getProductBrand($wcProduct);
		if(is_array($brandInfo)){
			$product->setBrand($brandInfo);
		}

		// build categories list
		$productCategories = [];//self::_buildProductCategoriesList($wcProduct);
		$product->setCategories($productCategories);

		if (isset($cart_item['bundled_by'])){
			$product->setListPrice(0);
			$product->setOriginalListPrice(0);
			$product->setSalePrice(0);
			$product->setOriginalSalePrice(0);
		} elseif ($product->isBundle()) {
			///** @var \WC_Product_Yith_Bundle $bundleObject */
			//$bundleObject = $cart_item['data'];
			//$bundlePrice = $bundleObject->get_per_item_price_tot();
			$product->setListPrice(Data::formatPriceForGetCart($wcProduct->getListPrice()));
			//$product->setOriginalListPrice(Data::formatPriceForGetCart($wcProduct->getOriginalListPrice()));
			$product->setOriginalListPrice(Data::formatPriceForGetCart($wcProduct->getListPrice()));
			$product->setSalePrice(Data::formatPriceForGetCart($wcProduct->getSalePrice()));
			//$product->setSalePriceBeforeRounding(Data::formatPriceForGetCart($wcProduct->getSalePriceBeforeRounding()));
			$product->setSalePriceBeforeRounding(Data::formatPriceForGetCart($wcProduct->getSalePrice()));
			//$product->setOriginalSalePrice(Data::formatPriceForGetCart($wcProduct->getOriginalSalePrice()));
			$product->setOriginalSalePrice(Data::formatPriceForGetCart($wcProduct->getSalePrice()));
		} else {
			$product->setListPrice(Data::formatPriceForGetCart($wcProduct->getListPrice()));
			//$product->setOriginalListPrice(Data::formatPriceForGetCart($wcProduct->getOriginalListPrice()));
			$product->setOriginalListPrice(Data::formatPriceForGetCart($wcProduct->getListPrice()));
			$product->setSalePrice(Data::formatPriceForGetCart($wcProduct->getSalePrice()));
			//$product->setSalePriceBeforeRounding(Data::formatPriceForGetCart($wcProduct->getSalePriceBeforeRounding()));
			$product->setSalePriceBeforeRounding(Data::formatPriceForGetCart($wcProduct->getSalePrice()));
			//$product->setOriginalSalePrice(Data::formatPriceForGetCart($wcProduct->getOriginalSalePrice()));
			$product->setOriginalSalePrice(Data::formatPriceForGetCart($wcProduct->getSalePrice()));
		}
        $product->setIsFixedPrice(null);
        if (!empty($_GET['IsFixedPrice'])) {
            $product->setIsFixedPrice($_GET['IsFixedPrice'] ? true : false);
        }
		if (!$wcProduct->isTaxable()) {
			$product->setLocalVATRateType($this->getZeroVatRateType('not-taxable'));
		} elseif ($wcProduct->isTaxable() && !empty($wcProduct->getTaxRate())) {
			$product->setLocalVATRateType($this->getVATRateType($wcProduct));
		} else {
			$product->setLocalVATRateType($this->getUnknownVatRateType());
			// TODO:
			//$product->setVATRateType();
		}

		return $product;
	}


	protected function getVATRateType($wcProduct){

		$vatRate = new VatRateTypeEntity();
		$vatRate->setRate($wcProduct->getTaxRate()['rate']);
		$vatRate->setName($wcProduct->getTaxClass());
		$vatRate->setVATRateTypeCode($wcProduct->getTaxClass());
		return $vatRate;
	}

	protected function getUnknownVatRateType()
	{
		$vatRate = new VatRateTypeEntity();
		$vatRate->setUnknown();
		return $vatRate;
	}

	protected function getZeroVatRateType($name = 'zero')
	{
		$vatRate = new VatRateTypeEntity();
		$vatRate->setRate(0);
		$vatRate->setName($name);
		$vatRate->setVATRateTypeCode($name);
		return $vatRate;
	}


	/**
	 * @return array
	 */
	public function getFreeShipping(){
		$cart = $this->getWcCart();

		if(isset($cart->applied_coupons)){
			/**
			 * @var  $coupon_name
			 * @var \WC_Coupon $coupon
			 */
			foreach($cart->coupons as $coupon_name => $coupon) {
				if($coupon->get_free_shipping()){
					return [true, $coupon->get_code()];
				}
			}
		}
		return [false, null];
	}

	public function buildCartDiscounts(){

		$cart = $this->getWcCart();
		$discounts = null;

		if(isset($cart->discount_cart) && $cart->discount_cart > 0){ // same as $cart->get_discount_total()

			$discounts = [];
			/**
			 * @var string $couponName
			 * @var \WC_Coupon $coupon */
			foreach($cart->coupons as $couponName => $coupon){

				$discount = new DiscountEntity();

				$isPercent = in_array($coupon->get_discount_type(), [self::DISCOUPT_TYPE_PERCENT_1, self::DISCOUPT_TYPE_PERCENT_2]);
				if ($isPercent) {
					$discount->setCalculationMode(DiscountEntity::DISCOUNT_CALCULATION_MODE_PERCENT);
				} else {
					$discount->setCalculationMode(DiscountEntity::DISCOUNT_CALCULATION_MODE_FIXED_ORIGINAL);
				}

				// do not add onetime coupons here
				if($coupon->is_type(self::DISCOUPT_TYPE_ONE_TIME)) {
					continue;
				} else {
					$discount->setDiscountValue($cart->coupon_discount_amounts[$couponName]);
					$discount->setOriginalDiscountValue(
						\WC()->cart->get_coupon_discount_amount( $coupon->get_code(), \WC()->cart->display_cart_ex_tax )
					);
					//$discount->setOriginalDiscountValue($cart->coupon_discount_amounts[$couponName]);
					$discount->setName(sprintf(DiscountEntity::DISCOUNT_NAME_FORMAT, $couponName));
					$discount->setDescription(sprintf(DiscountEntity::DISCOUNT_DESC_FORMAT, $couponName, $coupon->get_amount(), ($isPercent?'%':'') ));
					$discount->setCouponCode($coupon->get_code());
					$discount->setDiscountType(DiscountEntity::DISCOUNT_TYPE_CART);
					$discount->setVATRate($cart->coupon_discount_tax_amounts[$couponName]);
					$discount->setLocalVATRate($cart->coupon_discount_tax_amounts[$couponName]);
				}
				$discounts[] = $discount;
			}

		}

		return $discounts;
	}

	public function generateMerchantCartHash()
	{
		$hash = $this->getWcCart()->get_cart_hash();
		return $hash;
	}

	public function validateMerchantCartHash($cartId)
	{
		list ($sessionKey, $hash) = explode('-', $cartId);
		$valid = ($hash == $this->generateMerchantCartHash());
		return $valid;
	}
}