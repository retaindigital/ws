<?php
namespace Globale\Pro\Model\Order;


class Item
{

	public static function hideOrderItemsMeta($meta){

		//$meta[] = '_globale_InternationalPrice';
		$meta[] = '_globale_InternationalDiscountedPrice';
		return $meta;
	}

	/**
	 * @param $display_key
	 * @param $meta
	 * @param $ob
	 * @return string
	 */
	public static function renameOrderItemMeta($display_key, $meta, $ob)
	{
	    switch ($display_key) {
            case '_globale_InternationalDiscountedPrice':
            case '_globale_InternationalPrice':
                $display_key = "International Price";
                break;
            default:
                break;
        }

		return $display_key;
	}

	/**
	 * @param $display_value
	 * @param $meta
	 * @param \WC_Order_Item $ob
	 * @return string
	 */
	public static function formatOrderItemMeta($display_value, $meta, $ob)
	{
		// _globale_InternationalDiscountedPrice
		if (in_array($meta->get_data()['key'], ['_globale_InternationalDiscountedPrice', '_globale_InternationalPrice'])) {
			$amount = $display_value;
			$display_value = wc_price($amount, ['currency' => $ob->get_order()->get_meta('_globale_InternationalDetails_CurrencyCode') ]);
			if ($ob->get_quantity() > 1) {
				$subtotal = (int)$amount * $ob->get_quantity();
				$display_value .=
					' <small class="times">×</small> '.
					$ob->get_quantity().
					' : '.
					wc_price($subtotal, ['currency' => $ob->get_order()->get_meta('_globale_InternationalDetails_CurrencyCode') ]);
			}
		}
		return $display_value;
	}

}