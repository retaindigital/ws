<?php
namespace Globale\Pro\Model\Order;


class Status
{

	const GLOBALE_CANCELED = 'canceled';
	const GLOBALE_NA       = 'N/A';
	const GLOBALE_NNA      = 'N\/A';
	const GLOBALE_COMPLETE = 'complete';

	const WC_CANCELLED = 'cancelled';
	const WC_PENDING   = 'pending';
	const WC_COMPLETE  = 'completed';

	protected static $map = [
		self::GLOBALE_CANCELED => self::WC_CANCELLED,
		self::GLOBALE_NA       => self::WC_PENDING,
		self::GLOBALE_NNA      => self::WC_PENDING,
		self::GLOBALE_COMPLETE => self::WC_COMPLETE,
	];

	/**
	 * Converts global-e status to wc status
	 * @param $status
	 * @return bool
	 */
	public static function convertOrderStatus($status)
	{
		if (isset(self::$map[$status])) {
			$status = self::$map[$status];
		}
		if (!wc_is_order_status('wc-' . $status)) {
			return false;
		}
		return $status;
	}
}