<?php
namespace Globale\Pro;

use \Globale\Pro\GlobalePro;
use \Globale\Pro\Config;


class GlobaleProIntegration extends \WC_Integration {

	protected  $globaleEnabled = false;
	/**
	 * Init and hook in the integration.
	 */
	public function __construct() {
		$this->id                 = 'globale_pro';
		$this->method_title       = __( 'GlobalE PRO Integration', 'woocommerce_globale_pro' );
		$this->method_description = __( 'GlobalE PRO Integration.', 'woocommerce_globale_pro' );


		$this->init_form_fields();

		add_action( 'woocommerce_update_options_integration', array( &$this, 'process_admin_options' ) );

		$this->init_settings();

		// Define user set variables.
		$this->globaleEnabled          = $this->get_option( 'api_enabled' );

		$this->init_integration();

		if ($this->globaleEnabled == 'yes') {
			if (GlobalePro::isApplyGlobaleModifications()) {
				$this->load_scripts();
			}
		}

	}

	/**
	 * To save integration settings
	 */
	public function process_admin_options() {
		parent::process_admin_options();
	}

	private function load_scripts()
	{

		if (!is_404() && !\Globale\Pro\GlobalePro::isRestApi())
		{
			add_action('wp_head', [$this, 'gphead']);
		}

	}

	public static function gphead(){
		include('head.phtml');
	}

	private function init_integration()
	{
		if ($this->globaleEnabled) {
			Config::initFromOptions($this->settings);
		}
	}

	/**
	 * Initialize integration settings form fields.
	 *
	 * @return void
	 */
	public function init_form_fields() {

		$this->form_fields = array(

			[
				'title' => __( 'Global-E API Settings', 'woocommerce' ),
				'desc'  => __( '', 'woocommerce' ),
				'type'  => 'title',
				'id'    => 'api',
			],

			Config::ENABLE_GEM_INCLUDE => [
				'title'    => __( 'Enable JS/CSS include', 'woocommerce' ),
				'desc'     => '',
				'id'       => 'api_enabled',
				'type'     => 'checkbox',
				//'default'  => 'no',
				'desc_tip' => true,
			],

			Config::MERCHANT_ID => [
				'title'    => __( 'Merchant ID', 'woocommerce' ),
				'desc'     => '',
				'id'       => 'api_merchant_id',
				'type'     => 'text',
				'default'  => '',
				'css'      => 'min-width:300px;',
				'desc_tip' => true,
			],

			Config::MERCHANT_GUID => [
				'title'    => __( 'Merchant GUID', 'woocommerce' ),
				'desc'     => '',
				'type'     => 'text',
				'default'  => '',
				'css'      => 'min-width:300px;',
				'desc_tip' => true,
			],

			Config::API_BASE_URL => [
				'title'    => __( 'API path', 'woocommerce' ),
				'desc'     => '',
				'type'     => 'text',
				'default'  => '//stglite.bglobale.com/',
				'css'      => 'min-width:300px;',
				'desc_tip' => true,
			],

            Config::INCLUDE_MODE => [
                'title'    => __( 'Include mode', 'woocommerce' ),
                'desc'     => '',
                'type'     => 'select',
                'default'  => Config::INCLUDE_MODE_PRO,
                'css'      => 'min-width:300px;',
                'desc_tip' => true,
                'options' => [
                    Config::INCLUDE_MODE_PRO => 'PRO',
                    Config::INCLUDE_MODE_GEM => 'GEM'
                ]
            ],

            Config::COOKIE_NAME_GLOBALE_DATA_KEY => [
                'title'    => __( 'Globale data cookie name', 'woocommerce' ),
                'desc'     => '',
                'type'     => 'text',
                'default'  => Config::COOKIE_NAME_DEFAULT_GLOBALE_DATA,
                'css'      => 'min-width:300px;',
            ],

			[
				'title' => __( 'Products', 'woocommerce' ),
				'type'  => 'title',
				'desc'  => '',
				'id'    => 'products',
			],

			Config::EXTRA_ATTR_LIST => [
				'title'    => __( 'Extra product attributes', 'woocommerce' ),
				'description'     => 'Slugs of custom attributes with copa separator (like: "attr1,attr2,attr3")',
				'type'     => 'text',
				'default'  => '',
				'css'      => 'min-width:300px;',
				'desc_tip' => true,
			],
			Config::ATTRIBUTE_MAP => [
                'title'    => __( 'Product attributes map', 'woocommerce' ),
                'description'     => 'Products attributes map. Json. Attributes slug => Attribute sign. (example: {"attribute_pa_size":"size"} )',
                'type'     => 'text',
                'default'  => '',
                'css'      => 'min-width:300px;',
                'desc_tip' => true,
            ],

			[
				'title' => __( 'Order', 'woocommerce' ),
				'type'  => 'title',
				'desc'  => '',
				'id'    => 'order',
			],

			Config::SKIP_CART_HASH_VALIDATION => [
				'title'    => __( 'Skip Cart Validation', 'woocommerce' ),
				'desc'     => '',
				'type'     => 'checkbox',
				'default'  => 'no',
				'desc_tip' => true,
			],

            Config::SHIPPING_MAPPING => [
                'title'    => __( 'Use Shipping Method Mapping', 'woocommerce' ),
                'desc'     => '',
                'type'     => 'select',
                'default'  => Config::SHIPPING_MAPPING_PLUGIN,
                'css'      => 'min-width:300px;',
                'desc_tip' => true,
                'options' => [
                    Config::SHIPPING_MAPPING_NONE    => 'Default',
                    Config::SHIPPING_MAPPING_PLUGIN  => 'Plugin map',
                    Config::SHIPPING_MAPPING_GLOBALE => 'Globale map',
                ]
            ],

            Config::SAVE_COUNTRY_NAME_AS => [
                'title'    => __( 'Save country name as', 'woocommerce' ),
                'desc'     => '',
                'type'     => 'select',
                'default'  => Config::SAVE_COUNTRY_NAME_AS_NAME,
                'css'      => 'min-width:300px;',
                'desc_tip' => true,
                'options'  => [
                    Config::SAVE_COUNTRY_NAME_AS_NAME  => 'Country name',
                    Config::SAVE_COUNTRY_NAME_AS_CODE  => 'Country ISO code'
                ]
            ],

            Config::EXTRA_META_TO_ORDER_ITEM => [
                'title'       => __( 'Add extra meta data to order items', 'woocommerce' ),
                'description' => 'Extra meta, that should be added to order items . Json. MetaKey => MetaValue. (example: {"internationalOrder":"YES"} )',
                'type'        => 'text',
                'default'     => '',
                'css'         => 'min-width:300px;',
                'desc_tip'    => true,
            ]

		);
	}

}

