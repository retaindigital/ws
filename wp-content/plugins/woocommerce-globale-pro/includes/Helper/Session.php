<?php
namespace Globale\Pro\Helper;

use \Globale\Pro\Helper\Cookie;
use \Globale\Pro\Helper\Data;
use \Globale\Pro\Config;

class Session {

    static protected $initialized = null;

    public static function getData($key=null){
        global $woocommerce;
        if($woocommerce->session) {
            return $woocommerce->session->get($key);
        }
        else{
            return null;
        }
    }

    public static function setData($key, $value){
        global $woocommerce;

        if($woocommerce->session) {
            $woocommerce->session->set($key, $value);
        }

    }

    public static function setCartCoookie(){

		if(Data::globaleOrderApiMode() ||
			Data::isAdminArea() ||
			!Data::isBrowsingSupported() ||
			self::$initialized
		){

			return;
		}

		$userId = null;
		if( is_user_logged_in() )
		{
			$userId = apply_filters( 'woocommerce_checkout_customer_id', get_current_user_id());
		}

		// Extract cart key
        $sessionKey = '';
        if (!empty($_COOKIE['wp_woocommerce_session_' . COOKIEHASH])) {
            $cookieValue = $_COOKIE['wp_woocommerce_session_' . COOKIEHASH];
            $cookieArray = explode('||', $cookieValue);
            $sessionKey = $cookieArray[0];
        }

		if(\WC()->cart) {
			if (function_exists('wc_get_cart_item_data_hash')) {
				$cartHash = \WC()->cart->get_cart_hash();

				$cookieData = [
					'CartId' => "$sessionKey-$cartHash",
					'UserId' => $userId,
					'PreferedCulture' => null,
					'StoreCode' => null,
                ];

				$cookie_domain = null;

				Cookie::set(Config::COOKIE_NAME_GEM_DATA, json_encode($cookieData),0,'/',$cookie_domain,null, false);

				self::$initialized = true;
			}
		}


	}

	/**
	 * Inject cookie value to _COOKIE for \WC_Session_Handler to able to launch and read cart data
	 * @param $session
	 * @return mixed
	 */
	public static function injectCookieForRestCall($session){

		// compatibility with : \WC_Session_Handler::get_session_cookie
		$sessExp = '9999999999';
		list($sessionKey, $cartHash) = explode('-', $_GET['hash']);
		$toHash = $sessionKey . '|' . $sessExp;
		$hash    = hash_hmac( 'md5', $toHash, wp_hash( $toHash ) );
		$_COOKIE['wp_woocommerce_session_' . COOKIEHASH] = "$sessionKey||$sessExp||.||$hash";
		$_COOKIE['woocommerce_cart_hash'] = $cartHash;

		// FP -->
		/** Price Based on Country plugin */
		if (function_exists('wcpbc')) {
			wcpbc()->current_zone = wcpbc_get_zone_by_country($_GET['countryCode']);
			\WCPBC_Frontend_Pricing::init();
		}
		/** Aelia plugin */
		if($_GET['countryCode'] || $_GET['currencyCode']) {
			$str = json_encode(['countryISO' => $_GET['countryCode'], 'currencyCode' => $_GET['currencyCode']]);
            Cookie::setGlobaleDataCookie($str);
			if (class_exists('Aelia\WC\CurrencySwitcher\Definitions')) {
				if($_GET['countryCode']) {
					Cookie::set(\Aelia\WC\CurrencySwitcher\Definitions::SESSION_CUSTOMER_COUNTRY, $_GET['countryCode']);
                    $_COOKIE[\Aelia\WC\CurrencySwitcher\Definitions::SESSION_CUSTOMER_COUNTRY] = $_GET['countryCode'];
				}
				if($_GET['currencyCode']) {
					Cookie::set(\Aelia\WC\CurrencySwitcher\Definitions::USER_CURRENCY, $_GET['currencyCode']);
                    $_COOKIE[\Aelia\WC\CurrencySwitcher\Definitions::USER_CURRENCY] = $_GET['currencyCode'];
				}
			}
		}
		// <-- FP
		return $session;
	}

	/**
	 * Clears cart automaticaly but ONLY after order arrived from global-e to WC,
	 * in case customer quickly go back to main page and global-e server still not initiated create-order call : customer will see not empty cart
	 * The function can be disabled, but \Globale\Pro\Api\Cart::clear() should be called directly from checkout success page by JS
	 */
	public static function clearCart( ) {
		//return;
		if (\WC()->cart) {
			$cartHash = \WC()->cart->get_cart_hash();

			if ($cartHash) {
				$args = array(
					'meta_key' => \Globale\Pro\Model\Order::ORDER_META_MERCHANT_CART_HASH,
					'meta_value' => $cartHash,
					'post_type' => 'shop_order',
					'post_status' => 'wc-pending'
				);
				$posts = get_posts($args);
				$args['post_status'] = 'wc-processing';
				$processing = get_posts($args);
				$posts = array_merge($posts, $processing);

				if (!empty($posts)) {
					foreach ($posts as $post) {
						$order_id = $post->ID;
						// If we have no cart in session, create a new order and pass it to the SendCart
						if (isset($order_id) && $order_id > 0) {
							$clearCart = get_post_meta($order_id, '_globale_ClearCart', true);
							if ($clearCart == 1) {
								\WC()->cart->empty_cart(true);
								\WC()->session->set('cart', array());
								update_post_meta($order_id, '_globale_ClearCart', 'done');
							}
						}
					}
					//
				}
			}
		}
	}

    /**
     * @return string|null
     */
    public static function getCountryCookie()
    {
        $country = null;

        $cookieString = Cookie::getGlobaleDataCookie();
        if (!empty($cookieString)) {
            $cookie = json_decode($cookieString, true);
            $country = $cookie['countryISO'];
        }

        if (!empty($_GET['glCountry'])) {
            $country = $_GET['glCountry'];
        }

        return $country;
    }

    /**
     * @return string|null
     */
    public static function getCurrencyCookie()
    {
        $currency = null;

        $cookieString = Cookie::getGlobaleDataCookie();
        if (!empty($cookieString)) {
            $cookie = json_decode($cookieString, true);
            $currency = $cookie['currencyCode'];
        }

        if (!empty($_GET['glCurrency'])) {
            $currency = $_GET['glCurrency'];
        }

        return $currency;
    }
}