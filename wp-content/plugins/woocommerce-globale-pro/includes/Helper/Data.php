<?php
namespace Globale\Pro\Helper;

use \Globale\Pro\Config;

class Data {

    public static function isAdminArea()
    {
        // For some reason all Ajax request are considered as admin requests, we are ignoring some of them in this function
        return is_admin()
                && !(is_ajax() &&
            (($_REQUEST['action'] == 'woocommerce_get_refreshed_fragments') // cart update ajax request
            || ($_REQUEST['action'] == 'get_cart_content') // add to cart popup ajax request
            || ($_REQUEST['action'] == 'ivpa_add_to_cart_callback') // add to cart popup ajax request
            || ($_REQUEST['action'] == 'get_result_popup') // predictive search
            || ($_REQUEST['action'] == 'get_results') // search
            || ($_REQUEST['action'] == 'ulp-load-inline-popups') // search
            || ($_REQUEST['action'] == 'ux_quickview') // quickview
            || ($_REQUEST['action'] == 'flatsome_quickview')
            || ($_REQUEST['action'] == 'prdctfltr_respond_550'))); // prdctfltr -- left menu filter ajax
    }

//	/**
//	 * Create API object
//	 */
//	public static function getApiObject()
//	{
//		$api = new Globale_Integration_Api(
//			Config::getApiBaseUrl(),
//			Config::getMerchantGuid(),
//			Config::getApiCacheEnabled(),
//			Config::getApiCacheFolder());
//
//		return $api;
//	}

	/**
	 * Check if current request is done in context of GlobalE order API
	 */
	public static function globaleOrderApiMode()
	{
		// TODO check that it works:

		if(strpos($_SERVER['REQUEST_URI'],'/wc-api/order') !== false){
			return true;
		}

		return false;
	}

	/**
	 * Check if Browsing integration with GlobalE is supported
	 * In can be disabled via configuration settings globally, or
	 * in the specific session, by setting "globale_disabled" in the session to "false"
	 * @return bool
	 */
	public static function isBrowsingSupported()
	{
		return Config::getEnableGemInclude();
	}

	public static function getCart()
	{
		global $woocommerce;
		$cart = $woocommerce->cart;
		return $cart;
	}

	public static function getCartHash()
	{
		return self::getCart()->get_cart_hash();
	}

	/**
	 * Format price to be used in the SendCart API call
	 * @param float price to format
	 * @param bool TRUE for original price, by default - FALSE
	 * @return float
	 */
	public static function formatPriceForGetCart($price)
	{
		//$decimalPoints = 2;
		$decimalPoints = wc_get_price_decimals();
		$price = round($price, $decimalPoints);
		$price = number_format($price, $decimalPoints, '.', '');
		$price = preg_replace('/^([\D]+)?([0-9\.,]+)(.*)?/','$2',$price);
		return $price;
	}

	public static function productIsBundle(\WC_Product $product){

			if ($product
				&& get_class($product) == "WC_Product_Yith_Bundle")
				// do not use ` instance of WC_Product_Yith_Bundle ` because it might nor be defined
				// or we can use method_exists($this->wc_product,"get_bundled_items")
				// why don't they use interfaces aaarrrgggghhhhhh!!!
			{

				$v = $product->get_bundled_items();
				return !empty($v);
			}
			return false;
	}
}