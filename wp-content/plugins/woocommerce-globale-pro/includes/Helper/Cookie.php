<?php
namespace Globale\Pro\Helper;

use Globale\Pro\Config;

class Cookie {
    public static function get($key){
        return stripslashes($_COOKIE[$key]);
    }

    public static function set($key, $val, $expire = 0, $path = '/', $domain = null, $secure = null, $http_only = null){
        setcookie($key, $val, $expire, $path, $domain, $secure, $http_only);
    }

    public static function delete($key){
        setcookie ($key, "", time() - 3600);
    }

    public static function getGlobaleDataCookie() {
        $cookie = self::get(Config::COOKIE_NAME_DEFAULT_GLOBALE_DATA);
        if (!empty($cookie)) {
            return $cookie;
        }

        $cookie = self::get(Config::getGlobaleDataCookieName());
        if (!empty($cookie)) {
            return $cookie;
        }

        return null;
    }

    public static function setGlobaleDataCookie($value) {
        $_COOKIE[Config::getGlobaleDataCookieName()] = $value;
        $_COOKIE[Config::COOKIE_NAME_DEFAULT_GLOBALE_DATA] = $value;
    }
}