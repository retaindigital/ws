<?php

namespace ForGravity\EntryAutomation\Extensions;

use ForGravity\EntryAutomation\Extension;

use GFAddOn;
use GFForms;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Ftp as FTPAdapter;
use League\Flysystem\Sftp\SftpAdapter as SFTPAdapter;

/**
 * Entry Automation FTP Add-On.
 *
 * @since     1.0
 * @author    ForGravity
 * @copyright Copyright (c) 2017, Travis Lopes
 */
class FTP extends Extension {

	/**
	 * Contains an instance of this class, if available.
	 *
	 * @since  1.0
	 * @access private
	 * @var    object $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Defines the version of Entry Automation FTP Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_version Contains the version, defined from entryautomation-ftp.php
	 */
	protected $_version = FG_ENTRYAUTOMATION_FTP_VERSION;

	/**
	 * Defines the plugin slug.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_slug The slug used for this plugin.
	 */
	protected $_slug = 'forgravity-entryautomation-ftp';

	/**
	 * Defines the main plugin file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_path The path to the main plugin file, relative to the plugins folder.
	 */
	protected $_path = 'forgravity-entryautomation-ftp/entryautomation-ftp.php';

	/**
	 * Defines the full path to this class file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_full_path The full path.
	 */
	protected $_full_path = __FILE__;

	/**
	 * Defines the title of this Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_title The title of the Extension.
	 */
	protected $_title = 'Entry Automation FTP Extension';

	/**
	 * Defines the short title of the Extension.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_short_title The short title.
	 */
	protected $_short_title = 'FTP Extension';

	/**
	 * Contains active FTP connections.
	 *
	 * @since  1.1
	 * @access protected
	 * @var    array $_validation_results Contains active FTP connections.
	 */
	protected $_connections = [];

	/**
	 * Get instance of this class.
	 *
	 * @since  1.0
	 * @access public
	 * @static
	 *
	 * @return FTP
	 */
	public static function get_instance() {

		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;

	}

	/**
	 * Register needed pre-initialization hooks.
	 *
	 * @since  1.0
	 */
	public function pre_init() {

		parent::pre_init();

		add_filter( 'fg_entryautomation_export_settings_fields', [ $this, 'settings_fields' ], 10, 1 );

		require_once 'includes/vendor/autoload.php';

	}

	/**
	 * Register needed hooks.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init() {

		parent::init();

		add_action( 'wp_ajax_fg_entryautomation_ftp_validate_credentials', [ $this, 'ajax_validate_credentials' ], 10 );
		add_action( 'wp_ajax_fg_entryautomation_ftp_validate_path', [ $this, 'ajax_validate_path' ], 10 );
		add_action( 'fg_entryautomation_after_export', [ $this, 'upload_file' ], 10, 3 );

	}

	/**
	 * Enqueue needed scripts.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @return array
	 */
	public function scripts() {

		$scripts = [
			[
				'handle'  => $this->get_slug() . '_feed_settings',
				'src'     => $this->get_base_url() . '/js/feed_settings.js',
				'version' => $this->get_version(),
				'enqueue' => [
					[
						'admin_page' => [ 'form_settings' ],
						'tab'        => fg_entryautomation()->get_slug(),
					],
				],
				'callback' => function() {

					wp_localize_script(
						$this->get_slug() . '_feed_settings',
						'fgea_ftp',
						[
							'legacy' => $this->use_legacy_functionality(),
							'nonce'  => wp_create_nonce( $this->get_slug() ),
						]
					);

				},

			],
		];

		return array_merge( parent::scripts(), $scripts );

	}

	/**
	 * Enqueue needed stylesheets.
	 *
	 * @since  1.4
	 *
	 * @return array
	 */
	public function styles() {

		$styles = [
			[
				'handle'  => $this->get_slug() . '_feed_settings',
				'src'     => $this->get_base_url() . "/css/feed_settings.css",
				'version' => $this->get_version(),
				'enqueue' => [
					[
						'admin_page' => [ 'form_settings' ],
						'tab'        => fg_entryautomation()->get_slug(),
					],
				],
			],
		];

		return array_merge( parent::styles(), $styles );

	}





	// # ACTION SETTINGS -----------------------------------------------------------------------------------------------

	/**
	 * Adds Export to FTP settings tab.
	 *
	 * @since  1.0
	 *
	 * @param array $fields Entry Automation settings fields.
	 *
	 * @return array
	 */
	public function settings_fields( $fields ) {

		// Use legacy settings.
		if ( $this->use_legacy_functionality() ) {
			return $this->legacy_settings_fields( $fields );
		}

		// Add Connection Settings tab.
		$existing_tabs = wp_list_pluck( $fields, 'id' );
		if ( ! in_array( 'connections', $existing_tabs ) ) {
			$fields[] = [
				'id'         => 'connections',
				'title'      => esc_html__( 'Connection Settings', 'forgravity_entryautomation' ),
				'dependency' => [
					'live'   => true,
					'fields' => [
						[
							'field'  => 'action',
							'values' => [ 'export' ],
						],
					],
				],
				'sections'   => [],
			];
		}

		// Get Connection Settings key.
		$connections_key = array_search( 'connections', wp_list_pluck( $fields, 'id' ) );

		// Add FTP section.
		$fields[ $connections_key ]['sections'][] = [
			'title'  => esc_html__( 'FTP Connection Settings', 'forgravity_entryautomation' ),
			'fields' => [
				[
					'name'  => 'ftpEnable',
					'label' => esc_html__( 'Upload Export File to FTP Server', 'forgravity_entryautomation_ftp' ),
					'type'  => 'toggle',
				],
				[
					'name'          => 'ftpProtocol',
					'label'         => esc_html__( 'Protocol', 'forgravity_entryautomation_ftp' ),
					'type'          => 'radio',
					'required'      => true,
					'horizontal'    => true,
					'default_value' => 'ftp',
					'dependency'    => [
						'live'   => true,
						'fields' => [
							[
								'field' => 'ftpEnable',
							],
						],
					],
					'tooltip'       => esc_html__( 'Select if export file should be uploaded via FTP or SFTP.', 'forgravity_entryautomation_ftp' ),
					'choices'       => [
						[
							'value' => 'ftp',
							'icon'  => file_get_contents( $this->get_base_path() . '/images/icons/protocol-ftp.svg' ),
							'label' => esc_html__( 'FTP', 'forgravity_entryautomation_ftp' ),
						],
						[
							'value' => 'sftp',
							'icon'  => file_get_contents( $this->get_base_path() . '/images/icons/protocol-sftp.svg' ),
							'label' => esc_html__( 'SFTP', 'forgravity_entryautomation_ftp' ),
						],
					],
				],
				[
					'name'              => 'ftpHost',
					'label'             => esc_html__( 'Host', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'class'             => 'medium',
					'autocomplete'      => 'off',
					'required'          => true,
					'dependency'        => [
						'live'   => true,
						'fields' => [
							[
								'field' => 'ftpEnable',
							],
						],
					],
					'feedback_callback' => [ $this, 'validate_credentials' ],
					'tooltip'           => esc_html__( 'Enter the host for the (S)FTP server. To specify a port, separate host and port with a colon. (e.g. 127.0.0.1:21)', 'forgravity_entryautomation_ftp' )
				],
				[
					'name'              => 'ftpUsername',
					'label'             => esc_html__( 'Username', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'class'             => 'medium',
					'autocomplete'      => 'off',
					'required'          => true,
					'dependency'        => [
						'live'   => true,
						'fields' => [
							[
								'field' => 'ftpEnable',
							],
						],
					],
					'feedback_callback' => [ $this, 'validate_credentials' ],
					'tooltip'           => esc_html__( 'Enter the username for the (S)FTP server.', 'forgravity_entryautomation_ftp' ),
				],
				[
					'name'              => 'ftpPassword',
					'label'             => esc_html__( 'Password', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'input_type'        => 'password',
					'class'             => 'medium',
					'autocomplete'      => 'new-password',
					'required'          => false,
					'dependency'        => [
						'live'   => true,
						'fields' => [
							[
								'field' => 'ftpEnable',
							],
						],
					],
					'feedback_callback' => [ $this, 'validate_credentials' ],
					'tooltip'           => esc_html__( 'Enter the password for the (S)FTP server.', 'forgravity_entryautomation_ftp' ),
				],
				[
					'name'              => 'ftpPath',
					'label'             => esc_html__( 'Path', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'class'             => 'medium',
					'autocomplete'      => 'off',
					'required'          => true,
					'dependency'        => [
						'live'   => true,
						'fields' => [
							[
								'field' => 'ftpEnable',
							],
						],
					],
					'default_value'     => '/',
					'feedback_callback' => [ $this, 'validate_path' ],
					'tooltip'           => esc_html__( 'Enter the path the export file will be uploaded to.', 'forgravity_entryautomation_ftp' ),
				],

			],
		];

		return $fields;

	}

	/**
	 * Adds legacy Export to FTP settings tab.
	 *
	 * @since 1.4
	 *
	 * @param array $fields Entry Automation settings fields.
	 *
	 * @return array
	 */
	public function legacy_settings_fields( $fields ) {

		// Add FTP section.
		$fields[] = [
			'id'         => 'section-export-ftp',
			'class'      => 'entryautomation-feed-section ' . fg_entryautomation()->is_current_section( 'section-export-ftp', false, true ),
			'tab'        => [
				'label' => esc_html__( 'FTP Settings', 'forgravity_entryautomation_ftp' ),
				'icon'  => ' fa-server',
			],
			'dependency' => [ 'field' => 'action', 'values' => [ 'export' ] ],
			'fields'     => [
				[
					'name'    => 'ftpEnable',
					'label'   => esc_html__( 'Upload Export File', 'forgravity_entryautomation_ftp' ),
					'type'    => 'checkbox',
					'onclick' => "jQuery( this ).parents( 'form' ).submit()",
					'choices' => [
						[
							'name'  => 'ftpEnable',
							'label' => esc_html__( 'Upload export file to FTP server', 'forgravity_entryautomation_ftp' ),
						],
					],
				],
				[
					'name'          => 'ftpProtocol',
					'label'         => esc_html__( 'Protocol', 'forgravity_entryautomation_ftp' ),
					'type'          => 'radio',
					'required'      => true,
					'horizontal'    => true,
					'default_value' => 'ftp',
					'dependency'    => [ 'field' => 'ftpEnable', 'values' => [ '1' ] ],
					'tooltip'       => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Protocol', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Select if export file should be uploaded via FTP or SFTP.', 'forgravity_entryautomation_ftp' )
					),
					'choices'       => [
						[
							'value' => 'ftp',
							'label' => esc_html__( 'FTP', 'forgravity_entryautomation_ftp' ),
						],
						[
							'value' => 'sftp',
							'label' => esc_html__( 'SFTP', 'forgravity_entryautomation_ftp' ),
						],
					],
				],
				[
					'name'              => 'ftpHost',
					'label'             => esc_html__( 'Host', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'class'             => 'medium',
					'autocomplete'      => 'off',
					'required'          => true,
					'dependency'        => [ 'field' => 'ftpEnable', 'values' => [ '1' ] ],
					'feedback_callback' => [ $this, 'validate_credentials' ],
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Host', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Enter the host for the (S)FTP server. To specify a port, separate host and port with a colon. (e.g. 127.0.0.1:21)', 'forgravity_entryautomation_ftp' )
					),
				],
				[
					'name'              => 'ftpUsername',
					'label'             => esc_html__( 'Username', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'class'             => 'medium',
					'autocomplete'      => 'off',
					'required'          => true,
					'dependency'        => [ 'field' => 'ftpEnable', 'values' => [ '1' ] ],
					'feedback_callback' => [ $this, 'validate_credentials' ],
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Username', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Enter the username for the (S)FTP server.', 'forgravity_entryautomation_ftp' )
					),
				],
				[
					'name'              => 'ftpPassword',
					'label'             => esc_html__( 'Password', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'input_type'        => 'password',
					'class'             => 'medium',
					'autocomplete'      => 'new-password',
					'required'          => false,
					'dependency'        => [ 'field' => 'ftpEnable', 'values' => [ '1' ] ],
					'feedback_callback' => [ $this, 'validate_credentials' ],
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Password', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Enter the password for the (S)FTP server.', 'forgravity_entryautomation_ftp' )
					),
				],
				[
					'name'              => 'ftpPath',
					'label'             => esc_html__( 'Path', 'forgravity_entryautomation_ftp' ),
					'type'              => 'text',
					'class'             => 'medium',
					'autocomplete'      => 'off',
					'required'          => true,
					'dependency'        => [ 'field' => 'ftpEnable', 'values' => [ '1' ] ],
					'default_value'     => '/',
					'feedback_callback' => [ $this, 'validate_path' ],
					'tooltip'           => sprintf(
						'<h6>%s</h6>%s',
						esc_html__( 'FTP Path', 'forgravity_entryautomation_ftp' ),
						esc_html__( 'Enter the path the export file will be uploaded to.', 'forgravity_entryautomation_ftp' )
					),
				],
			],
		];

		return $fields;

	}

	/**
	 * Validate FTP credentials.
	 *
	 * @since  1.0
	 *
	 * @return bool|null
	 */
	public function validate_credentials() {

		return 'sftp' === fg_entryautomation()->get_setting( 'ftpProtocol' ) ? $this->validate_sftp_credentials() : $this->validate_ftp_credentials();

	}

	/**
	 * Validate FTP credentials via AJAX
	 *
	 * @since  1.0
	 */
	public function ajax_validate_credentials() {

		// Verify nonce.
		if ( ! wp_verify_nonce( rgpost( 'nonce' ), $this->get_slug() ) ) {
			wp_send_json_error();
		}

		// Determine if successful.
		$valid = 'sftp' === rgpost( 'ftpProtocol' ) ? $this->validate_sftp_credentials( true ) : $this->validate_ftp_credentials( true );

		if ( $valid ) {
			wp_send_json_success();
		} else {
			wp_send_json_error();
		}

	}

	/**
	 * Validate FTP path.
	 *
	 * @since  1.0
	 *
	 * @return bool|null
	 */
	public function validate_path() {

		return 'sftp' === fg_entryautomation()->get_setting( 'ftpProtocol' ) ? $this->validate_sftp_path() : $this->validate_ftp_path();

	}

	/**
	 * Validate FTP path via AJAX
	 *
	 * @since  1.0
	 */
	public function ajax_validate_path() {

		// Verify nonce.
		if ( ! wp_verify_nonce( rgpost( 'nonce' ), $this->get_slug() ) ) {
			wp_send_json_error();
		}

		// Determine if successful.
		$valid = 'sftp' === rgpost( 'ftpProtocol' ) ? $this->validate_sftp_path( true ) : $this->validate_ftp_path( true );

		if ( $valid ) {
			wp_send_json_success();
		} else {
			wp_send_json_error();
		}

	}





	// # ON EXPORT -----------------------------------------------------------------------------------------------------

	/**
	 * Handle uploading file upon export.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array  $task      The current Task meta.
	 * @param array  $form      The current Form object.
	 * @param string $file_path Path to export file.
	 *
	 * @uses   FTP::upload_via_ftp()
	 * @uses   FTP::upload_via_sftp()
	 */
	public function upload_file( $task, $form, $file_path ) {

		// If task does not have FTP upload enabled, exit.
		if ( ! rgar( $task, 'ftpEnable' ) ) {
			return;
		}

		// Get host, port, username, password.
		$credentials = [
			'host'     => rgar( $task, 'ftpHost' ),
			'port'     => 'sftp' === rgar( $task, 'ftpProtocol' ) ? 22 : 21,
			'username' => rgar( $task, 'ftpUsername' ),
			'password' => rgar( $task, 'ftpPassword' ),
		];

		// If host has port defined, set it.
		if ( rgar( $credentials, 'host' ) && strpos( $credentials['host'], ':' ) !== false ) {
			list( $credentials['host'], $credentials['port'] ) = explode( ':', $credentials['host'] );
		}

		/**
		 * Modify the FTP credentials before connecting.
		 *
		 * @since 1.0
		 *
		 * @param array $credentials FTP credentials: host, port, username, password.
		 * @param array $task        Entry Automation Task meta.
		 * @param array $form        The Form object.
		 */
		$credentials = gf_apply_filters( [
			'fg_entryautomation_ftp_credentials',
			$form['id'],
			$task['task_id'],
		], $credentials, $task, $form );

		// If host or username are empty, return.
		if ( ! rgar( $credentials, 'host' ) || ! rgar( $credentials, 'username' ) ) {
			return;
		}

		// Connect to server.
		if ( 'ftp' === rgar( $task, 'ftpProtocol' ) ) {

			$filesystem = $this->connect_to_ftp( $credentials['host'], $credentials['port'], $credentials['username'], $credentials['password'] );

		} else if ( 'sftp' === rgar( $task, 'ftpProtocol' ) ) {

			$filesystem = $this->connect_to_sftp( $credentials['host'], $credentials['port'], $credentials['username'], $credentials['password'] );

		}

		// If unable to connect to server, return.
		if ( ! $filesystem ) {
			$this->log_error( __METHOD__ . '(): Unable to upload file as connection could not be created.' );
			return;
		}

		// Get file name.
		$file_name = basename( $file_path );

		/**
		 * Modify the export file path before uploading.
		 *
		 * @since 1.0
		 *
		 * @param string $remote_file_path Destination file path.
		 * @param string $file_name        Export file name.
		 * @param array  $task             Entry Automation Task meta.
		 * @param array  $form             The Form object.
		 */
		$remote_file_path = gf_apply_filters( [
			'fg_entryautomation_ftp_file_path',
			$form['id'],
			$task['task_id'],
		], trailingslashit( rgar( $task, 'ftpPath' ) ) . $file_name, $file_name, $task, $form );

		// Delete existing file.
		if ( ( '1' == rgar( $task, 'exportOverwriteExisting' ) || 'new' !== rgar( $task, 'exportWriteType' ) ) && $filesystem->has( $remote_file_path ) ) {
			$filesystem->delete( $remote_file_path );
		}

		try {

			// Upload file.
			$uploaded = $filesystem->writeStream( $remote_file_path, fopen( $file_path, 'r' ) );

		} catch ( \Exception $e ) {

			// Log that file could not be uploaded.
			fg_entryautomation()->log_error( __METHOD__ . '(): Unable to upload export file to "' . $credentials['host'] . ':' . $credentials['port']. '"; ' . $e->getMessage() );

		}

		// Log that file could not be uploaded.
		if ( ! $uploaded ) {
			fg_entryautomation()->log_error( __METHOD__ . '(): Unable to upload export file to "' . $credentials['host'] . ':' . $credentials['port']. '"; ' . $e->getMessage() );
		}

	}





	// # FTP METHODS ---------------------------------------------------------------------------------------------------

	/**
	 * Connect to FTP server.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $host     FTP host.
	 * @param int    $port     FTP port.
	 * @param string $username FTP username.
	 * @param string $password FTP password.
	 *
	 * @uses   GFAddOn::log_error()
	 *
	 * @return bool|Filesystem
	 */
	public function connect_to_ftp( $host, $port, $username, $password ) {

		if ( ! $port ) {
			$port = 21;
		}

		// Get cache key.
		$key = md5( $host . $port . $username . $password );

		// If connection is already initialized, retrieve it.
		if ( isset( $this->_connections[ $key ] ) ) {
			return $this->_connections[ $key ];
		}

		// Force port to integer.
		if ( ! is_integer( $port ) ) {
			$port = intval( $port );
		}

		// Prepare FTP parameters.
		$params = [
			'host'     => $host,
			'username' => $username,
			'password' => $password,
			'port'     => $port,
			'root'     => '/',
			'passive'  => true,
			'ssl'      => 22 == $port,
			'timeout'  => apply_filters( 'fg_entryautomation_ftp_timeout', 10 ),
		];

		/**
		 * Modify connection params before connecting.
		 *
		 * @since 1.1
		 *
		 * @param array  $params   Connection parameters.
		 * @param string $protocol Protcol connecting to.
		 */
		$params = apply_filters( 'fg_entryautomation_ftp_connection_params', $params, 'ftp' );

		// Initialize Flysystem.
		$filesystem = new Filesystem( new FTPAdapter( $params ) );

		try {

			// Attempt to connect.
			$filesystem->getAdapter()->connect();

		} catch ( \Exception $e ) {

			// Log that connection failed.
			$this->log_error( __METHOD__ . '(): Unable to connect to server "' . $host . ':' . $port . '"; ' . $e->getMessage() );

			// Check if port is blocked.
			$open_port = @fsockopen( $host, $port, $errno, $errstr, .1 );

			// If port is blocked, add message.
			if ( ! $open_port ) {
				$this->log_error( __METHOD__ . '(): Unable to connect to server because port is blocked; "' . $errstr . '" (' . $errno . ')' );
			} else {
				fclose( $open_port );
			}

			$filesystem = false;

		}

		// Store connection.
		$this->_connections[ $key ] = $filesystem;

		return $filesystem;

	}

	/**
	 * Validate FTP credentials.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param bool $via_post Get credentials via $_POST.
	 *
	 * @uses   FTP::connect_to_ftp()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return bool|null
	 */
	public function validate_ftp_credentials( $via_post = false ) {

		// Get host, port, username, password.
		$host     = $via_post ? rgpost( 'ftpHost' ) : fg_entryautomation()->get_setting( 'ftpHost' );
		$port     = 21;
		$username = $via_post ? rgpost( 'ftpUsername' ) : fg_entryautomation()->get_setting( 'ftpUsername' );
		$password = $via_post ? rgpost( 'ftpPassword' ) : fg_entryautomation()->get_setting( 'ftpPassword' );

		// If host or username are empty, return.
		if ( rgblank( $host ) || rgblank( $username ) ) {
			return null;
		}

		// If host has port defined, set it.
		if ( strpos( $host, ':' ) !== false ) {
			list( $host, $port ) = explode( ':', $host );
		}

		// Prepare cache key.
		$cache_key = md5( $host . $port . $username . $password );

		// Check for cached validation.
		if ( get_transient( 'fg_entryautomation_ftp_' . $cache_key ) ) {
			return true;
		}

		// If connection failed, return.
		if ( is_bool( $this->connect_to_ftp( $host, $port, $username, $password ) ) ) {
			return false;
		}

		// Cache validation.
		set_transient( 'fg_entryautomation_ftp_' . $cache_key, true, 5 * MINUTE_IN_SECONDS );

		return true;

	}

	/**
	 * Validate FTP path.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param bool $via_post Get credentials via $_POST.
	 *
	 * @uses   FTP::connect_to_ftp()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return bool|null
	 */
	public function validate_ftp_path( $via_post = false ) {

		// Get host, port, username, password, path.
		$host     = $via_post ? rgpost( 'ftpHost' ) : fg_entryautomation()->get_setting( 'ftpHost' );
		$port     = 21;
		$username = $via_post ? rgpost( 'ftpUsername' ) : fg_entryautomation()->get_setting( 'ftpUsername' );
		$password = $via_post ? rgpost( 'ftpPassword' ) : fg_entryautomation()->get_setting( 'ftpPassword' );
		$path     = $via_post ? rgpost( 'ftpPath' ) : fg_entryautomation()->get_setting( 'ftpPath' );

		// If host or username are empty, return.
		if ( rgblank( $host ) || rgblank( $username ) ) {
			return null;
		}

		// If host has port defined, set it.
		if ( strpos( $host, ':' ) !== false ) {
			list( $host, $port ) = explode( ':', $host );
		}

		// Prepare cache key.
		$cache_key = md5( $host . $port . $username . $password . $path );

		// Check for cached validation.
		if ( get_transient( 'fg_entryautomation_ftp_' . $cache_key ) ) {
			return true;
		}

		// Get FTP connection.
		$ftp_connection = $this->connect_to_ftp( $host, $port, $username, $password );

		// If could not connect to FTP, return.
		if ( is_bool( $ftp_connection ) ) {
			return false;
		}

		// If path could not be found, return.
		if ( ! ftp_chdir( $ftp_connection->getAdapter()->getConnection(), $path ) ) {
			return false;
		}

		// Cache validation.
		set_transient( 'fg_entryautomation_ftp_' . $cache_key, true, 5 * MINUTE_IN_SECONDS );

		return true;

	}





	// # SFTP METHODS --------------------------------------------------------------------------------------------------

	/**
	 * Connect to SFTP server.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $host     SFTP host.
	 * @param int    $port     SFTP port.
	 * @param string $username SFTP username.
	 * @param int    $password SFTP password.
	 *
	 * @uses   GFAddOn::log_error()
	 *
	 * @return bool|Filesystem
	 */
	public function connect_to_sftp( $host, $port, $username, $password ) {

		if ( ! $port ) {
			$port = 22;
		}

		// Get cache key.
		$key = md5( $host . $port . $username . $password );

		// If connection is already initialized, retrieve it.
		if ( isset( $this->_connections[ $key ] ) ) {
			return $this->_connections[ $key ];
		}

		// Force port to integer.
		if ( ! is_integer( $port ) ) {
			$port = intval( $port );
		}

		// Prepare SFTP parameters.
		$params = [
			'host'     => $host,
			'username' => $username,
			'password' => $password,
			'port'     => $port,
			'root'     => '/',
			'timeout'  => apply_filters( 'fg_entryautomation_ftp_timeout', 10 ),
		];

		/**
		 * Modify connection params before connecting.
		 *
		 * @since 1.1
		 *
		 * @param array  $params   Connection parameters.
		 * @param string $protocol Protcol connecting to.
		 */
		$params = apply_filters( 'fg_entryautomation_ftp_connection_params', $params, 'sftp' );

		// Initialize Flysystem.
		$filesystem = new Filesystem( new SFTPAdapter( $params ) );

		try {

			// Attempt to connect.
			@$filesystem->getAdapter()->connect();

		} catch ( \Exception $e ) {

			// Log that connection failed.
			$this->log_error( __METHOD__ . '(): Unable to connect to server "' . $host . ':' . $port . '"; ' . $e->getMessage() );

			// Check if port is blocked.
			$open_port = @fsockopen( $host, $port, $errno, $errstr, .1 );

			// If port is blocked, add message.
			if ( ! $open_port ) {
				$this->log_error( __METHOD__ . '(): Unable to connect to server because port is blocked; "' . $errstr . '" (' . $errno . ')' );
			} else {
				fclose( $open_port );
			}

			$filesystem = false;

		}

		// Store connection.
		$this->_connections[ $key ] = $filesystem;

		return $filesystem;

	}

	/**
	 * Validate SFTP credentials.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param bool $via_post Get credentials via $_POST.
	 *
	 * @uses   FTP::connect_to_sftp()
	 * @uses   GFAddOn::get_setting()
	 *
	 * @return bool|null
	 */
	public function validate_sftp_credentials( $via_post = false ) {

		// Get host, port, username, password.
		$host     = $via_post ? rgpost( 'ftpHost' ) : fg_entryautomation()->get_setting( 'ftpHost' );
		$port     = 22;
		$username = $via_post ? rgpost( 'ftpUsername' ) : fg_entryautomation()->get_setting( 'ftpUsername' );
		$password = $via_post ? rgpost( 'ftpPassword' ) : fg_entryautomation()->get_setting( 'ftpPassword' );

		// If host or username are empty, return.
		if ( rgblank( $host ) || rgblank( $username ) ) {
			return null;
		}

		// If host has port defined, set it.
		if ( strpos( $host, ':' ) !== false ) {
			list( $host, $port ) = explode( ':', $host );
		}

		// Prepare cache key.
		$cache_key = md5( $host . $port . $username . $password );

		// Check for cached validation.
		if ( get_transient( 'fg_entryautomation_ftp_' . $cache_key ) ) {
			return true;
		}

		// If connection failed, return.
		if ( is_bool( $this->connect_to_sftp( $host, $port, $username, $password ) ) ) {
			return false;
		}

		// Cache validation.
		set_transient( 'fg_entryautomation_ftp_' . $cache_key, true, 5 * MINUTE_IN_SECONDS );

		return true;

	}

	/**
	 * Validate SFTP path.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param bool $via_post Get credentials via $_POST.
	 *
	 * @uses   FTP::connect_to_sftp()
	 * @uses   GFAddOn::get_setting()
	 * @uses   \phpseclib\Net\SFTP::chdir()
	 *
	 * @return bool|null
	 */
	public function validate_sftp_path( $via_post = false ) {

		// Get host, port, username, password, path.
		$host     = $via_post ? rgpost( 'ftpHost' ) : fg_entryautomation()->get_setting( 'ftpHost' );
		$port     = 22;
		$username = $via_post ? rgpost( 'ftpUsername' ) : fg_entryautomation()->get_setting( 'ftpUsername' );
		$password = $via_post ? rgpost( 'ftpPassword' ) : fg_entryautomation()->get_setting( 'ftpPassword' );
		$path     = $via_post ? rgpost( 'ftpPath' ) : fg_entryautomation()->get_setting( 'ftpPath' );

		// If host or username are empty, return.
		if ( rgblank( $host ) || rgblank( $username ) ) {
			return null;
		}

		// If host has port defined, set it.
		if ( strpos( $host, ':' ) !== false ) {
			list( $host, $port ) = explode( ':', $host );
		}

		// Prepare cache key.
		$cache_key = md5( $host . $port . $username . $password . $path );

		// Check for cached validation.
		if ( get_transient( 'fg_entryautomation_ftp_' . $cache_key ) ) {
			return true;
		}

		// Get SFTP connection.
		$sftp_connection = $this->connect_to_sftp( $host, $port, $username, $password );

		// If could not connect to SFTP, return.
		if ( is_bool( $sftp_connection ) ) {
			return false;
		}

		// If path could not be found, return.
		if ( $sftp_connection->getAdapter()->getConnection()->get( $path ) === false ) {
			return false;
		}

		// Cache validation.
		set_transient( 'fg_entryautomation_ftp_' . $cache_key, true, 5 * MINUTE_IN_SECONDS );

		return true;

	}





	// # HELPER METHODS ------------------------------------------------------------------------------------------------

	/**
	 * Determines whether legacy functionality should be used.
	 *
	 * @since 1.4
	 *
	 * @return bool
	 */
	public function use_legacy_functionality() {

		return version_compare( fg_entryautomation()->get_version(), '3.0-rc-1', '<' ) || ! $this->is_gravityforms_supported( '2.5-rc-1' );

	}

}
