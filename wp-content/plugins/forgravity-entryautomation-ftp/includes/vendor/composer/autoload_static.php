<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit134f3802514808a2d94f0f0508d54f4b
{
    public static $files = array (
        'decc78cc4436b1292c6c0d151b19445c' => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib/bootstrap.php',
    );

    public static $prefixLengthsPsr4 = array (
        'p' => 
        array (
            'phpseclib\\' => 10,
        ),
        'L' => 
        array (
            'League\\MimeTypeDetection\\' => 25,
            'League\\Flysystem\\Sftp\\' => 22,
            'League\\Flysystem\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'phpseclib\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpseclib/phpseclib/phpseclib',
        ),
        'League\\MimeTypeDetection\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/mime-type-detection/src',
        ),
        'League\\Flysystem\\Sftp\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/flysystem-sftp/src',
        ),
        'League\\Flysystem\\' => 
        array (
            0 => __DIR__ . '/..' . '/league/flysystem/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit134f3802514808a2d94f0f0508d54f4b::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit134f3802514808a2d94f0f0508d54f4b::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
