window.addEventListener( 'load', () => {

	// Determine if we're using legacy UI.
	const legacy = fgea_ftp.legacy === '1';

	// Define inputs.
	const $inputs = {
		ftpHost:     document.getElementById( 'ftpHost' ),
		ftpUsername: document.getElementById( 'ftpUsername' ),
		ftpPassword: document.getElementById( 'ftpPassword' ),
		ftpPath:     document.getElementById( 'ftpPath' ),
	};

	/**
	 * Validate FTP credentials.
	 *
	 * @since 1.3
	 *
	 * @param {bool} validatePath Validate only FTP path.
	 * @returns {Promise<void>}
	 */
	const validateFTPCredentials = async( validatePath ) => {

		// Define action.
		const action = validatePath ? 'fg_entryautomation_ftp_validate_path' : 'fg_entryautomation_ftp_validate_credentials';

		// Get protocol.
		const ftpProtocol = legacy ? document.querySelector( 'input[name="_gaddon_setting_ftpProtocol"]:checked' ).value : document.querySelector( 'input[name="_gform_setting_ftpProtocol"]:checked' ).value;

		// Request validation.
		const request = await fetch(
			ajaxurl,
			{
				method:  'POST',
				headers: {
					'Accept':       'application/json',
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				body:    `action=${ action }&nonce=${ fgea_ftp.nonce }&ftpProtocol=${ ftpProtocol }ftpHost=${ $inputs.ftpHost.value }&ftpUsername=${ $inputs.ftpUsername.value }&ftpPassword=${ $inputs.ftpPassword.value }&ftpPath=${ $inputs.ftpPath.value }`
			}
		);

		// Process response.
		const response = await request.json();

		// Update validation state.
		if ( legacy ) {

			// Loop through inputs, display validation.
			for ( const $input in $inputs ) {

				// If validating path, skip other inputs.
				if ( validatePath && $input !== 'ftpPath' ) {
					continue;
				}

				// Add feedback element.
				if ( ! document.querySelector( `#gaddon-setting-row-${ $input } td i` ) ) {

					// Create element.
					let $i = document.createElement( 'i' );
					$i.classList.add( 'fa' );

					// Append element.
					document.querySelector( `#gaddon-setting-row-${ $input } td` ).appendChild( $i );

				}

				let $i = document.querySelector( `#gaddon-setting-row-${ $input } td i` );

				// Reset validation state.
				$i.classList.remove( 'icon-check', 'icon-remove', 'fa-check', 'fa-times', 'gf_valid', 'gf_invalid' );

				// Set new validation state.
				if ( response.success ) {
					$i.classList.add( 'icon-check', 'fa-check', 'gf_valid' );
				} else {
					$i.classList.add( 'icon-remove', 'fa-times', 'gf_invalid' );
				}

			}

		} else {

			// Loop through inputs, display validation.
			for ( const $input in $inputs ) {

				// If validating path, skip other inputs.
				if ( validatePath && $input !== 'ftpPath' ) {
					continue;
				}

				// Define element.
				const $elem = $inputs[ $input ];

				// Reset validation state.
				$elem.parentNode.classList.remove( 'gform-settings-input__container--feedback-error', 'gform-settings-input__container--feedback-success' );

				// Set new validation state.
				if ( response.success ) {
					$elem.parentNode.classList.add( 'gform-settings-input__container--feedback-success' );
				} else {
					$elem.parentNode.classList.add( 'gform-settings-input__container--feedback-error' );
				}

			}

		}

	};

	// Bind change events for text inputs.
	for ( const $input in $inputs ) {
		$inputs[ $input ].addEventListener( 'blur', () => validateFTPCredentials( $input === 'ftpPath' ) )
	}

	// Bind change events for radio inputs.
	document.querySelectorAll( legacy ? 'input[name="_gaddon_setting_ftpProtocol"]' : 'input[name="_gform_setting_ftpProtocol"]' ).forEach( ( radio ) => radio.addEventListener( 'change', () => validateFTPCredentials( false ) ) );

} );
