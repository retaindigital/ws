## Version 1.5 (2021-06-24)
- Added security enhancements.

## Version 1.4 (2021-04-26)
- Added support for Entry Automation 3.0.

## Version 1.3 (2019-08-19)
- Added support for Entry Automation 2.0.
- Fixed export file not uploading when using Overwrite Existing File setting.
- Fixed FTP password setting automatically populating in Chrome.

## Version 1.2 (2019-07-31)
- Added check to see if port is blocked when unable to connect to server.
- Fixed FTP password setting being automatically populated.

## Version 1.1 (2018-12-23)
- Added connection caching to prevent opening multiple connections to one server during a request.
- Fixed validation issues.

## Version 1.0 (2018-03-02)
- It's all new!