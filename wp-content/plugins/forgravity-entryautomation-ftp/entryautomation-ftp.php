<?php
/**
 * Plugin Name: Entry Automation FTP Extension
 * Plugin URI: http://forgravity.com/plugins/entry-automation/
 * Description: Upload your Entry Automation export file to your (S)FTP server
 * Version: 1.5
 * Author: ForGravity
 * Author URI: http://forgravity.com
 * Text Domain: forgravity_entryautomation_ftp
 * Domain Path: /languages
 **/

if ( ! defined( 'FG_EDD_STORE_URL' ) ) {
	define( 'FG_EDD_STORE_URL', 'https://forgravity.com' );
}

define( 'FG_ENTRYAUTOMATION_FTP_VERSION', '1.5' );
define( 'FG_ENTRYAUTOMATION_FTP_EDD_ITEM_ID', 3160 );

// Initialize plugin updater.
add_action( 'init', array( 'EntryAutomation_FTP_Bootstrap', 'updater' ), 0 );

// If Gravity Forms is loaded, bootstrap the Entry Automation FTP Add-On.
add_action( 'gform_loaded', array( 'EntryAutomation_FTP_Bootstrap', 'load' ), 10 );

/**
 * Class EntryAutomation_FTP_Bootstrap
 *
 * Handles the loading of the Entry Automation FTP Add-On.
 */
class EntryAutomation_FTP_Bootstrap {

	/**
	 * If Entry Automation exists, Entry Automation FTP Add-On is loaded.
	 *
	 * @access public
	 * @static
	 */
	public static function load() {

		if ( ! function_exists( 'fg_entryautomation' ) ) {
			return;
		}

		if ( ! class_exists( '\ForGravity\EntryAutomation\Extensions\FTP\EDD_SL_Plugin_Updater' ) ) {
			require_once( fg_entryautomation()->get_base_path() . '/includes/EDD_SL_Plugin_Updater.php' );
		}

		require_once( 'class-entryautomation-ftp.php' );

		fg_entryautomation_ftp();

	}

	/**
	 * Initialize plugin updater.
	 *
	 * @access public
	 * @static
	 */
	public static function updater() {

		// Get Entry Automation instance.
		$entry_automation = function_exists( 'fg_entryautomation' ) ? fg_entryautomation() : false;

		// If Entry Automation could not be retrieved, exit.
		if ( ! $entry_automation ) {
			return;
		}

		// Get license key.
		$license_key = fg_entryautomation()->get_license_key();

		new ForGravity\EntryAutomation\EDD_SL_Plugin_Updater(
			FG_EDD_STORE_URL,
			__FILE__,
			array(
				'version' => FG_ENTRYAUTOMATION_FTP_VERSION,
				'license' => $license_key,
				'item_id' => FG_ENTRYAUTOMATION_FTP_EDD_ITEM_ID,
				'author'  => 'ForGravity',
			)
		);

	}
}

/**
 * Returns an instance of the Entry_Automation class
 *
 * @see    FTP::get_instance()
 *
 * @return bool|ForGravity\EntryAutomation\Extensions\FTP
 */
function fg_entryautomation_ftp() {
	return class_exists( '\ForGravity\EntryAutomation\Extensions\FTP' ) ? ForGravity\EntryAutomation\Extensions\FTP::get_instance() : false;
}
