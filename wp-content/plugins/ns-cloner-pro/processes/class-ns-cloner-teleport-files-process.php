<?php
/**
 * Teleport Files Background Process
 *
 * @package NS_Cloner
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * NS_Cloner_Teleport_Files_Process class.
 *
 * Processes a queue of local files and copies them to a remote site.
 */
class NS_Cloner_Teleport_Files_Process extends NS_Cloner_Process {

	/**
	 * Ajax action hook
	 *
	 * @var string
	 */
	protected $action = 'teleport_files_process';

	/**
	 * Teleport FTP connection resource
	 *
	 * @var null|resource
	 */
	protected $ftp_connection = null;

	/**
	 * Teleport SFTP connection resource
	 *
	 * @var null|resource
	 */
	protected $sftp_connection = null;

	/**
	 * Teleport FTP/SFTP root path of WP
	 *
	 * @var null|string
	 */
	protected $ftp_wp_path = null;

	/**
	 * Target destination ABSPATH
	 *
	 * @var string
	 */
	protected $target_abs_path = '';

	/**
	 * Initialize and set label
	 */
	public function __construct() {
		parent::__construct();
		$this->report_label = __( 'Files', 'ns-cloner' );
		// Set a limit for batches save to fit data weight.
		add_filter( $this->identifier . '_max_batch', array( $this, 'set_limit_batch_save_hook_cb' ) );
	}

	/**
	 * Set up FTP/SFT connections if they were configured for Teleport mode
	 */
	protected function maybe_set_up_ftp_connection() {
		$is_use_ftp            = ns_cloner_request()->get( 'teleport_ftp_use' );
		$ftp_type              = trim( ns_cloner_request()->get( 'teleport_ftp_type', '' ) );
		$ftp_host              = trim( ns_cloner_request()->get( 'teleport_ftp_host', '' ) );
		$ftp_port              = trim( ns_cloner_request()->get( 'teleport_ftp_port', '' ) );
		$ftp_user              = trim( ns_cloner_request()->get( 'teleport_ftp_username', '' ) );
		$ftp_pass              = trim( ns_cloner_request()->get( 'teleport_ftp_password', '' ) );
		$ftp_path              = trim( ns_cloner_request()->get( 'teleport_ftp_root_path', '' ) );
		$ftp_path              = '/' . trim( $ftp_path, '/' );

		if ( ! empty( $is_use_ftp ) ) {
			if ( $ftp_type === 'ftp' ) {
				$ftp_connection     = @ftp_connect( $ftp_host, $ftp_port );
				$ftp_authentication = @ftp_login( $ftp_connection, $ftp_user, $ftp_pass );
				if ( $ftp_connection && $ftp_authentication ) {
					// Try to set FTP passive mode.
					@ftp_pasv( $ftp_connection, true );
					$this->ftp_connection = $ftp_connection;
					$this->ftp_wp_path    = $ftp_path;
				}
			} elseif ( $ftp_type === 'sftp' && function_exists( 'ssh2_connect' ) ) {
				$sftp_connection     = @ssh2_connect( $ftp_host, $ftp_port );
				$sftp_authentication = @ssh2_auth_password( $sftp_connection, $ftp_user, $ftp_pass );
				if ( $sftp_connection && $sftp_authentication ) {
					$this->sftp_connection = ssh2_sftp( $sftp_connection );
					$this->ftp_wp_path     = $ftp_path;
				}
			}
		}
	}

	/**
	 * Task
	 *
	 * Override this method to perform any actions required on each
	 * queue item. Return the modified item for further processing
	 * in the next pass through. Or, return false to remove the
	 * item from the queue.
	 *
	 * @param mixed $item Queue item to iterate over.
	 *
	 * @return mixed
	 */
	protected function task( $item ) {
		$teleport = ns_cloner()->get_addon( 'teleport' );
		if ( $this->ftp_connection ) {
			$result = $teleport->send_file_ftp(
				$item['source'],
				$item['destination'],
				$this->ftp_connection,
				$this->ftp_wp_path
			);
		} elseif ( $this->sftp_connection ) {
			$result = $teleport->send_file_sftp(
				$item['source'],
				$item['destination'],
				$this->sftp_connection,
				$this->ftp_wp_path
			);
		} else {
			$result = $teleport->send_file( $item['source'], $item['destination'], $this->target_abs_path );
		}

		return parent::task( $item );
	}

	/**
	 * Set limit on batches save count.
	 *
	 * @param $limit
	 *
	 * @return int
	 */
	public function set_limit_batch_save_hook_cb( $limit ) {
		return 2500;
	}

	/**
	 * do actions before starting tasks
	 */
	public function handle() {
		// Maybe initialise FTP/SFTP if it's configured.
		$this->maybe_set_up_ftp_connection();
		// Set up target ABSPATH.
		$target_upload_dir     = ns_cloner_request()->get( 'target_upload_dir' );
		$target_upload_dir_rel = ns_cloner_request()->get( 'target_upload_dir_relative' );
		$this->target_abs_path = substr( $target_upload_dir, 0, - strlen( $target_upload_dir_rel ) );
		// Pass back to parent for handling.
		parent::handle();
	}
}

