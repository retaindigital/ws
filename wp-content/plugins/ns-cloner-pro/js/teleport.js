/**
 * Teleport Cloner Addon
 *
 * @package NS_Cloner
 */

jQuery(
	function($){

		// Update sections UI when selecting/de-selecting teleport.
		$( '.ns-cloner-main-form' ).on(
			'ns_cloner_form_refresh',
			function(){
				var mode             = $( '.ns-cloner-select-mode' ).val();
				var network_selector = $( '.ns-cloner-teleport-network-selector' );
				if ( 'clone_teleport' === mode ) {
					// Show full network option in select source section.
					network_selector.slideDown();
				} else {
					// Hide full network option in select source section.
					network_selector.slideUp();
					// Uncheck full network.
					$( '#full_network_1' ).removeAttr( 'checked' );
					$( '#full_network_0' ).attr( 'checked', 'checked' ).trigger( 'change' );
				}
			}
		);

		// Make users tables mandatory for teleport.
		$( '.ns-cloner-select-tables-control' ).on(
			'updated',
			function(){
				var user_tables = $( this ).find( '[value$=users],[value$=usermeta]' );
				if ( 'clone_teleport' === $( '.ns-cloner-select-mode' ).val() ) {
					user_tables.prop( 'checked', true ).prop( 'disabled', true );
				} else {
					user_tables.prop( 'disabled', false );
				}
			}
		)

		// Update UI after connecting to remote site.
		var all_blocks      = $( '#ns-cloner-section-teleport_target .ns-cloner-section-content' ).children();
		var waiting_block   = $( '.teleport-site-waiting' );
		var loading_block   = $( '.teleport-site-loading' );
		var connected_block = $( '.teleport-site-connected' );
		$( '#ns-cloner-section-teleport_site' ).on(
			'validation.start',
			function(){
				all_blocks.hide();
				loading_block.show();
			}
		);
		$( '#ns-cloner-section-teleport_site' ).on(
			'validation.error',
			function(){
				all_blocks.hide();
				waiting_block.show();
			}
		);
		$( '#ns-cloner-section-teleport_site' ).on(
			'validation.success',
			function(){
				$.get(
					ns_cloner.ajaxurl,
					{
						action: 'ns_cloner_get_remote_data',
						clone_nonce: ns_cloner.nonce
					},
					function( result ){
						if ( ! result.data ) {
							return;
						}
						all_blocks.hide();
						connected_block.show();
						var is_full_network     = $( '#full_network_1' ).is( ':checked' );
						var is_remote_multisite = result.data.is_multisite;
						var remote_url          = $( '[name=remote_url]' ).val().match( /(https?:\/\/)(.+)/ );
						var target_input        = $( '[name=teleport_target_name]' );
						// Show / hide target site based on if this will be a subsite to subsite clone (only time it's applicable).
						if ( is_remote_multisite && ! is_full_network ) {
							// Now that class is visible, update the URL/name input UI based on whether remote is subdomain or subdir.
							if ( result.data.is_subdomain ) {
								target_input.prev( 'label' ).text( remote_url[1] );
								target_input.next( 'label' ).text( '.' + remote_url[2] );
							} else {
								target_input.prev( 'label' ).text( remote_url[1] + remote_url[2] + '/' );
								target_input.next( 'label' ).text( '/' );
							}
							target_input.removeAttr( 'readonly' );
						} else {
							target_input.prev( 'label' ).text( remote_url[1] );
							target_input.val( remote_url[2] ).attr( 'readonly', 'readonly' );
							target_input.next( 'label' ).text( '/' );
						}
					}
				);
			}
		);

		// Make UI updates when full network option is selected / deselected.
		$( '[name=teleport_full_network]' ).change(
			function(){
				// Gray out / disable / update source selection.
				var source = $( '.ns-cloner-site-select' );
				var others = $( '.ns-cloner-teleport-network-selector' ).nextAll();
				if ( $( '#full_network_1' ).is( ':checked' ) ) {
					others.addClass( 'ns-cloner-teleport-disabled' );
					source.append( '<option value="" style="display:none">Full Network</option>' );
					source.val( '' );
					source.prop( 'disabled', true );
				} else {
					others.removeClass( 'ns-cloner-teleport-disabled' );
					source.prop( 'disabled', false );
					source.find( 'option:contains(Network)' ).remove();
					source.val( source.find( 'option:first' ).attr( 'value' ) );
				}
				source.trigger( 'change' ).trigger( 'chosen:updated' );
			}
		);

		// Auto separate and fill remote key when pasting url+key combo.
		$( '#remote_url' ).on(
			'input',
			function(){
				var parts = $( this ).val().split( /\s/ );
				if ( parts.length > 1 ) {
					$( this ).val( parts.slice( 0, 1 ).join( '' ) );
					$( '#remote_key' ).val( parts.slice( 1 ).join( '' ) ).trigger( 'input' );
				}
			}
		);

		// Auto-select remote connection fields when clicking on them.
		$( '.ns-cloner-teleport-settings-form' ).on(
			'focus',
			'[readonly]',
			function(){
				$( this ).select();
			}
		);

		/* ============== Teleport FTP configuration START ============== */
		$(document).on('change', '.teleport-ftp-configuration [name="teleport_ftp_use"]', function () {
			var ftp_fields_block = $(this).closest('.teleport-ftp-configuration').find('.teleport-ftp-fields');
			if ($(this).is(':checked')) {
                ftp_fields_block.removeClass('disabled');
			} else {
                ftp_fields_block.addClass('disabled');
                $(this).closest('.teleport-ftp-configuration').find('.teleport-ftp-verify').hide();
                ftp_fields_block.find('.is-error').removeClass('is-error');
			}
        });

		$(document).on('change', '.teleport-ftp-configuration [name="teleport_ftp_type"]', function () {
			var value = $(this).val(),
				ftp_config_block = $(this).closest('.teleport-ftp-configuration'),
				ftp_host_input = ftp_config_block.find('[name="teleport_ftp_host"]'),
				ftp_port_input = ftp_config_block.find('[name="teleport_ftp_port"]'),
                sftp_miss_error = ftp_config_block.find('.teleport-sftp-miss-error');
			if (value === 'ftp') {
                ftp_host_input.attr('placeholder', 'ftp.example.com');
                ftp_port_input.val(21);
                if (sftp_miss_error.length > 0) {
                    sftp_miss_error.slideUp();
				}
			} else if (value === 'sftp') {
                ftp_host_input.attr('placeholder', 'sftp.example.com');
                ftp_port_input.val(22);
                if (sftp_miss_error.length > 0) {
                    sftp_miss_error.slideDown();
                }
			}
        });

		$(document).on('click', '.teleport-ftp-configuration .teleport-ftp-verify-connection', function (e) {
			e.preventDefault();
			var ftp_config = $(this).closest('.teleport-ftp-configuration'),
				ftp_use_input = ftp_config.find('[name="teleport_ftp_use"]'),
				ftp_type_input = ftp_config.find('[name="teleport_ftp_type"]'),
				ftp_host_input = ftp_config.find('[name="teleport_ftp_host"]'),
				ftp_port_input = ftp_config.find('[name="teleport_ftp_port"]'),
				ftp_user_input = ftp_config.find('[name="teleport_ftp_username"]'),
				ftp_pass_input = ftp_config.find('[name="teleport_ftp_password"]'),
				ftp_path_input = ftp_config.find('[name="teleport_ftp_root_path"]');

			if (!ftp_use_input.is(':checked')) {
                ftp_config.find('.teleport-ftp-fields').addClass('disabled');
                return false;
			}
			var credetials_valid = true;

			if (ftp_type_input.val() !== 'ftp' && ftp_type_input.val() !== 'sftp') {
				credetials_valid = false;
                ftp_type_input.addClass('is-error');
			} else {
                ftp_type_input.removeClass('is-error');
			}

			if (ftp_host_input.val().trim().length === 0) {
				credetials_valid = false;
                ftp_host_input.addClass('is-error');
			} else {
                ftp_host_input.removeClass('is-error');
            }

            if (ftp_port_input.val().trim().length === 0) {
                credetials_valid = false;
                ftp_port_input.addClass('is-error');
			} else {
				var port = parseInt(ftp_port_input.val().trim());
				if (isNaN(port) || port <= 0) {
                    credetials_valid = false;
                    ftp_port_input.addClass('is-error');
				} else {
                    ftp_port_input.removeClass('is-error');
				}
			}

            if (ftp_user_input.val().trim().length === 0) {
                credetials_valid = false;
                ftp_user_input.addClass('is-error');
            } else {
                ftp_user_input.removeClass('is-error');
            }

            if (ftp_pass_input.val().trim().length === 0) {
                credetials_valid = false;
                ftp_pass_input.addClass('is-error');
            } else {
                ftp_pass_input.removeClass('is-error');
            }

            if (credetials_valid) {
                window.ns_cloner_form.validate_section(ftp_config.closest('.ns-cloner-section').attr('id'));
			}
        });

        $(document).on('change, keydown', '.teleport-ftp-configuration .is-error', function() {
			$(this).removeClass('is-error');
        });

        $(document).on('click', '.ns-cloner-button', function () {
			$('.teleport-ftp-configuration').find('.teleport-ftp-verify').hide();
        });

		var teleport_ftp_section = $('#ns-cloner-section-teleport_ftp');
		if (teleport_ftp_section.length > 0) {
            teleport_ftp_section.on('validation.start', function () {
				var section = $(this);
                section.find('.teleport-ftp-verify-connection').hide();
                section.find('.ns-cloner-error-message').remove();
                section.find('.teleport-ftp-verify').hide();
				section.find('.teleport-ftp-verify[data-type="check"]').fadeIn();
            });
            teleport_ftp_section.on('validation.success', function () {
                var section = $(this);
                section.find('.teleport-ftp-verify-connection').show();
                section.find('.teleport-ftp-verify').hide();
                section.find('.teleport-ftp-verify[data-type="success"]').fadeIn();
            });
            teleport_ftp_section.on('validation.error', function () {
                var section = $(this);
                section.find('.teleport-ftp-verify-connection').show();
                section.find('.teleport-ftp-verify').hide();
                section.find('.teleport-ftp-verify[data-type="fail"]').fadeIn();
            });
		}
        /* ============== Teleport FTP configuration END ============== */

		/* ============== Teleport Themes & Plugins Start ===============*/
        $(document).on('change', '.teleport-themes-plugins [name="migrate_themes"]', function () {
            var themes_block = $(this).closest('.teleport-themes-plugins').find('.teleport-themes');
            if ($(this).is(':checked')) {
                themes_block.removeClass('disabled');
            } else {
                themes_block.addClass('disabled');
            }
        });

        $(document).on('change', '.teleport-themes-plugins [name="migrate_plugins"]', function () {
            var plugins_block = $(this).closest('.teleport-themes-plugins').find('.teleport-plugins');
            if ($(this).is(':checked')) {
                plugins_block.removeClass('disabled');
            } else {
                plugins_block.addClass('disabled');
            }
        });

        $(document).on('click', '.teleport-themes-plugins .teleport-themes-plugins-check-all .ns-cloner-gold-link', function () {
			var block = $(this).closest('.teleport-plugins, .teleport-themes'),
				checkboxes = block.find('.ns-cloner-multi-checkbox-wrapper input[type=checkbox]');
			if ($(this).hasClass('all')) {
                checkboxes.prop('checked', true);
			} else if ($(this).hasClass('none')) {
                checkboxes.prop('checked', false);
			}
        });
		/* ============== Teleport Themes & Plugins END =================*/
	}
);
