<?php
/**
 * Teleport section: Migrate Themes and Plugins
 *
 * @package NS_Cloner
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Class NS_Cloner_Section_Teleport_Themes_Plugins
 */
class NS_Cloner_Section_Teleport_Themes_Plugins extends NS_Cloner_Section {

	/**
	 * Mode ids that this section should be visible and active for.
	 *
	 * @var array
	 */
	public $modes_supported = [ 'clone_teleport' ];

	/**
	 * DOM id for section box.
	 *
	 * @var string
	 */
	public $id = 'teleport_themes_plugins';

	/**
	 * Priority relative to other section boxes in UI.
	 *
	 * @var int
	 */
	public $ui_priority = 760;

	/**
	 * Do any setup before starting the cloning process (like hooks to modify the process).
	 */
	public function process_init() {
		// Enable the file copy step if migrate themes or plugins are checked.
		add_filter( 'ns_cloner_do_step_copy_files', array( $this, 'maybe_enable_copy_files' ) );
	}

	/**
	 * Output content for section settings box on admin page.
	 */
	public function render() {
		$this->open_section_box( __( 'Migrate Themes & Plugins', 'ns-cloner' ) );
		$themes             = wp_get_themes();
		$plugins            = get_plugins();
		$plugins_to_exclude = array(
			'ns-cloner-pro/ns-cloner-pro.php',
			'ns-cloner-site-copier/ns-cloner.php'
		);
		?>
        <div class="teleport-themes-plugins">
            <h5>
                <label class="ns-cloner-checkbox-label">
                    <input type="checkbox" name="migrate_themes" checked
                           value="1"><?php esc_html_e( 'Migrate installed themes to a new site', 'ns-cloner' ); ?>
                </label>
            </h5>
            <div class="teleport-themes">
                <div class="teleport-themes-plugins-check-all">
                    <em class="ns-cloner-gold-link all"><?php esc_html_e( 'Select All', 'ns-cloner' ); ?></em>
                    <em class="ns-cloner-gold-link none"><?php esc_html_e( 'Select None', 'ns-cloner' ); ?></em>
                </div>
                <div class="ns-cloner-multi-checkbox-wrapper ns-cloner-select-migrate-themes-control">
					<?php if ( count( $themes ) > 0 ) : ?>
						<?php foreach ( $themes as $theme_folder => $theme ) : ?>
                            <label class="ns-cloner-checkbox-label">
                                <input value="<?php echo esc_attr( $theme_folder ); ?>"
                                       type="checkbox" name="themes_to_migrate[]"
                                       checked/><?php echo $theme->get( 'Name' ); ?>
                            </label>
						<?php endforeach; ?>
					<?php endif; ?>
                </div>
                <div class="disabled-overlay"></div>
            </div>
            <h5>
                <label class="ns-cloner-checkbox-label">
                    <input type="checkbox" name="migrate_plugins" checked
                           value="1"><?php esc_html_e( 'Migrate installed plugins to a new site', 'ns-cloner' ); ?>
                </label>
            </h5>
            <div class="teleport-plugins">
                <div class="teleport-themes-plugins-check-all">
                    <em class="ns-cloner-gold-link all"><?php esc_html_e( 'Select All', 'ns-cloner' ); ?></em>
                    <em class="ns-cloner-gold-link none"><?php esc_html_e( 'Select None', 'ns-cloner' ); ?></em>
                </div>
                <div class="ns-cloner-multi-checkbox-wrapper ns-cloner-select-migrate-plugins-control">
					<?php if ( count( $plugins ) > 0 ) : ?>
						<?php foreach ( $plugins as $plugin_root => $plugin ) : ?>
							<?php if ( ! in_array( $plugin_root, $plugins_to_exclude, true ) ) : ?>
                                <label class="ns-cloner-checkbox-label">
                                    <input value="<?php echo esc_attr( explode( '/', $plugin_root )[0] ); ?>"
                                           type="checkbox" name="plugins_to_migrate[]"
                                           checked/><?php echo $plugin['Name']; ?>
                                </label>
							<?php endif; ?>
						<?php endforeach; ?>
					<?php endif; ?>
                </div>
                <div class="disabled-overlay"></div>
            </div>
            <p class="description ns-cloner-disclaimer">
                <strong>Warning! Cloning will overwrite any copies of the themes & plugins on the remote site</strong>
            </p>
        </div>
		<?php
		$this->close_section_box();
	}

	/**
	 * Enable copy files if plugins ot themes checked
	 *
	 * @param bool $is_copy_enabled Whether to copy.
	 *
	 * @return bool
	 */
	public function maybe_enable_copy_files( $is_copy_enabled ) {
		if ( $is_copy_enabled ) {
			return $is_copy_enabled;
		}

		return ns_cloner_request()->get( 'migrate_themes' ) || ns_cloner_request()->get( 'migrate_plugins' );
	}
}