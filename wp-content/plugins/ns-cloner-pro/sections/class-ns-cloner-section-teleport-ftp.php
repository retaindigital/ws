<?php
/**
 * Create Teleport FTP/SFTP connection to remote site
 *
 * @package NS_Cloner
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Class NS_Cloner_Section_Teleport_Ftp
 *
 * Adds and validates options for ftp/sftp connection to a remote site.
 */
class NS_Cloner_Section_Teleport_Ftp extends NS_Cloner_Section {
	/**
	 * Mode ids that this section should be visible and active for.
	 *
	 * @var array
	 */
	public $modes_supported = [ 'clone_teleport' ];

	/**
	 * DOM id for section box.
	 *
	 * @var string
	 */
	public $id = 'teleport_ftp';

	/**
	 * Priority relative to other section boxes in UI.
	 *
	 * @var int
	 */
	public $ui_priority = 350;

	/**
	 * Output content for section settings box on admin page.
	 */
	public function render() {
		$this->open_section_box( __( 'Transfer files to a Destination Server with FTP/SFTP', 'ns-cloner' ) );
		?>
        <div class="teleport-ftp-configuration">
            <h5>
                <label class="ns-cloner-checkbox-label">
                    <input type="checkbox" name="teleport_ftp_use"
                           value="1"><?php esc_html_e( 'Use FTP/SFTP connection for transferring files to a remote server', 'ns-cloner' ); ?>
                </label>
            </h5>
            <div class="teleport-ftp-fields disabled">
                <h5>
                    <label for=""><?php esc_html_e( 'Connection Type', 'ns-cloner' ); ?></label>
                </h5>
                <div class="ns-cloner-input-group">
                    <select name="teleport_ftp_type">
                        <option value="ftp"><?php esc_html_e( 'FTP', 'ns-cloner' ); ?></option>
                        <option value="sftp"><?php esc_html_e( 'SFTP', 'ns-cloner' ); ?></option>
                    </select>
                </div>
                <?php if ( ! function_exists( 'ssh2_connect' ) ) : ?>
                    <div class="teleport-sftp-miss-error">
                        <h5 class="ns-cloner-disclaimer">
                            <strong><?php esc_html_e( 'SFTP connection requires SSH2 package being installed.', 'ns-cloner' ); ?></strong><br>
                            <strong><?php esc_html_e( 'Unfortunately your current server is missing the library. Please contact your host provider or install the package manually.', 'ns-cloner' ); ?></strong><br>
                            <strong><a href="<?php echo esc_url( 'https://www.php.net/manual/en/ssh2.requirements.php' ); ?>"
                                       target="_blank"><?php esc_html_e( 'More details here', 'ns-cloner' ); ?></a></strong>
                        </h5>
                    </div>
                <?php endif; ?>
                <h5>
                    <label for=""><?php esc_html_e( 'Host', 'ns-cloner' ); ?></label>
                </h5>
                <div class="ns-cloner-input-group">
                    <input type="text" name="teleport_ftp_host" placeholder="ftp.example.com"/>
                </div>
                <h5>
                    <label for=""><?php esc_html_e( 'Port', 'ns-cloner' ); ?></label>
                </h5>
                <div class="ns-cloner-input-group">
                    <input type="number" name="teleport_ftp_port" value="21"/>
                </div>
                <h5>
                    <label for=""><?php esc_html_e( 'Username', 'ns-cloner' ); ?></label>
                </h5>
                <div class="ns-cloner-input-group">
                    <input type="text" name="teleport_ftp_username"/>
                </div>
                <h5>
                    <label for="teleport_ftp_password"><?php esc_html_e( 'Password', 'ns-cloner' ); ?></label>
                </h5>
                <div class="ns-cloner-input-group">
                    <input type="password" name="teleport_ftp_password"/>
                </div>
                <h5>
                    <label for="teleport_ftp_root_path"><?php esc_html_e( 'Root path to WP installation', 'ns-cloner' ); ?></label>
                </h5>
                <div class="ns-cloner-input-group">
                    <input type="text" name="teleport_ftp_root_path" value="/"/>
                </div>
                <button class="ns-cloner-form-button teleport-ftp-verify-connection">Verify connection</button>
                <div class="teleport-ftp-fields-overlay"></div>
            </div>
            <div class="teleport-ftp-verify" data-type="check">
                <h5>
                    <span class="ns-cloner-loader-icon"><?php esc_html_e( 'Establishing connection to a remote server', 'ns-cloner' ); ?></span>
                </h5>
            </div>
            <div class="teleport-ftp-verify" data-type="success">
                <h5><?php esc_html_e( 'Connection confirmed! ', 'ns-cloner' ); ?></h5>
            </div>
            <div class="teleport-ftp-verify" data-type="fail">
                <h5><?php esc_html_e( 'Connection failed!', 'ns-cloner' ); ?></h5>
            </div>
        </div>
		<?php
		$this->close_section_box();
	}

	/**
	 * Check ns_cloner_request() and any validation error messages to $this->errors.
	 */
	public function validate() {
		$is_use_ftp = ns_cloner_request()->get( 'teleport_ftp_use' );
		if ( ! empty( $is_use_ftp ) ) {
			$can_connect = true;

			$ftp_type = trim( ns_cloner_request()->get( 'teleport_ftp_type', '' ) );
			if ( ! in_array( $ftp_type, array( 'ftp', 'sftp', true ) ) ) {
				$this->errors[] = __( 'Missing or invalid FTP type', 'ns-cloner' );
				$can_connect    = false;
			}

			$ftp_host = trim( ns_cloner_request()->get( 'teleport_ftp_host', '' ) );
			if ( empty( $ftp_host ) ) {
				$this->errors[] = __( 'Missing FTP/SFTP host', 'ns-cloner' );
				$can_connect    = false;
			}

			$ftp_port = trim( ns_cloner_request()->get( 'teleport_ftp_port', '' ) );
			if ( intval( $ftp_port ) <= 0 ) {
				$this->errors[] = __( 'Missing or invalid FTP/SFTP port', 'ns-cloner' );
				$can_connect    = false;
			}

			$ftp_user = trim( ns_cloner_request()->get( 'teleport_ftp_username', '' ) );
			if ( empty( $ftp_user ) ) {
				$this->errors[] = __( 'Missing FTP/SFTP username', 'ns-cloner' );
				$can_connect    = false;
			}

			$ftp_pass = trim( ns_cloner_request()->get( 'teleport_ftp_password', '' ) );
			if ( empty( $ftp_pass ) ) {
				$this->errors[] = __( 'Missing FTP/SFTP password', 'ns-cloner' );
				$can_connect    = false;
			}

			$ftp_path = trim( ns_cloner_request()->get( 'teleport_ftp_root_path', '' ) );
			$ftp_path = '/' . trim( $ftp_path, '/' );

			if ( ! $can_connect ) {
				return;
			}

			if ( $ftp_type === 'ftp' ) {
				//Check if ftp is supported.
				if ( ! function_exists( 'ftp_connect' ) ) {
					$this->errors[] = __( 'Sorry, your current server does not support FTP connection', 'ns-cloner' );

					return;
				}
				//Check ftp connection and authorization.
				$ftp_connection     = @ftp_connect( $ftp_host, $ftp_port );
				$ftp_authentication = @ftp_login( $ftp_connection, $ftp_user, $ftp_pass );
				if ( ! $ftp_connection || ! $ftp_authentication ) {
					$this->errors[] = __( 'Failed to connect to remote FTP with given credentials', 'ns-cloner' );

					return;
				}
				//Check if root path is a path to WP installation.
				if ( ! @ftp_chdir( $ftp_connection, trailingslashit( $ftp_path ) . 'wp-content' ) ) {
					$this->errors[] = __( 'Failed to find WP installation with given root path', 'ns-cloner' );

					return;
				}
				//Check if Cloner PRO is installed on remote server.
				if ( ! @ftp_chdir( $ftp_connection, trailingslashit( $ftp_path ) . 'wp-content/plugins/ns-cloner-pro' ) ) {
					$this->errors[] = __( 'Failed to find a Cloner PRO installed on a remote server', 'ns-cloner' );

					return;
				}
			} elseif ( $ftp_type === 'sftp' ) {
				//Check if sftp is supported.
				if ( ! function_exists( 'ssh2_connect' ) ) {
					$this->errors[] = __( 'Sorry, your current server does not support SFTP connection. Please install SSH2 package', 'ns-cloner' );

					return;
				}
				//Check sftp connection and authorization.
				$sftp_connection     = @ssh2_connect( $ftp_host, $ftp_port );
				$sftp_authentication = @ssh2_auth_password( $sftp_connection, $ftp_user, $ftp_pass );
				if ( ! $sftp_connection || ! $sftp_authentication ) {
					$this->errors[] = __( 'Failed to connect to remote SFTP with given credentials', 'ns-cloner' );

					return;
				}
				//Check if root path is a path to WP installation.
				$sftp = ssh2_sftp( $sftp_connection );
				if ( ! file_exists( "ssh2.sftp://{$sftp}" . trailingslashit( $ftp_path ) . 'wp-content' ) ) {
					$this->errors[] = __( 'Failed to find WP installation with given root path', 'ns-cloner' );

					return;
				}
				//Check if root path is a path to WP installation.
				if ( ! file_exists( "ssh2.sftp://{$sftp}" . trailingslashit( $ftp_path ) . 'wp-content/plugins/ns-cloner-pro' ) ) {
					$this->errors[] = __( 'Failed to find a Cloner PRO installed on a remote server', 'ns-cloner' );

					return;
				}
			}
		}
	}
}
