<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link       https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package    WordPress
 * @subpackage Lebe
 * @since      1.0
 * @version    1.0
 */

get_header(); ?>
    <div class="container">
        <div class="text-center page-404">
            <div class="img-404"></div>
            <h1 class="heading">
				<?php esc_html_e( 'Oops', 'wilkinson-sword' ); ?>
            </h1>
            <h2 class="title">
                <?php esc_html_e( 'We couldn\'t find that page', 'wilkinson-sword' ); ?><br>
                <?php esc_html_e( 'Click the link below to return home.', 'wilkinson-sword' ); ?>
            </h2>
			<?php //get_search_form(); ?>
            <a class="button"
               href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Back To Home Page', 'wilkinson-sword' ); ?></a>
        </div>
    </div>
<?php get_footer();
