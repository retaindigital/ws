jQuery(document).ready(function($){
 
 	// Removes cached switcher
	$("#geoSwitcher").remove();

	// Set Cookie through JS
	function setCookie(cname, cvalue, exdays) {
	  var d = new Date();
	  d.setTime(d.getTime() + (exdays*24*60*60*1000));
	  var expires = "expires="+ d.toUTCString();
	  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	// Set landing page detail panels to be equal heights
	$(window).on('load resize',function(e){
		if ($(window).width() >= 767) {
			var maxHeight = 0;
			$('.male .detail-panel').each(function(){
			   var thisH = $(this).height();
			   if (thisH > maxHeight) { maxHeight = thisH; }
			});

			$('.male .detail-panel').height(maxHeight);

		} else {
			$('.detail-panel').height('auto');
		}
	});

	// Retrieve Cookie through JS
	function getCookie(cname) {
	  var name = cname + "=";
	  var decodedCookie = decodeURIComponent(document.cookie);
	  var ca = decodedCookie.split(';');
	  for(var i = 0; i <ca.length; i++) {
	    var c = ca[i];
	    while (c.charAt(0) == ' ') {
	      c = c.substring(1);
	    }
	    if (c.indexOf(name) == 0) {
	      return c.substring(name.length, c.length);
	    }
	  }
	  return "";
	}

	// Cookie Name
	var geoCookie = 'ws_geo_location';

 	// Check to see if we need to offer a different location (driving by ws plugin)
	if (getCookie(geoCookie) == '') {

		// Remove the trailing slash
		var lang_loc = $('body').data("lang-loc").slice(0, -1);

		$.get( "/"+lang_loc+"/wp-json/wc/v2/get-location", function( data ) {

			// If we have a successful offer and the current site is not equal to the suggested site
			if(data.status == 'success' && data.offer != lang_loc){

				// Create the offer link based on the offered site and hreflang tag
		  	var offer_link = $('link[hreflang^="'+data.offer+'"]').attr("href");

		  	// if there's not hreflang, offer the home page
		  	if( offer_link == null){
		  		var offer_link = data.go_to_url;
		  	} 

		  	// replace the placeholder with the offer link
		  	var popup = data.popup_code.replace("{{GO_TO_URL}}", offer_link);

		  	// Append the popup
		  	$('body').append( popup );

		  	/* 
		  	 * Set the cookie to stop it displaying again.
		  	*/

		  	// Set the date 
				var d = new Date();

				// Set the time, 1 year in the future
				d.setTime(d.getTime() + (365*24*60*60*1000));
	  		var expires = "expires="+ d.toUTCString();

	  		// Create cookie
				setCookie(geoCookie, data.offer, 365);

		  	// Show the popup REMOVE FOR LIVE
		  	$("#geoSwitcher").modal("show");


			}
	
		});

	}

  $(document).on('click', '.contact_box.chat',  function (e) {
  	console.log('clicked chat');
		$('button[class^="twyla-widget__Greeting__getStartedButton___1lotz"]').click();
	});



	// Mini Cart height
	function height_mini_cart() {
		if ($(window).width() < 1025) {
			$('.header .minicart-list-items').css({'height': $(window).outerHeight() - 223 + 'px'});
		}
	}

	height_mini_cart();

	// Reinit some important things after ajax
	$(document).ajaxComplete(function (event, xhr, settings) {
	  height_mini_cart();  
	});


	$(".add-to-cart-scroll").click(function() {
	    $([document.documentElement, document.body]).animate({
	        scrollTop: $(".variations_form").offset().top
	    }, 500);
	});

	// bvCallback must be used when interacting with BV via JavaScript and using the `async` attribute. 
	window.bvCallback = function (BV) {
		// Register a function to be called when a BV feature needs to display the R&R element, such as Rating Summary's stars
	  BV.reviews.on('show', function () {
	     // If the R&R container is hidden (such as behind a tab), put code here to make it visible (open the tab)
	     $('#tab-title-reviews a').click();
	  });
	};

});

