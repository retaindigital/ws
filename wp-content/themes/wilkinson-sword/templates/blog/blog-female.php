<?php 
	$panel_image_ID 	= get_field('panel_image');
	$panel_image 		= wp_get_attachment_image($panel_image_ID, 'medium');
?>
     			<div class="thumb">
					<?php 
						echo '<div class="pseudo-background-img-container">';
						echo $panel_image_ID ? $panel_image : the_post_thumbnail() ;
						echo '</div>';
					?>
                </div>
                <div class="info">
  				<div class="content-info">
	  				<?php
	  					$string1 = '';
	  					$string2 = '';
	                	$text = get_the_title();
	                	$length = floor(strlen($text));

						if ($length <= 15){
							$middle = stripos($text, ' ') + 1;
						} else {
							$middle = strripos(substr($text, 0, floor(strlen($text) / 2)), ' ') + 1;	
						}
						if ($middle >= 2){
							$string1 = substr($text, 0, $middle); 
							$string2 = substr($text, $middle); 
						} else {
							$string1 = $text; 
						}

						echo '<div class="panel_header">';
							echo '<span>' . $string1 . '</span>';
							echo $string2 ? '<span>' . $string2 . '</span>' : '';
						echo '</div>';
	  				?>
                        <div class="post-excerpt"><?php echo wp_trim_words( apply_filters( 'the_excerpt', get_the_excerpt() ), 15, esc_html__( '...', 'lebe' ) ); ?></div>
                    </div>
                 </div>
                  <a class="button blog" href="<?php the_permalink(); ?>"><span><?php echo esc_html__( 'Continue reading', 'lebe' ); ?></span></a>


