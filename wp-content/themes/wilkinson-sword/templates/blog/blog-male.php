<?php $width  = 440;
      $height = 503;
      $panel_image_ID   = get_field('panel_image');
      $panel_image      = wp_get_attachment_url($panel_image_ID, 'large');
      $blog_url   = get_the_permalink();
?>
     <div class="post-thumb-grid">
          <?php 
            echo $panel_image_ID ? '<div class="post-thumb"><a class="thumb-link" href="' . $blog_url . '"><img class="fami-img fami-lazy lazy img-responsive attachment-post-thumbnail wp-post-image lazy-loaded" width="440" height="475" src="' . $panel_image . '" alt="" style="display: block;"></a></div>' : lebe_post_thumbnail( $width, $height) ;
           ?>
        </div>

    <div class="post-info">
        <div class="content-info">
            <h3 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            <div class="post-excerpt"><?php echo wp_trim_words( apply_filters( 'the_excerpt', get_the_excerpt() ), 15, esc_html__( '...', 'lebe' ) ); ?></div>
        </div>
      <a class="button blog" href="<?php the_permalink(); ?>"><span><?php echo esc_html__( 'Continue reading', 'lebe' ); ?></span></a>
    </div>