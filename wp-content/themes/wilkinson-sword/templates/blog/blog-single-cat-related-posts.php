<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$cats = wp_get_post_categories( $post->ID );

if ( $cats ) {
	$cat_ids = array();
	foreach ( $cats as $cat ) {
		$cat_ids[] = $cat;
	}

	$args          = array(
		'category__in'             => $cat_ids,
		'post__not_in'        => array( $post->ID ),
		'posts_per_page'      => 5,
		'ignore_sticky_posts' => 1
	);
	$related_query = new WP_Query( $args );
	if ( $related_query->have_posts() ) {
	// 	$data_responsive = array(
	// 		'0'    => array(
	// 			'items' => 1,
	// 		),
	// 		'480'  => array(
	// 			'items' => 2
	// 		),
	// 		'768'  => array(
	// 			'items' => 3
	// 		),
	// 		'992'  => array(
	// 			'items' => 3
	// 		),
	// 		'1200' => array(
	// 			'items' => 3
	// 		),
	// 		'1500' => array(
	// 			'items' => 3
	// 		),
	// 	);
	// 	$data_responsive = json_encode( $data_responsive );

		?>
        <h3 class="related-posts-title"><?php esc_html_e( 'Related Posts', 'lebe' ); ?></h3>
        <div class="lebe-blog style-01 lebe-related-posts-wrap ">
            <div class="owl-carousel" data-autoplay="false" data-nav="false" data-dots="true" data-loop="false"
                 data-margin="30" data-responsive="<?php echo esc_attr( htmlentities2( $data_responsive ) ); ?>">
				<?php
				while ( $related_query->have_posts() ) : $related_query->the_post(); ?>
					<?php 
						//choose the right template
						$term = get_the_category($post);
		                if ( $term->parent > 0 ) {
		                    $term = get_term_by('id', $term->parent, 'category');
		                 } 
		                 $ws_blog_style  = get_field('blog_style','category_' . $term[0]->term_id);
		                $post_classes[] = $ws_blog_style;
		                $post_classes[] = 'post-item';
					?>
					<div class="blog-content grid auto-clear row">
					 <article <?php post_class( $post_classes ); ?>>
					<?php 
						if ( $ws_blog_style && $ws_blog_style != 'generic' ){
							
                        	get_template_part( 'templates/blog/blog', $ws_blog_style );
                    	} else {
                    		get_template_part( 'templates/blog/blog-styles/content-blog', 'style-01' ); 
                    	}
                    	?>
                    </article>
                </div>
					<?php
				endwhile;
				?>
            </div>
        </div>
		<?php
	}
	wp_reset_postdata();
}