<?php
/**
 * Template Name: Full Width Page
 *
 * @package WordPress
 * @subpackage Lebe
 * @since Lebe 1.0
 *
 *Template override created by LL to add in breadcrumb trail
 */
get_header();

?>
    <div class="fullwidth-template">
        <div class="container">
            <?php get_template_part( 'template-parts/part', 'breadcrumb' ); ?>
            <?php

            // Start the loop.
            while ( have_posts() ) : the_post();
                ?>
                <?php the_content(); ?>
                <?php
                // End the loop.
            endwhile;
            ?>
        </div>
    </div>
<?php
get_footer();