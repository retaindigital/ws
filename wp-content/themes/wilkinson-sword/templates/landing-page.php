<!-- 
Template name:Landing page  
-->
<?php get_header(); ?>

<?php
// Get correct background image
	// $image_ID 							  = attachment_url_to_postid( esc_url( $page_img_banner['image'] ) );
	// $image_ID 							  = attachment_url_to_postid( esc_url( '/en-gb/wp-content/uploads/sites/4/2019/09/hydro-generic.gif' ) );
	// list ($urlSmall,$urlMedium,$urlLarge) = get_image_sizes($image_ID);

// Get defaults
$settings 				= get_field('settings');
$page_intro 			= get_field('page_intro');
$postParent 			= wp_get_post_parent_id($post);

if ($settings){
	//find data form the new field layout
	$page_type 				= $settings['page_type'];
	$header_style 			= ' ' . $settings['header_style'] . '_style';	
	$page_default_color 	= $settings['page_default_colour'];
	$button_default_colour 	= $settings['button_default_colour'];
	$main_heading 			= $page_intro['main_heading'];
	$secondary_heading 		= $page_intro['secondary_heading'];
	$intro_copy 			= $page_intro['intro_copy'];
	$full_copy 				= $page_intro['full_intro'];
} else {
	// find data form the old field layout
	$page_type 				= get_field('page_type');
	$page_default_color 	= get_field('page_default_colour');
	$button_default_colour 	= get_field('button_default_colour');
	$main_heading 			= get_field('main_heading');
	$secondary_heading 		= get_field('secondary_heading');
	$intro_copy 			= get_field('intro_copy');
	$full_copy 				= get_field('full_intro');
}
// $equal_heights 			= get_field('equal_row_heights');
// $equal_heights_class 	= ($equal_heights) ? 'equal-heights':'';

if ( $page_default_color ){
	echo '<style type="text/css">
			.page-template-landing-page .main-content h1, .page-template-landing-page .main-content .female h1, .page-main-content .panel a.button{
				color:' . $page_default_color . ';
			}
			.single-landing_page .main-content .panel.detail-panel{
				border-color:' . $page_default_color . ';
			}
		</style>';
}


?>

    <main class="site-main main-container no-sidebar">
                <div class="main-content">
					<?php
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post();
							?>
                            <div class="page-main-content <?php echo $page_type; echo $header_style; ?>">
								<?php //the_content(); ?>
                            	<div class="container">
                            		<?php 
							    		if ( $postParent > 0 ){
							    			get_template_part( 'template-parts/part', 'breadcrumb' );
							    		}    
							    	?>
                            		<?php if ( ( $main_heading  || $intro_copy || $full_copy || $secondary_heading['image_or_text'] == 'text' || $secondary_heading['image_or_text'] == 'img')) :
                            		 //special header for female masterbrand
                            		  if ( $page_type != 'female masterbrand' ){
                            		 // 	echo '<div class="intro col-sm-12">';
                            		 // } else {
                            		 	echo '<div class="intro col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">';
			                //Main heading
			                            		if ($main_heading || $secondary_heading) :
			                            			echo '<h1>';
			                            			echo ($main_heading) ?  '<span class="subheading">' . $main_heading . '</span>': '';

			                            			if ( $secondary_heading ){
			                            				$secondary_type 	= $secondary_heading['image_or_text'];
			                            				$secondary_image 	= $secondary_heading['brand_logo'];
			                            				$secondary_text 	= $secondary_heading['text'];
			                            				if ( $secondary_type == 'img' ){
			                            					list ($urlSmall,$urlMedium,$urlLarge) = get_image_sizes($secondary_image);
															$alt 			= get_post_meta($secondary_image, '_wp_attachment_image_alt', true);
															$alt_text		= ( $alt ) ? 'alt="' . $alt . '"' : '' ;
				                            	 			echo '<span class="sr-only sr-only-focusable">' . $alt . '</span><img class="landing_logo" src="' . $urlLarge . '" ' . $alt_text . '>';
			                            				} elseif ( $secondary_type == 'text' ){
				                            	 			echo '<span class="main_heading">' . $secondary_text . '</span>';
			                            				}
			                            			}
			                            		echo '</h1>';
			                   	        		endif;

			                // intro text
			                   	        		if ($intro_copy || $full_copy) :

													echo ($intro_copy) ? '<p><Strong>' . $intro_copy . '</strong></p>' : '';
													echo ($full_copy) ? '<p>' . $full_copy . '</p>' : '';
												endif;

											
										echo '</div>';
										}
										?>
								<?php endif; ?>

								<?php if( have_rows('panels') ):
										// echo '<div class="lp_panels ' . $equal_heights_class . '">';
									echo '<div class="lp_panels">';
    									while ( have_rows('panels') ) : the_row();

    										// Feature panel
    										if( get_row_layout() == 'feature_panel' ):

												$copy 		= get_sub_field('copy');
												$header 	= $copy['heading'];
												$body 		= $copy['paragraph'];
												$image 		= get_sub_field('image');
												$button_color 			= ( $button_default_colour ) ? 'style="color:' . $button_default_colour . '"' :  'style="color:' . $page_default_color . '"'  ;
												$button 				= get_sub_field('button');
												$button_type 			= $button['button_type'];
												$button_text 			= $button['button_text'];
												$button_searchterm 		= $button['search_term'];
												$button_cat 			= get_term_link( $button['button_category'] );
												$buttonlink_search		= ( $button_searchterm ) ? site_url() . '/?s=' . str_replace(' ', '+', $button_searchterm) . '&post_type=product' : '';
												$buttonlink_onsite 		= get_permalink( $button['button_link'] );
												$buttonlink_offsite		= $button['button_url'];
												// $button_link 			= ( $buttonlink_onsite ) ? $buttonlink_onsite : $buttonlink_offsite;


												if ( $button_type == 'url' && (isset( $buttonlink_onsite ) && !empty($buttonlink_onsite) ) ){
													$button_link = $buttonlink_onsite;
												} elseif ( $button_type == 'search' && (isset( $buttonlink_search ) && !empty($buttonlink_search ) )) {
													$button_link = $buttonlink_search;
												} elseif ( $button_type == 'link' && (isset( $buttonlink_offsite ) && !empty($buttonlink_offsite ) )) {
													$button_link = $buttonlink_offsite;
												} elseif ( $button_type == 'category' && (isset( $button_cat ) && !empty($button_cat ) )) {
													$button_link = $button_cat;
												}
													echo '<div class="panel feature-panel">
															<div class="image col-xs-12 col-sm-6 col-lg-5" style="background-image:url(' . $image .  ')"><div class="pseudo-triangle"></div></div>
															<div class="copy col-xs-12 col-sm-6 col-lg-7">';
																if ($page_type == 'subscription' || $page_type == 'female-subs subscription'){
																	echo '<div class="subs_number"></div>';
																}
															echo '<div class="copy_container">';
																	echo ($header) ? '<h3>' . $header . '</h3>' : '';
																	echo ($body) ? $body : '' ;
																	echo ( $button_type != 'nolink' && $button_link ) ? '<p><a href="' . $button_link . '" class="button" ' . $button_color . ' >' . $button_text . '</a></p>' : '';
																echo '</div>';
															echo '</div>
														</div>';
									 		endif;

											// Content panel
    										if( get_row_layout() == 'content_panel' ):

												$copy 		= get_sub_field('copy');
												$header 	= get_sub_field('header');

													echo '<div class="panel content-panel">';
															echo ($header) ? '<h3>' . $header . '</h3>' : '';
															echo ($copy) ? '<div class="copy col-xs-12">' . $copy . '</div>': '';
													echo '</div>';
									 		endif;

											// detail-panel panel - image and content
    										if( get_row_layout() == 'detail_panel' ):

												$copy 					= get_sub_field('copy');
												$header 				= $copy['heading'];
												$sub_header 			= $copy['sub-heading'];
												$body 					= $copy['paragraph'];
												$logo_image_src 		= $copy['logo_image'];
													if ($logo_image_src) :
		                            	 				$logo_image 	= '<img class="landing_logo" src="' . $logo_image_src . '" ' . $alt_text . '>';
		                            	 			endif;
												$image 					= get_sub_field('image');
												$image_url 				= $image['url'];
												$srcset 				= wp_get_attachment_image_srcset( $image['ID'], array( 400, 200 ) );
												$full_width_image 		= get_sub_field('detail_image_panel');
												$panel_color 			= get_sub_field('panel_colour');
												$border_color			= ( $panel_color ) ? $panel_color : '#868686';
												$button_color 			= ( $button_default_colour ) ? 'style="color:' . $button_default_colour . '"' :  'style="color:' . $panel_color . '"'  ;
												$button 				= get_sub_field('button');
												$button_type 			= $button['button_type'];
												$button_text 			= $button['button_text'];
												$button_searchterm 		= $button['search_term'];
												$button_cat 			= get_term_link( $button['button_category'] );
												$buttonlink_search		= ( $button_searchterm ) ? site_url() . '/?s=' . str_replace(' ', '+', $button_searchterm) . '&post_type=product' : '';
												$buttonlink_onsite 		= get_permalink( $button['button_link'] );
												$buttonlink_offsite		= $button['button_url'];
												// $button_link 			= ( $buttonlink_onsite ) ? $buttonlink_onsite : $buttonlink_offsite;


												if ( $button_type == 'url' && (isset( $buttonlink_onsite ) && !empty($buttonlink_onsite) ) ){
													$button_link = $buttonlink_onsite;
												} elseif ( $button_type == 'search' && (isset( $buttonlink_search ) && !empty($buttonlink_search ) )) {
													$button_link = $buttonlink_search;
												} elseif ( $button_type == 'link' && (isset( $buttonlink_offsite ) && !empty($buttonlink_offsite ) )) {
													$button_link = $buttonlink_offsite;
												} elseif ( $button_type == 'category' && (isset( $button_cat ) && !empty($button_cat ) )) {
													$button_link = $button_cat;
												}

												// if ($page_type == 'female') {
												// 	echo '<div class="panel detail-panel" ' . $border_color . '>
												// 			<div class="image" style="background-image:url(' . $image . ')">
												// 			</div>
												// 			<div class="copy col-xs-12 col-sm-7 col-md-6 col-lg-6">
												// 				<div class="copy_container">';
												// 					echo ( $logo_image ) ? $logo_image : '';
												// 					echo ( $header ) ? '<h3>' . $header . '</h3>' : '';
												// 					echo ( $sub_header ) ? '<h4>' . $sub_header . '</h4>' : '';
												// 					echo ( $body ) ? $body : '';
												// 					echo ( $button_type != 'nolink' && $button_link ) ? '<p><a href="' . $button_link . '" class="button" ' . $button_color . ' >' . $button_text . '</a></p>' : '';
												// 					echo '</div>
												// 				</div>';
												// 	echo '</div>';
												// } else {
													// echo '<div class="pseudo-border" ' . $border_color . '></div>';
													

													if ( $full_width_image ) { 	
															echo '<div class="panel panel-image" style="border-top:12px solid ' . $border_color . ';padding-top:1em;">
																	<div class="image-full col-xs-12" style="background-image:url(' . $full_width_image . ')"></div>
																</div>'; 
														}
															echo '<div class="panel detail-panel"';
															
												// Image
															if ($page_type == 'female masterbrand'){
																echo '/>';
																if ($panel_color){
																	echo '<span class="bg-color"  style="background-color:' . $panel_color . ';"></span>';
																}
																echo '<a href="' . $button_link . '" class="thumb" style="background-color:' . $panel_color . ';">';
																	echo '<span class="bgcolor-canceller"></span>'; //background is added to thumb so that it can be inherited by the circle pseudo element which follows it. We don't actually want the thumb to have a background so we are cancelling it out with this span as an overlay. 
																		echo '<div class="pseudo-background-img-container">';
																			echo '<img src="' . $image_url . '" srcset="' . $srcset . '">';
																		echo '</div>';
										                			// echo '</div>';
										                		echo '</a>';
										                		echo '<div class="copy col-xs-12 col-medium-6">
																<div class="copy_container">';
																	echo ( $logo_image ) ? $logo_image : '';
																	echo ( $header ) ? '<h3><a class="panel_header" href="' . $button_link . '">' . $header . '</a></h3>' : '';
																	echo ( $sub_header ) ? '<h4>' . $sub_header . '</h4>' : '';
																	echo ( $body ) ? $body : '';
																	echo ( $button_type != 'nolink' && $button_link ) ? '<p class="alignright"><a href="' . $button_link . '" class="button blog"><span>' . $button_text . '</a></span></p>' : '';
																	echo '</div>
																</div>';

															} else {
																echo (!$full_width_image) ? 'style="border-top:12px solid ' . $border_color . ';"' : '';
																echo '/>';
																echo '<div class="image ';
																echo ( $page_type != 'female' ) ? 'col-xs-12 col-sm-6': '';
																echo '" style="background-image:url(' . $image_url . ')">';
																	echo ($page_type == 'male') ? '<div class="pseudo-triangle"></div>' : '';
																echo '</div>';
																echo '<div class="copy ';
															echo ( $page_type != 'female' ) ? 'col-xs-12 col-sm-6': '';
															echo '">
																<div class="copy_container">';
																	echo ( $logo_image ) ? $logo_image : '';
																	echo ( $header ) ? '<h3>' . $header . '</h3>' : '';
																	echo ( $sub_header ) ? '<h4>' . $sub_header . '</h4>' : '';
																	echo ( $body ) ? $body : '';
																	echo ( $button_type != 'nolink' && $button_link ) ? '<p><a href="' . $button_link . '" class="button" ' . $button_color . ' >' . $button_text . '</a></p>' : '';
																	echo '</div>
																</div>';
															}

													echo '</div>';
												// }

									 		endif;

									 		// Image panel
									 		if( get_row_layout() == 'image_panel' ):
												// Get the variables
												$image 		= get_sub_field('image_panel');

												echo '<div class="panel">
														<div class="image-full col-xs-12" style="background-image:url(' . $image . ')"></div>
													</div>';
									 		endif;
// Remove this layout once new layouts have been implemented
									 		if( get_row_layout() == 'video_panel' ):
												// Get the variables
												$oembed 		= get_sub_field('video');
												$video_copy  	= get_sub_field('video_copy');
												// $left_width 	= ($page_type == 'female') ?  'col-xs-12 col-sm-7 col-md-5 col-lg-4' : 'col-xs-12 col-sm-6';

												if ( $video_copy) {
													echo '<div class="panel video-panel">
															<div class="copy video-copy col-xs-12"><h5>'
																. $video_copy .
															'</h5></div>
															<div class="embed-responsive embed-responsive-21by9">
														  		<iframe class="embed-responsive-item" src="'. $oembed .'" allowfullscreen></iframe>
														</div>
														</div>';
												} else {
												echo '<div class="panel video-panel">
														<div class="embed-responsive embed-responsive-21by9">
														  <iframe class="embed-responsive-item" src="'. $oembed .'" allowfullscreen></iframe>
														</div>
													</div>';
												}
									 		endif;
// Embed panel
									 		if( get_row_layout() == 'embed_panel' ):
												// Get the variables
												$embed_type 	= get_sub_field('type_of_embed');
												$video_ref 		= get_sub_field('video_ref');
												$other_code  	= get_sub_field('raw_code');
												$gravity_code  	= get_sub_field('form_embed');
												$embed_copy  	= get_sub_field('embed_copy');

												// if ( $embed_copy) {
												// 	echo '<div class="panel embed-panel">
												// 			<div class="copy video-copy col-xs-12"><h5>'
												// 				. $embed_copy .
												// 			'</h5></div>';
												// 			if ( $embed_type == 'video' && $video_url ){
												// 				echo '<div class="embed-responsive embed-responsive-21by9">
												// 		  			<iframe class="embed-responsive-item" src="https://www.youtube-nocookie.com/embed/'. $video_ref .'" allowfullscreen></iframe>
												// 				</div>';
												// 			} elseif ( $embed_type == 'other' && $other_code ) {
												// 				echo $other_code;
												// 			}
												// 	echo '</div>';
												// } else {
													echo '<div class="panel embed-panel">';
															if ( $embed_copy) {
																echo '<div class="copy video-copy col-xs-12">'
																		. $embed_copy .
																	'</div>';
															}

															if ( $embed_type == 'video' && $video_ref ){
																echo '<div class="embed-responsive embed-responsive-21by9">
														  				<iframe class="embed-responsive-item" src="https://www.youtube-nocookie.com/embed/'. $video_ref .'" allowfullscreen></iframe>
																	</div>';
															} elseif ( $embed_type == 'gravity_form' && $gravity_code ) {
																$gravity_id 			= $gravity_code['gravity_form_id'];
																$gravity_title 			= $gravity_code['show_title'] ? 'true' : 'false';
																$gravity_description 	= $gravity_code['form_description'] ? 'true' : 'false';
																echo do_shortcode( '[gravityform id="' . $gravity_id . '" title="' . $gravity_title . '" description="' . $gravity_description .'"]' );
															} elseif ( $embed_type == 'other' && $other_code ) {
																echo $other_code;
															}
														echo '</div>';
												// }
									 		endif;

									 											 		// Image panel
									 		if( get_row_layout() == 'products_panel' ):
												// Get the variables
												$prod_cat 			= get_sub_field('product_category');
												$prod_cat_slug 		= $prod_cat->slug;
												$prod_per_page 		= get_sub_field('total_items');
												$section_header 	= get_sub_field('section_header');
												$info_box 			= get_sub_field('information_box');
												$infobox_link_text	= $info_box['link_text'];
												$infobox_content 	= $info_box['modal_content'];

												echo '<div class="product_panel">';
												if ( $info_box ) {
												?>
													<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
													  <div class="modal-dialog" role="document">
													    <div class="ws-modal-content">
													        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													          <span aria-hidden="true">&times;</span>
													        </button>
													      <div class="modal-body">

													        <?php echo ( $infobox_content ) ? '<i class="fas fa-question-circle"></i>' . $infobox_content : 'Add some modal' ; ?>
													      </div>
													    </div>
													  </div>
													</div>
												<?php } ?>
												<?php 
													echo ($section_header) ? '<h3>' . $section_header . '</h3>' : '';
													echo '<button type="button" class="button question" data-toggle="modal" data-target="#exampleModalLong">';
													echo ( $infobox_link_text ) ? '<i class="fas fa-question-circle"></i>' . $infobox_link_text : 'Add some linking text here' ;
													echo '</button>';
													echo do_shortcode('[lebe_products product_style="1" per_page="' . $prod_per_page . '" product_image_size="300x300" taxonomy="' . $prod_cat_slug . '" target="best-selling" boostrap_bg_items="3" boostrap_md_items="3"]');
												echo '</div>';

									 		endif;

									 	endwhile;

									 	echo '</div>';

									 endif;
									 echo '</div>';
									 echo '<div class="container">';
										 	 the_content();
										echo '</div>';


						}
					}
					// do_action( 'woocommerce_after_single_product_summary' );


					?>
            </div>
    </main>
<?php get_footer(); ?>
