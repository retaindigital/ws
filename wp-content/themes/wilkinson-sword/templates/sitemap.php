<?php
/**
 * Template Name: Sitemap
 *
 * @package WordPress
 *
 *
 */

get_header();

?>

<main class="site-main">
  <h1 style="position:absolute;left-3000px;height:0px;font-size:0;"><?php the_title(); ?></h1>
  <div class="container">
    <div class="row">
      <h2 id="pages"><?php print esc_html__('Pages', 'wilkinson-sword'); ?></h2>
      <ul>
        <?php 
          // Add pages you'd like to exclude in the exclude here
        wp_list_pages( 
          array( 'exclude' => '',
            'title_li' => '',
          )
        );
        ?>
      </ul>

      <h2 id="posts"><?php print esc_html__('Posts', 'wilkinson-sword'); ?></h2>
      <ul>
        <?php
        // Add categories you'd like to exclude in the exclude here
        $cats = get_categories('exclude=');
        foreach ($cats as $cat) {
          print '<li>';
          print '<h3>' . $cat->cat_name . '</h3>';
          print "<ul>";
          query_posts('posts_per_page=-1&amp;cat='.$cat->cat_ID);
          while( have_posts() ) {
            the_post();
            $category = get_the_category();
            // Only display a post link once, even if it's in multiple categories
            if ($category[0]->cat_ID == $cat->cat_ID) {
              print '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
            }
          }
          print "</ul>";
          print "</li>";
        }
        ?>
      </ul>

      <h2 id="product-cats"><?php print esc_html__('Product Categories', 'wilkinson-sword'); ?></h2>
      <?php print wp_list_categories( array('taxonomy' => 'product_cat', 'title_li'  => '') ); ?>

      <h2 id="products"><?php print esc_html__('Products', 'wilkinson-sword'); ?></h2>
      <ul>
        <?php 
        $products = wc_get_products($args = array(
          'limit' => '-1',
          'orderby' => 'name',
          'order' => 'asc',
        ));
        ?>
        <?php foreach ($products as $product): ?>
          <?php if( $product->get_catalog_visibility() == 'visible'): ?>
            <li><a href="<?php print get_permalink($product->get_id()); ?>"><?php print $product->name; ?></a></li>
          <?php endif; ?>
        <?php endforeach; ?>
      </ul>

    </div>
  </div>
</main>
<?php get_footer(); ?>