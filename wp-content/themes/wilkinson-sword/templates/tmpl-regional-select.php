<!-- 
Template name:Regional select page  
-->

<?php get_header(); ?>
<div class="pageWrapper">
	<header>
		<div class="logowrap"><img src="/wp-content/themes/wilkinson-sword/assets/Wilkinson_landscape_style_No_Strikethrough.jpg" alt="Wilkinson Sword logo - Sharpening your style since 1772"></div>
		<h2> Choose a location</h2>
		<!-- Don't forget srcset! -->
	</header>
    <main class="site-main">
    	<!-- Modal -->
    	<div class="modal fade" id="regionalHelp" tabindex="-1" role="dialog" aria-labelledby="regionalHelp" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="ws-modal-content">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">CLOSE<i class="far fa-times-circle"></i></span>
		        </button>
			      <div class="modal-body">
			        <?php 
			        	$modalContent 		= get_field('modal_content');
			        	$contactDetails 	= $modalContent['contact_details'];
			        	if ($modalContent){
			        		echo $modalContent['header'] ? '<h2>' . $modalContent['header'] . '</h2>' : '';
			        		echo '<div class="maincontent">';
			        		echo $modalContent['content'] ? '<div>' . $modalContent['content'] . '</div>' : '';
			        	}
			        	if ($contactDetails){
			        		echo '<address class="contact_details">';
			        		echo $contactDetails['header'] ? '<h3>' . $contactDetails['header'] . '</h3>' : '';
			        		echo $contactDetails['contact_methods'] ? '<div>' . $contactDetails['contact_methods'] . '</div>' : '';
			        		echo '</address>';
			        		echo '</div>';
			        	}
			        ?>
			      </div>
		    </div>
		  </div>
		</div>
        <div class="main-content">
			<div class="graphic">
				<?php 
					// the_post_thumbnail('large');
				?>
				<!-- <img src="/wp-content/themes/wilkinson-sword/assets/Wilkinson_1000x1000.jpeg"> -->
				<!-- Don't forget srcset! -->
				<h3 class="strapline">Sharpening your style since 1772</h3>
			</div>
			<div class="locations">
				<!-- Country select -->
				<div><h2> Choose a location</h2></div>
				<!-- Get values from ACF fields -->
				<?php 
					if( have_rows('country_website') ):
						echo '<ul class="country-select">';
						 while ( have_rows('country_website') ) : the_row();
						 	$country 		= 	get_sub_field('country');
						 	if ($country):
						 		$countryName 	= ($country['country_name']['label'] != 'Please select') ? $country['country_name']['label'] : $country['custom_country'];
						 		$countryValue 	= ($country['country_name']['value'] != 'Please select') ? $country['country_name']['value'] : $country['custom_country'];
						 		$countryURL 			= get_sub_field('country_website_url');
						 		$customFlagID 			= get_sub_field('custom_flag');
						 		$custom_flag_option 	= get_sub_field('custom_flag_option');
						 		echo '<li class="' . $countryName . '">

						 				<a href="' . $countryURL . '">';
						 				if ($custom_flag_option == 'customflag' && $customFlagID){
						 					$image =  wp_get_attachment_image_src($customFlagID, 'medium');
						 					echo '<img src="' . $image['0'] . '" alt="' . $countryName . ' flag" />';
						 					// var_dump($customFlag);
						 				} else {
						 					echo '<img src="/wp-content/themes/wilkinson-sword/assets/flags/' . $countryValue . '.png" alt="' . $countryName . ' flag" />';
						 				}
						 				echo $countryName;
						 				echo '</a>
						 			</li>';
						 	endif;
						 endwhile;
					endif;
				?>
			</div>
			
    </main>
	<div class="mobile-footer-nav">
		<!-- Info button -->
		<a class="button question" data-toggle="modal" data-target="#regionalHelp">Need help?</a>
    </div>
	<footer>
		<p>&copy; <?php echo date('Y'); ?> Wilkinson Sword. All rights reserved</p>
	</footer>    
</div>
<?php get_footer(); ?>