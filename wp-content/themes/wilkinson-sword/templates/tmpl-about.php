<!-- 
Template name:About page  
-->
<?php get_header(); ?>

<?php
// Get correct background image
	// $image_ID 							  = attachment_url_to_postid( esc_url( $page_img_banner['image'] ) );
	// $image_ID 							  = attachment_url_to_postid( esc_url( '/en-gb/wp-content/uploads/sites/4/2019/09/hydro-generic.gif' ) );
	// list ($urlSmall,$urlMedium,$urlLarge) = get_image_sizes($image_ID);

// Get defaults

$page_intro 			= get_field('page_intro');
$postParent 			= wp_get_post_parent_id($post);
$page_intro 			= get_field('page_intro');

	$page_type 				= 'male timeline'; /*its not really but we can use the styles*/
	$page_default_color 	= get_field('page_default_colour');
	$button_default_colour 	= get_field('button_default_colour');
	$main_heading 			= $page_intro['main_heading'];
	$intro_copy 			= $page_intro['intro_copy'];
	$quick_nav 				= $page_intro['quick_nav'];	

// $equal_heights 			= get_field('equal_row_heights');
// $equal_heights_class 	= ($equal_heights) ? 'equal-heights':'';



?>

    <main class="site-main main-container no-sidebar">
                <div class="main-content">
					<?php
					if ( have_posts() ) {
						while ( have_posts() ) {
							the_post();
							?>
                            <div class="page-main-content <?php echo $page_type; ?>">
								<?php //the_content(); ?>
                            	<div class="container">
                            		<?php 
							    		if ( $postParent > 0 ){
							    			get_template_part( 'template-parts/part', 'breadcrumb' );
							    		}    
							    	?>
                            		<?php if ( $main_heading  || $intro_copy ) :
                            		 ?>
	                            		<div class="intro col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
			                            	<?php
			                //Main heading
			                            		if ($main_heading || $secondary_heading) :
			                            			echo '<h1>';
			                            			echo ($main_heading) ?  '<span>' . $main_heading . '</span>': '';

			                            		echo '</h1>';
			                   	        		endif;
			                // intro text
			                   	        		if ($intro_copy || $full_copy) :

													echo ($intro_copy) ? '<p>' . $intro_copy . '</p>' : '';
												endif;
											?>
										</div>
								<?php endif; ?>
								<?php 
								// Quick nav panel
									if( $quick_nav ):

											echo '<div class="quick_nav intro col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">';
													echo ($quick_nav) ? '<div class="copy col-xs-12">' . $quick_nav . '</div>': '';
											echo '</div>';
							 		endif;
							 		?>
								<?php if( have_rows('panels') ):

										// echo '<div class="lp_panels ' . $equal_heights_class . '">';
									echo '<div class="trackwrap col-sm-12">'; /*allows track to be positioned without interfering with the odds and evens styles*/
										echo '<img class="tracklogo" src="/wp-content/themes/wilkinson-sword/assets/WilkinsonSword_Line_schwarz.png" alt="Wilkinson Sword logo - classic shave">';
										echo '<div class="track"></div>';
									echo '<div class="lp_panels">';
    									while ( have_rows('panels') ) : the_row();

    										// Feature panel
    										if( get_row_layout() == 'feature_panel' ):

												$copy 		= get_sub_field('copy');
												$header 	= $copy['heading'];
												$body 		= $copy['content'];
												$image 		= get_sub_field('image');
												$image_position 	= get_sub_field('image_positioning') ? 'background-position:' . get_sub_field('image_positioning') . ';' : '';
												if ($image){
													$copycols = 'col-xs-12 col-sm-6 col-lg-7';
													$panel_classes 		= 'panel feature-panel';
												} else {
													$copycols = 'col-xs-12';
													$panel_classes 		= 'panel feature-panel noimage';
													$noimage 	= ' noimage';
												}

												// Year settings
												$panel_date	= get_sub_field('year');
												$year 		= $panel_date['year_date'];
												$panel_id 	= preg_replace('/[\x00-\x1F\x7F]/u', '', $year);
												$year_color	= $panel_date['year_color'] ? 'style="background-color:' . $panel_date['year_color'] . ';"' : '';

												// Button settings
												$button_color 			= ( $button_default_colour ) ? 'style="color:' . $button_default_colour . '"' :  'style="color:' . $page_default_color . '"'  ;
												$button 				= get_sub_field('button');
												$button_type 			= $button['button_type'];
												$button_text 			= $button['button_text'];
												$button_searchterm 		= $button['search_term'];
												$button_cat 			= get_term_link( $button['button_category'] );
												$buttonlink_search		= ( $button_searchterm ) ? site_url() . '/?s=' . str_replace(' ', '+', $button_searchterm) . '&post_type=product' : '';
												$buttonlink_onsite 		= get_permalink( $button['button_link'] );
												$buttonlink_offsite		= $button['button_url'];
												// $button_link 			= ( $buttonlink_onsite ) ? $buttonlink_onsite : $buttonlink_offsite;


												if ( $button_type == 'url' && (isset( $buttonlink_onsite ) && !empty($buttonlink_onsite) ) ){
													$button_link = $buttonlink_onsite;
												} elseif ( $button_type == 'search' && (isset( $buttonlink_search ) && !empty($buttonlink_search ) )) {
													$button_link = $buttonlink_search;
												} elseif ( $button_type == 'link' && (isset( $buttonlink_offsite ) && !empty($buttonlink_offsite ) )) {
													$button_link = $buttonlink_offsite;
												} elseif ( $button_type == 'category' && (isset( $button_cat ) && !empty($button_cat ) )) {
													$button_link = $button_cat;
												}
													echo '<div class="' . $panel_classes . '">
													<div class="bookmark" id="' . $panel_id . '"></div>
															<div class="year_marker" ' . $year_color . '>' . $year . '</div>
															<div class="image col-xs-12 col-sm-6 col-lg-5" style="background-image:url(' . $image .  ');' . $image_position . '"><div class="pseudo-triangle"></div></div>
															<div class="copy ' . $copycols . $noimage . '">';
															echo '<div class="copy_container">';
																	echo ($header) ? '<h3 class="title">' . $header . '</h3>' : '';
																	echo ($body) ? $body : '' ;
																	echo ( $button_type != 'nolink' && $button_link ) ? '<p><a href="' . $button_link . '" class="button" ' . $button_color . ' >' . $button_text . '</a></p>' : '';
																echo '</div>';
															echo '</div>
														</div>';
									 		endif;

											// Content panel
    										if( get_row_layout() == 'content_panel' ):

												$copy 		= get_sub_field('copy');
												$header 	= get_sub_field('header');

												// Year settings
												$panel_date	= get_sub_field('year');
												$year 		= $panel_date['year_date'];
												$panel_id 	= preg_replace('/[\x00-\x1F\x7F]/u', '', strip_tags($year));
												$year_color	= $panel_date['year_color'] ? 'style="background-color:' . $panel_date['year_color'] . ';"' : '';

													echo '<div class="panel content-panel">';
														echo '<div class="bookmark" id="' . $panel_id . '"></div>';
														echo '<div class="year_marker" ' . $year_color . '>' . $year . '</div>';
															echo ($header) ? '<h3 class="title">' . $header . '</h3>' : '';
															echo ($copy) ? '<div class="copy col-xs-12">' . $copy . '</div>': '';
													echo '</div>';
									 		endif;

									 	endwhile;

									 	echo '</div>';
									 	echo '</div>';

									 endif;
									 echo '</div>';
									 echo '<div class="container">';
										 	 the_content();
										echo '</div>';


						}
					}


					?>
            </div>
    </main>
<?php get_footer(); ?>
