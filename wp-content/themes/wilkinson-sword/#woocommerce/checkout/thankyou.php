<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}
?>
<div class="woocommerce-order">

  <?php if ( $order ) : ?>

    <?php if ( $order->has_status( 'failed' ) ) : ?>

      <h1 class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Order Error', 'woocommerce' ); ?></h1>

      <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

      <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
        <a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
        <?php if ( is_user_logged_in() ) : ?>
          <a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My account', 'woocommerce' ); ?></a>
        <?php endif; ?>
      </p>

      <?php else : ?>
        <div class="row">
          <div class="col-sm-12 col-lg-6">
            <h1 class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( '<i class="fad fa-check-square theme-ws-ok"></i> Order Confirmed', 'wilkinson-sword' ), $order ); ?></h1>
            <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you for ordering with us.', 'wilkinson-sword' ), $order ); ?></p>
            <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">
              <li class="woocommerce-order-overview__order order">
                <?php _e( 'Transaction ID:', 'wilkinson-sword' ); ?>
                <strong><?php echo $order->get_order_number(); ?></strong>
              </li>
              <li class="woocommerce-order-overview__date date">
                <?php _e( 'Date:', 'woocommerce' ); ?>
                <strong><?php echo wc_format_datetime( $order->get_date_created() ); ?></strong>
              </li>
              <li class="woocommerce-order-overview__total total">
                <?php _e( 'Total:', 'woocommerce' ); ?>
                <strong><?php echo $order->get_formatted_order_total(); ?></strong>
              </li>
            </ul>
            <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( '<i class="fad fa-envelope theme-ws-ok"></i> We have emailed you an <strong>order confirmation.</strong>', 'wilkinson-sword' ), $order ); ?></p>
          </div>
          <?php $address_search = $order->get_shipping_address_1().','.$order->get_shipping_postcode(); ?>
          <div class="col-sm-12 col-lg-6">
            <?php
            switch (ws_return_lang_loc()) {
                case 'en-gb':
                  print '<h4>We will send your order here. See a problem? <a href="/en-gb/contact-us/">Get in touch</a>.</h4>';
                  break;
                case 'de-de':
                  print '<h4>Deine Bestellung wird an die angezeigte Adresse verschickt. Sollte etwas nicht richtig sein, <a href="/de-de/kontakt/">kontaktiere uns hier</a>.</h4>';
                  break;
              }
            ?>
           <iframe width="600" height="250" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php print $address_search; ?>&key=AIzaSyBNp1AUtcBVC1aWCJOFm4CUglsu-63WSn8"></iframe>  
          </div>
        </div>


      <?php endif; ?>

      <?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
      <?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

      <?php else : ?>

        <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

      <?php endif; ?>

    </div>