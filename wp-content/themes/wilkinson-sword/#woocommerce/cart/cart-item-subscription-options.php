<?php
/**
 * Cart Item Subscription Options Template.
 *
 * Override this template by copying it to 'yourtheme/woocommerce/cart/cart-item-subscription-options.php'.
 *
 * On occasion, this template file may need to be updated and you (the theme developer) will need to copy the new files to your theme to maintain compatibility.
 * We try to do this as little as possible, but it does happen.
 * When this occurs the version of the template file will be bumped and the readme will list any important changes.
 *
 * @version 2.1.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="wcsatt-options-wrapper" <?php echo count( $options ) === 1 ? 'style="display:none;"' : '' ?>>
	<select class="subs-dropdown" name="cart[<?php echo $cart_item_key; ?>][convert_to_sub]">
		<?php
		foreach ( $options as $option ) {
			
			if( strpos($option['description'], '</del>') ){
				$desc = explode('</del>', $option['description']);
				$option['description'] = $desc[1];
			}

			if( $option['description'] == 'none' )
				$option['description'] = 'One time purchase';
			?>
				<option data-custom_data="<?php echo esc_attr( json_encode( $option[ 'data' ] ) ); ?>" value="<?php echo esc_attr( $option[ 'value' ] ); ?>" <?php selected( $option[ 'selected' ], true, true ); ?>>
					<?php echo $option[ 'description' ]; ?>
				</option>
			<?php
		}
	?>
	</select>
</div>