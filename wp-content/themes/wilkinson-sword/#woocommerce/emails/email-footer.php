<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
																						</td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="kmDividerBlock" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                                <tbody class="kmDividerBlockOuter">
                                  <tr>
                                    <td class="kmDividerBlockInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding-top:10px;padding-bottom:10px;padding-left:18px;padding-right:18px;">
                                      <table class="kmDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;border-top-width:2px;border-top-style:solid;border-top-color:#4E4C4C;">
                                        <tbody>
                                          <tr>
                                            <td style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed"><span></span></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>

                              <table border="0" cellpadding="0" cellspacing="0" class="kmTextBlock" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                                <tbody class="kmTextBlockOuter">
                                  <tr>
                                    <td class="kmTextBlockInner" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;">
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" class="kmTextContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                                        <tbody>
                                          <tr>
                                            <td class="kmTextContent" valign="top" style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;color:#58595B;font-family:"Helvetica Neue", Arial;font-size:14px;line-height:1.3;letter-spacing:0;text-align:left;max-width:100%;word-wrap:break-word;padding-top:9px;padding-bottom:9px;padding-left:18px;padding-right:18px;'>
                                              <p style="margin:0;padding-bottom:0;text-align: center;"><?php echo esc_html__( "If you've got any queries, please don't hesitate to get in touch!", 'woocommerce' ) ; ?></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="kmButtonBlock" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;width: 100% !important; ">
                                <tbody class="kmButtonBlockOuter">
                                  <tr>
                                    <td valign="top" align="center" class="kmButtonBlockInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;min-width:60px;padding:18px;padding-top:9;padding-bottom:9;padding-left:18;padding-right:18;">
                                      <table border="0" cellpadding="0" cellspacing="0" width="" class="kmButtonContentContainer" style="border-collapse:separate;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;border-top-left-radius:5px;border-top-right-radius:5px;border-bottom-right-radius:5px;border-bottom-left-radius:5px;background-color:#999;background-color:#001B7C;border-radius:5px;">
                                        <tbody>
                                          <tr>
                                            <!--[if !mso]><!-->
                                            <td align="center" valign="middle" class="kmButtonContent" style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;color:white;font-family:"Helvetica Neue", Arial;font-size:16px;color:#FFFFFF;letter-spacing:0px;font-size:16px;font-family:"Century Gothic", Arial;font-weight:bold;'> <a class="kmButton" title="" href="<?php print get_site_url(); ?>/contact-us/" target="_self" style='word-wrap:break-word;max-width:100%;font-weight:normal;line-height:100%;text-align:center;text-decoration:underline;color:#29ABE3;font-family:"Helvetica Neue", Arial;font-size:16px;text-decoration:none; display: inline-block; padding-top:15px;padding-bottom:15px;font-size:16px;color:#FFFFFF;letter-spacing:0px;font-weight:bold;padding-left:15px;padding-right:15px;font-family:"Century Gothic", Arial; ;'><?php echo esc_html__( 'Contact Us', 'woocommerce' ) ; ?></a> </td>
                                            <!--<![endif]-->
                                            <!--[if mso]> <td align="center" valign="middle" class="kmButtonContent" style="padding:15px; padding-top:15px;padding-bottom:15px;font-size:16px;color:#FFFFFF;letter-spacing:0px;font-weight:bold;padding-left:15px;padding-right:15px;font-family:&quot;Century Gothic&quot;, Arial;"> <a class="kmButton" title="" href="https://productionws.wpengine.com/en-gb/contact-us/" target="_self" style="text-decoration:none;padding-top:15px;padding-bottom:15px;font-size:16px;color:#FFFFFF;letter-spacing:0px;font-weight:bold;padding-left:15px;padding-right:15px;font-family:&quot;Century Gothic&quot;, Arial;; margin-top:-15px;margin-bottom:-15px;margin-left:-15px;margin-right:-15px;">Contact Us</a> </td> <![endif]-->
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </table>
                <!--[if !mso]><!-->
              </div>
            </div>
            <!--<![endif]-->
            <!--[if mso]> </td> </tr> </tbody> </table> <![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
  </center>
</body>

</html>