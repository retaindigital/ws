<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title></title>
  <style type="text/css">
  @media only screen and (max-width:480px) {

    body,
    table,
    td,
    p,
    a,
    li,
    blockquote {
      -webkit-text-size-adjust: none !important
    }

    body {
      width: 100% !important;
      min-width: 100% !important
    }

    #bodyCell {
      padding: 10px !important
    }

    table.kmMobileHide {
      display: none !important
    }

    table.kmDesktopOnly,
    td.kmDesktopOnly,
    th.kmDesktopOnly,
    tr.kmDesktopOnly,
    td.kmDesktopWrapHeaderMobileNone {
      display: none !important
    }

    table.kmMobileOnly {
      display: table !important
    }

    tr.kmMobileOnly {
      display: table-row !important
    }

    td.kmMobileOnly,
    td.kmDesktopWrapHeader,
    th.kmMobileOnly {
      display: table-cell !important
    }

    tr.kmMobileNoAlign,
    table.kmMobileNoAlign {
      float: none !important;
      text-align: initial !important;
      vertical-align: middle !important;
      table-layout: fixed !important
    }

    tr.kmMobileCenterAlign {
      float: none !important;
      text-align: center !important;
      vertical-align: middle !important;
      table-layout: fixed !important
    }

    td.kmButtonCollection {
      padding-left: 9px !important;
      padding-right: 9px !important;
      padding-top: 9px !important;
      padding-bottom: 9px !important
    }

    td.kmMobileHeaderStackDesktopNone,
    img.kmMobileHeaderStackDesktopNone,
    td.kmMobileHeaderStack {
      display: block !important;
      margin-left: auto !important;
      margin-right: auto !important;
      padding-bottom: 9px !important;
      padding-right: 0 !important;
      padding-left: 0 !important
    }

    td.kmMobileWrapHeader,
    td.kmMobileWrapHeaderDesktopNone {
      display: inline-block !important
    }

    td.kmMobileHeaderSpacing {
      padding-right: 10px !important
    }

    td.kmMobileHeaderNoSpacing {
      padding-right: 0 !important
    }

    table.kmDesktopAutoWidth {
      width: inherit !important
    }

    table.kmMobileAutoWidth {
      width: 100% !important
    }

    table.kmTextContentContainer {
      width: 100% !important
    }

    table.kmBoxedTextContentContainer {
      width: 100% !important
    }

    td.kmImageContent {
      padding-left: 0 !important;
      padding-right: 0 !important
    }

    img.kmImage {
      width: 100% !important
    }

    td.kmMobileStretch {
      padding-left: 0 !important;
      padding-right: 0 !important
    }

    table.kmSplitContentLeftContentContainer,
    table.kmSplitContentRightContentContainer,
    table.kmColumnContainer,
    td.kmVerticalButtonBarContentOuter table.kmButtonBarContent,
    td.kmVerticalButtonCollectionContentOuter table.kmButtonCollectionContent,
    table.kmVerticalButton,
    table.kmVerticalButtonContent {
      width: 100% !important
    }

    td.kmButtonCollectionInner {
      padding-left: 9px !important;
      padding-right: 9px !important;
      padding-top: 9px !important;
      padding-bottom: 9px !important
    }

    td.kmVerticalButtonIconContent,
    td.kmVerticalButtonTextContent,
    td.kmVerticalButtonContentOuter {
      padding-left: 0 !important;
      padding-right: 0 !important;
      padding-bottom: 9px !important
    }

    table.kmSplitContentLeftContentContainer td.kmTextContent,
    table.kmSplitContentRightContentContainer td.kmTextContent,
    table.kmColumnContainer td.kmTextContent,
    table.kmSplitContentLeftContentContainer td.kmImageContent,
    table.kmSplitContentRightContentContainer td.kmImageContent {
      padding-top: 9px !important
    }

    td.rowContainer.kmFloatLeft,
    td.rowContainer.kmFloatLeft,
    td.rowContainer.kmFloatLeft.firstColumn,
    td.rowContainer.kmFloatLeft.firstColumn,
    td.rowContainer.kmFloatLeft.lastColumn,
    td.rowContainer.kmFloatLeft.lastColumn {
      float: left;
      clear: both;
      width: 100% !important
    }

    table.templateContainer,
    table.templateContainer.brandingContainer,
    div.templateContainer,
    div.templateContainer.brandingContainer,
    table.templateRow {
      max-width: 600px !important;
      width: 100% !important
    }

    h1 {
      font-size: 40px !important;
      line-height: 1.1 !important
    }

    h2 {
      font-size: 32px !important;
      line-height: 1.1 !important
    }

    h3 {
      font-size: 24px !important;
      line-height: 1.1 !important
    }

    h4 {
      font-size: 18px !important;
      line-height: 1.1 !important
    }

    td.kmTextContent {
      font-size: 14px !important;
      line-height: 1.3 !important
    }

    td.kmTextBlockInner td.kmTextContent {
      padding-right: 18px !important;
      padding-left: 18px !important
    }

    table.kmTableBlock.kmTableMobile td.kmTableBlockInner {
      padding-left: 9px !important;
      padding-right: 9px !important
    }

    table.kmTableBlock.kmTableMobile td.kmTableBlockInner .kmTextContent {
      font-size: 14px !important;
      line-height: 1.3 !important;
      padding-left: 4px !important;
      padding-right: 4px !important
    }
  }
  </style>
  <!--[if mso]> <style> .templateContainer { border: 0px none #aaaaaa; background-color: #ffffff; border-radius: 0px; } #brandingContainer { background-color: transparent !important; border: 0; } .templateContainerInner { padding: 0px; } </style> <![endif]-->
</head>

<body style="margin:0;padding:0;background-color:#eee">
  <center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" id="bodyTable" width="100%" data-upload-file-url="/ajax/email-editor/file/upload" data-upload-files-url="/ajax/email-editor/files/upload" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:auto;padding:0;background-color:#eee;height:100%;margin:0;width:100%">
      <tbody>
        <tr>
          <td align="center" id="bodyCell" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:auto;padding-top:50px;padding-left:20px;padding-bottom:20px;padding-right:20px;border-top:0;height:100%;margin:0;width:100%">
            <!--[if !mso]><!-->
            <div class="templateContainer" style="border:0 none #aaa;background-color:#fff;border-radius:0;display: table; width:600px">
              <div class="templateContainerInner" style="padding:0">
                <!--<![endif]-->
                <!--[if mso]> <table border="0" cellpadding="0" cellspacing="0" class="templateContainer" width="600" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;"> <tbody> <tr> <td class="templateContainerInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;"> <![endif]-->
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                  <tr>
                    <td align="center" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                      <table border="0" cellpadding="0" cellspacing="0" class="templateRow" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                        <tbody>
                          <tr>
                            <td class="rowContainer kmFloatLeft" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                              <table border="0" cellpadding="0" cellspacing="0" class="kmTextBlock" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                                <tbody class="kmTextBlockOuter">
                                  <tr>
                                    <td class="kmTextBlockInner" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;background-color:#EEEEEE;">
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" class="kmTextContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                                        <tbody>
                                          <tr>
                                            <td class="kmTextContent" valign="top" style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;color:#58595B;font-family:"Helvetica Neue", Arial;font-size:14px;line-height:1.3;letter-spacing:0;text-align:left;max-width:100%;word-wrap:break-word;color:#727272;padding-bottom:9px;text-align:center;padding-right:18px;padding-left:18px;padding-top:9px;'>
                                              <p style="margin:0;padding-bottom:0;font-size: 10px;"><!-- A quick preview of what this email contains. --></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                      <table border="0" cellpadding="0" cellspacing="0" class="templateRow" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                        <tbody>
                          <tr>
                            <td class="rowContainer kmFloatLeft" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                              <table border="0" cellpadding="0" cellspacing="0" class="kmImageBlock" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;min-width:100%">
                                <tbody class="kmImageBlockOuter">
                                  <tr>
                                    <td class="kmImageBlockInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding:9px;padding-top:20px;padding-right:9;padding-left:9;padding-bottom:10px;" valign="top">
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" class="kmImageContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;min-width:100%">
                                        <tbody>
                                          <tr>
                                            <td class="kmImageContent" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding:0;font-size:0;padding-top:0px;padding-bottom:0;padding-left:9px;padding-right:9px;text-align: center;"> <img align="center" alt="" class="kmImage" src="<?php print get_stylesheet_directory_uri(); ?>/img/ws-logo-email.jpg" width="180" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;width:100%;max-width:180px;padding:0;border-width:0;" /> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" width="100%" class="kmDividerBlock" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                                <tbody class="kmDividerBlockOuter">
                                  <tr>
                                    <td class="kmDividerBlockInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding-top:10px;padding-bottom:10px;padding-left:18px;padding-right:18px;">
                                      <table class="kmDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;border-top-width:2px;border-top-style:solid;border-top-color:#4E4C4C;">
                                        <tbody>
                                          <tr>
                                            <td style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed"><span></span></td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" class="kmImageBlock kmDesktopOnly" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;min-width:100%">
                                <tbody class="kmImageBlockOuter">
                                  <tr>
                                    <td class="kmImageBlockInner" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding:0px;padding-right:9;padding-left:9;" valign="top">
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" class="kmImageContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;min-width:100%">
                                        <tbody>
                                          <tr>
                                            <td class="kmImageContent" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding:0;font-size:0;padding:0;"> <img align="left" alt="" class="kmImage" src="<?php print get_stylesheet_directory_uri(); ?>/img/ws-header-email.jpg" width="582" style="border:0;height:auto;line-height:100%;outline:none;text-decoration:none;max-width:100%;padding-bottom:0;display:inline;vertical-align:top;font-size:12px;width:100%;margin-right:0;max-width:3155px;padding:0;border-width:0;" /> </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                              <table border="0" cellpadding="0" cellspacing="0" class="kmTextBlock" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                                <tbody class="kmTextBlockOuter">
                                  <tr>
                                    <td class="kmTextBlockInner" valign="top" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;padding-bottom:0px;">
                                      <table align="left" border="0" cellpadding="0" cellspacing="0" class="kmTextContentContainer" width="100%" style="border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed">
                                        <tbody>
                                          <tr>
                                            <td class="kmTextContent" valign="top" style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;table-layout:fixed;color:#58595B;font-family:"Helvetica Neue", Arial;font-size:14px;line-height:1.3;letter-spacing:0;text-align:left;max-width:100%;word-wrap:break-word;padding-top:9px;padding-bottom:9px;text-align:center;padding-left:18px;padding-right:18px;'>

                                            	<h3 style='color:#58595B;display:block;font-family:"Helvetica Neue", Arial;font-size:24px;font-style:normal;font-weight:bold;line-height:1.1;letter-spacing:0;margin:0;margin-bottom:12px;text-align:left;text-align: center;'><span style="color:#001458;"><span style="font-size:28px;"><?php echo $email_heading; ?></span></span></h3>