<?php
/**
 * Single-Product Subscription Options Template.
 *
 * Override this template by copying it to 'yourtheme/woocommerce/single-product/product-subscription-options.php'.
 *
 * On occasion, this template file may need to be updated and you (the theme developer) will need to copy the new files to your theme to maintain compatibility.
 * We try to do this as little as possible, but it does happen.
 * When this occurs the version of the template file will be bumped and the readme will list any important changes.
 *
 * @version 2.2.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="wcsatt-options-wrapper" <?php echo count( $options ) === 1 ? 'style="display:none;"' : '' ?>><?php

	if ( $prompt ) {
		echo $prompt;
	}

	?>
	<!-- <h3 class="accent-p1">Subscribe and Save!</h3> -->

	<select class="subs-dropdown" name="convert_to_sub_<?php echo absint( $product_id ); ?>">
		<?php
		foreach ( $options as $option ) {
			
			if( strpos($option['description'], '</del>') ){
				$desc = explode('</del>', $option['description']);
				$option['description'] = $desc[1];
			}

			if( $option['description'] == 'none' )
				$option['description'] = 'One time purchase';
			?>
				<option data-custom_data="<?php echo esc_attr( json_encode( $option[ 'data' ] ) ); ?>" value="<?php echo esc_attr( $option[ 'value' ] ); ?>" <?php checked( $option[ 'selected' ], true, true ); ?>>
					<?php echo $option[ 'description' ]; ?>
				</option>
			<?php
		}
	?>
	</select>
</div>
