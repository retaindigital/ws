module.exports = function(grunt) {
  grunt.initConfig({
    sprite:{
      all: {
        src: 'flags/*.png',
        // retinaSrcFilter: ['app/img/sprites/*@2x.png'],
        dest: 'img/sprite.png',
        // retinaDest: 'dest/img/sprite-2x.png',
        destCss: 'css/sprite.css',
        cssOpts: {
          cssSelector: function(item) {
            // If this is a hover sprite, name it as a hover one (e.g. 'home-hover' -> 'home:hover')
            if (item.name.indexOf('-hover') !== -1) {
              return '.icon-' + item.name.replace('-hover', ':hover');
            // Otherwise, use the name as the selector (e.g. 'home' -> 'home')
            } else {
              return '.icon-' + item.name;
            }
          }
        },
      }
    },
    sass: {
      dist: {
        files: {
          'css/style.css': 'sass/style.scss',
        },
      }
    },
    uglify: {
      my_target: {
        files: {
          'js/ws.min.js': ['js/ws.js']
        }
      }
    },
    cssmin: {
      target: {
        files: [{
          expand: true,
          cwd: 'css',
          src: ['*.css', '!*.min.css'],
          dest: 'css',
          ext: '.min.css'
        }]
      }
    },
    watch: {
      files:["sass/*", "js/ws.js"],
      // files:["sass/*"],
      tasks: ["sass", "cssmin", "uglify"]
      // tasks: ["sprite","sass", "cssmin", "uglify"]
    }
  });
  grunt.loadNpmTasks('grunt-spritesmith');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
};
