<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
# This answer
function endsWith($haystack, $needle) {
	return substr($haystack,-strlen($needle))===$needle;
}

function ws_header_metadata() {
	// WS-515
	switch (ws_return_lang_loc()) {
		case 'de-de':
			print '<meta name="p:domain_verify" content="b0d8db759126bf78a43607234a27eb40" />';
			break;
	}
}
add_action( 'wp_head', 'ws_header_metadata' );


add_action( 'woocommerce_single_product_summary', 'wilkinson_sword_woocommerce_template_single_price', 2 );
function wilkinson_sword_woocommerce_template_single_price(){
	global $product;
	if( $product->get_type() == 'variable' ){
		remove_action('woocommerce_single_product_summary','woocommerce_template_single_price',10  );
	}
}


/** Disable All WooCommerce  Styles and Scripts Except Shop Pages*/
add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_styles_scripts', 99 );
function dequeue_woocommerce_styles_scripts() {
	if ( function_exists( 'is_woocommerce' ) ) {
		if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
			# Styles
			wp_dequeue_style( 'woocommerce-general' );
			wp_dequeue_style( 'woocommerce-layout' );
			wp_dequeue_style( 'woocommerce-smallscreen' );
			wp_dequeue_style( 'woocommerce_frontend_styles' );
			wp_dequeue_style( 'woocommerce_fancybox_styles' );
			wp_dequeue_style( 'woocommerce_chosen_styles' );
			wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
			# Scripts
			wp_dequeue_script( 'wc_price_slider' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-add-to-cart' );
			wp_dequeue_script( 'wc-cart-fragments' );
			wp_dequeue_script( 'wc-checkout' );
			wp_dequeue_script( 'wc-add-to-cart-variation' );
			wp_dequeue_script( 'wc-single-product' );
			wp_dequeue_script( 'wc-cart' );
			wp_dequeue_script( 'wc-chosen' );
			wp_dequeue_script( 'woocommerce' );
			wp_dequeue_script( 'prettyPhoto' );
			wp_dequeue_script( 'prettyPhoto-init' );
			wp_dequeue_script( 'jquery-blockui' );
			wp_dequeue_script( 'jquery-placeholder' );
			wp_dequeue_script( 'fancybox' );
			wp_dequeue_script( 'jqueryui' );
		}
	}
}

// Loops Breadcrumbs on non woocommerce pages
add_filter( 'breadcrumb_trail_items', 'wilkinson_sword_breadcrumb_trail_items' );
function wilkinson_sword_breadcrumb_trail_items($items){
		// Loop the items in the BC
		foreach ($items as $key => $value) {
			// Explode by "
			$parts = explode('"', $value);

			// if we're linking to the home url
			if( $parts[1] == home_url() ){
				// add a trailing slash to the url
				$items[$key] = str_replace('"' . home_url() . '"', '"' . trailingslashit(home_url()) . '"', $value);
			}
		}
		// return items
		return $items;
}

// Needs to be UK only
// add_filter( 'woocommerce_checkout_fields' , 'wilkinson_sword_checkout_fields' );
function wilkinson_sword_checkout_fields( $fields ) {
	$fields['billing']['billing_phone']['description'] = __('we\'ll text you delivery updates', 'wilkinson-sword');
	return $fields;
}



// Limit Country to UK
add_filter( 'gform_pre_render_3', 'wilkinson_sword_limit_countries' );
add_filter( 'gform_pre_validation_3', 'wilkinson_sword_limit_countries' );
add_filter( 'gform_pre_submission_filter_3', 'wilkinson_sword_limit_countries' );
add_filter( 'gform_admin_pre_render_3', 'wilkinson_sword_limit_countries' );
function wilkinson_sword_limit_countries( $form ) {
  add_filter( 'gform_countries', function( $countries ) {
    return array( 'United Kingdom' );
  });
  return $form;
}

// Postcode Validation
add_filter("gform_field_validation_3_18", "wilkinsons_sword_uk_postcode_validation", 10, 4);
function wilkinsons_sword_uk_postcode_validation($result, $value, $form, $field){
    //address field will pass $value as an array with each of the elements as an item within the array, the key is the field id
    
	$postcode = $value["18.5"];
	
	//the default regex is valid both with and without the space in the middle, upper and lower case
	//to force the space, replace with the following:
	//'#^(GIR ?0AA|[A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]([0-9ABEHMNPRV-Y])?)|[0-9][A-HJKPS-UW]) [0-9][ABD-HJLNP-UW-Z]{2})$#'
	
	if(!(preg_match('#^(girGIR ?0aaAA|[a-pr-uwyzA-PR-UWYZ]([0-9]{1,2}|([a-hk-yA-HK-Y][0-9]([0-9abehmnprv-yABEHMNPRV-Y])?)|[0-9][a-hjkps-uwA-HJKPS-UW]) ?[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2})$#', $postcode))) {
		$result["is_valid"] = false;
		$result["message"] = "Please enter a valid UK postcode";
	}

    return $result;
}


function replace_mens_womens($data){

	// Set the cats to be changed
	switch (ws_return_lang_loc()) {
		case 'en-gb':
			$find_mens = 'product-category/mens/"';
			$replace_mens = 'mens/"';
			$find_womens = 'product-category/womens/"';
			$replace_womens = 'womens/"';
			break;
		case 'de-de':
			$find_mens = 'produktkategorie/maenner/"';
			$replace_mens = 'maenner/"';
			$find_womens = 'produktkategorie/frauen/"';
			$replace_womens = 'frauen/"';
			break;
	}

	$data = str_replace($find_mens, $replace_mens, $data);
	$data = str_replace($find_womens, $replace_womens, $data);

	return $data;		

}

// Replaces breadcrumb for men and women
add_filter( 'woocommerce_get_breadcrumb', 'wilkinson_sword_change_breadcrumb' );
function wilkinson_sword_change_breadcrumb( $crumbs ) {

	$find_mens = '';

	// Set the cats to be changed
	switch (ws_return_lang_loc()) {
		case 'en-gb':
			$find_mens = 'product-category/mens/';
			$replace_mens = 'mens/';
			$find_womens = 'product-category/womens/';
			$replace_womens = 'womens/';
			break;
		case 'de-de':
			$find_mens = 'produktkategorie/maenner/';
			$replace_mens = 'maenner/';
			$find_womens = 'produktkategorie/frauen/';
			$replace_womens = 'frauen/';
			break;
	}

	if( !empty($find_mens) ):

		// loop each crumb
		foreach ($crumbs as $key => $value) {

	    // Make sure the HOME url has a trailingslashit
			if($key == 0){
				$crumbs[$key][1] = trailingslashit($crumbs[$key][1]);
			}

	    	// check if the url ends with mens and replace it
			if( endsWith($value[1], $find_mens) == 1 ){
				$crumbs[$key] = str_replace($find_mens, $replace_mens, $crumbs[$key]);

	    	// check if the url ends with womens and replace it
			} else if( endsWith($value[1], $find_womens) == 1 ){
				$crumbs[$key] = str_replace($find_womens, $replace_womens, $crumbs[$key]);
			}

		}

	endif;

	return $crumbs;
}

// Fixes LEBE showing draft and hidden products
function wilkinson_sword_search_filter($query) {
	if ( $query->query_vars['post_type'] == 'product' && $query->query_vars['posts_per_page'] == '-1' ) {
		$query->set( 'post_status', 'publish' );
		$query->set( 'tax_query', array(
	    'relation' => 'OR',
	    array(
	      'taxonomy' => 'product_visibility',
	      'field'    => 'name',
	      'terms'    => 'exclude-from-search',
	      'operator' => 'NOT IN',
	    ),
	    array(
	      'taxonomy' => 'product_visibility',
	      'field'    => 'name',
	      'terms'    => 'exclude-from-search',
	      'operator' => '!=',
	    ),
	   )
		);
	}
}
add_action( 'pre_get_posts', 'wilkinson_sword_search_filter' );


// Disable the new Block Edtior
add_filter('use_block_editor_for_post_type', 'disable_block_editor');
function disable_block_editor( $use_block_editor ) {
	return false;
}

// Stop media getting slugs
add_filter( 'wp_unique_post_slug_is_bad_attachment_slug', '__return_true' );


// Load the child theme styles
if ( ! function_exists( 'child_theme_configurator_css' ) ):
	function child_theme_configurator_css() {
		wp_enqueue_style( 'wilkinson_sword', trailingslashit( get_stylesheet_directory_uri() ) . 'css/style.min.css', array(
			'owl-carousel',
			'flat-icons',
			'scrollbar',
		) );
		// WS Scripts
		wp_enqueue_script('ws-scripts',  get_stylesheet_directory_uri() . '/js/ws.min.js', array ( 'jquery' ), 1.2, true);
		// Twlya Chat Bot
		wp_enqueue_script('twyla', 'https://widget.chat.twyla.ai/twyla-widget.js', array (), 0.1, false);

		if (ws_return_lang_loc() == 'en-gb'){
			wp_enqueue_script('pingdom', '//rum-static.pingdom.net/pa-5eb29665146aea0015000028.js', array (), 0.1, false);
		}
	}
endif;
add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css', 20 );


/* Add Multisite lang-location to body class */ 
function wilkinson_sword_body_classes( $classes ) {
	$classes[] = ws_return_lang_loc();

	// add the WS_PAGE_TYPE class to the body
	if( get_post_meta(get_the_ID(), 'ws_page_type', true) != '' )
		$classes[] = get_post_meta(get_the_ID(), 'ws_page_type', true);

	return $classes;  
}
add_filter( 'body_class','wilkinson_sword_body_classes' );

// Adjust Twlya script
function wilkinson_sword_add_data_attribute($tag, $handle) {
	if ( 'twyla' !== $handle )
		return $tag;

	$lang_location = ws_return_lang_loc();

	switch ($lang_location) {
		case 'en-gb':
			$twyla_replacement = 'id="twyla-widget-script" src="https://widget.chat.twyla.ai/twyla-widget.js" data-hook-url="https://api.canvas.twyla.ai/widget-hook/edgewell/717B6r" data-api-key="0Iw2syb4nOJGmiM8OsJL0NVcmqs" data-primary-color="#000000" data-avatar="https://i.postimg.cc/C1hzfR9Z/Wilkins-Avatar.png" data-greeting-bubble-text="Hey sword fan, got a question?" data-greeting-button-label="Chat to Wilkins" src" src';
			break;
		case 'de-de':
			$twyla_replacement = 'id="twyla-widget-script" src="https://widget.chat.twyla.ai/twyla-widget.js" data-hook-url="https://api.canvas.twyla.ai/widget-hook/edgewell/X1onYr" data-api-key="plHOe2xLcSyHwjhcDsnqQfzuFTI" data-primary-color="#000000" data-language="de" data-avatar="https://assets.canvas.twyla.ai/Edgewell/Wilkins%20Avatar.png" data-title="Gerne stehe ich für deine Fragen zur Verfügung" data-greeting-bubble-text="Hast Du eine Frage?" data-greeting-button-label="Mit Wilkins chatten" src" src';
			$twyla_replacement = '';
			break;
	}

	return str_replace( ' src', $twyla_replacement, $tag );
}
add_filter('script_loader_tag', 'wilkinson_sword_add_data_attribute', 10, 2);


// Add JSON 
function wilkinson_sword_before_header_custom() {
	global $wp;
  // Check if we're on the home page or a contact page for ORG JSON
	if ( is_front_page() || strpos($wp->request, 'contact') == 0 ) {
		$org_json_template = file_get_contents(WP_PLUGIN_DIR.'/ws/json/organisation.inc');
		$logo_id = lebe_get_option( 'lebe_mobile_logo' );
		$logo_url = wp_get_attachment_image_url( $logo_id, 'full' );
		$org_json_template = str_replace('{{LOGO_URL}}', $logo_url, $org_json_template);
		print $org_json_template;
	}

	if ( is_single() ){
		$article_json_template = file_get_contents(WP_PLUGIN_DIR.'/ws/json/article.inc');

		$post_id = get_the_id();
		$post_URL = get_permalink( $post_id );
		$post_title = get_the_title();
		$post_image = get_the_post_thumbnail_url( $post_id );
		$post_published = get_the_date();
		$post_modified = get_the_modified_date();
		$post_author = the_author();
		$logo_id = lebe_get_option( 'lebe_mobile_logo' );
		$logo_url = wp_get_attachment_image_url( $logo_id, 'full' );
		$post_desc = get_the_excerpt();

    // Replace Data
		$article_json_template = str_replace('{{ART_URL}}', $post_URL, $article_json_template);
		$article_json_template = str_replace('{{ART_TITLE}}', $post_title, $article_json_template);
		$article_json_template = str_replace('{{ART_IMG}}', $post_image, $article_json_template);
		$article_json_template = str_replace('{{ART_DATE_PUB}}', $post_published, $article_json_template);
		$article_json_template = str_replace('{{ART_DATE_MOD}}', $post_modified, $article_json_template);
		$article_json_template = str_replace('{{ART_AUTHOR}}', $post_author, $article_json_template);
		$article_json_template = str_replace('{{ART_BRAND_LOGO}}', $logo_url, $article_json_template);
		$article_json_template = str_replace('{{ART_SHORT_DESC}}', $post_desc, $article_json_template);
		print $article_json_template;
	}
}
add_action( 'wilkinson_sword_before_header', 'wilkinson_sword_before_header_custom' );


// Enable unfiltered_html capability for admin and editors - allows WP Bakery
function wilkinson_sword_allow_unfiltered_html( $caps, $cap, $user_id ) {
	if ( 'unfiltered_html' === $cap && ( user_can( $user_id, 'administrator' ) || user_can( $user_id, 'editor' ) || user_can( $user_id, 'shop_manager' ) || user_can( $user_id, 'edgewell' ) ) ) {
		$caps = array( 'unfiltered_html' );
	}
	return $caps;
}
add_filter( 'map_meta_cap', 'wilkinson_sword_allow_unfiltered_html', 1, 3 );


// SHARETHIS
function wilkinson_sword_before_shop_loop(){
	if( !is_product() )
		get_template_part( 'template-parts/part', 'breadcrumb' );
}
add_action( 'lebe_before_shop_loop', 'wilkinson_sword_before_shop_loop', 9 );


// SHARETHIS
function wilkinson_sword_before_add_to_cart(){
	echo '<div class="sharethis-inline-share-buttons"></div>';
}
add_action( 'woocommerce_after_add_to_cart_button', 'wilkinson_sword_before_add_to_cart', 9 );

// Add Payment Method title to checkout
function wilkinson_sword_before_payment_methods(){
	echo '<h3 id="payment_method_heading">' . __('Payment Methods') . '</h3>';
}
add_action('woocommerce_review_order_before_payment', 'wilkinson_sword_before_payment_methods', 10, 2);


// Add 'Add to Basket' Anchor Link if product is in stock
function wilkinson_sword_before_single_product_summary(){
	global $product;
	if( $product->is_in_stock() )
		echo '<button class="add-to-cart-scroll hidden">Add To Basket</button>';
}
// add_action('woocommerce_single_product_summary', 'wilkinson_sword_before_single_product_summary', 9, 2);


// Change the default thumbnail gallery size
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
	return array(
		'width' => 300,
		'height' => 300,
		'crop' => 0,
	);
});

// Moves the cat desc to the bottom of the page
remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_taxonomy_archive_description', 100 );

// Remove sort by rating on PLP
function wilkinson_sword_woocommerce_catalog_orderby( $orderby ) {
	unset($orderby["rating"]);    
	return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "wilkinson_sword_woocommerce_catalog_orderby", 20 );

// Splits the product name in the mini cart
function wilkinson_sword_cart_item_name( $product_name, $product_data ) {
	if( !is_page( 'cart' ) && !is_page( 'warenkorb' ) ):
		$max = (int) 18;
	$string = $product_name;
	$in_pieces = array();
	while (strlen($string) > $max) {
			// location of last space in the first $max characters
		$substring = substr($string, 0, $max);
	    // pull from position 0 to $substring position
		$in_pieces[] = trim( substr( $substring, 0, strrpos($substring, ' ') ) );
	    // $string (haystack) now = haystack with out the first $substring characters
		$string = substr($string, strrpos($substring, ' '));
	    // if remaining string has no spaces AND has a string length
	    //  greater than $max, then this will loop infinitely!  So instead, just have to bail-out (boo!)
		if (strlen($string) > $max) break;
	}
		$in_pieces[] = trim($string); // final bits o' text
		$product_name = '';
		$count = count($in_pieces)-1;
		foreach ($in_pieces as $key => $value) {
			if( $count != $key)
				$product_name .= $value.'<br>';
			else
				$product_name .= $value;
		}
	endif; 
	return $product_name;
}
add_filter( 'woocommerce_cart_item_name', 'wilkinson_sword_cart_item_name', 10, 2 );

// Remove Woocommerce structured data
function remove_output_structured_data() {
	remove_action( 'wp_footer', array( WC()->structured_data, 'output_structured_data' ), 10 ); // This removes structured data from all frontend pages
	remove_action( 'woocommerce_email_order_details', array( WC()->structured_data, 'output_email_structured_data' ), 30 ); // This removes structured data from all Emails sent by WooCommerce
	// Remove Bazaarvoice schema
	remove_action('woocommerce_before_single_product', 'bv_schema_div_open', 10);
	remove_action('woocommerce_after_single_product', 'bv_schema_div_close', 25);
}
add_action( 'init', 'remove_output_structured_data' );

// Remove Yoast structured data
add_filter( 'wpseo_json_ld_output', '__return_false' );

// Age Verification

// Remove stripe apple pay etc. when 18+ required
add_action( 'woocommerce_before_cart', 'wilkinson_sword_age_verification_remove_stripe_payment_request_from_cart', 99 );
function wilkinson_sword_age_verification_remove_stripe_payment_request_from_cart() {
	// Loads the cart to check for a restricted item
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
	    $product = $cart_item['data'];
	    //Check product for 'age restricted' tag
	    if ( has_term( 'age restricted', 'product_tag', $product->id ) ) {
					remove_action( 'woocommerce_proceed_to_checkout', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_html' ), 1 );
					remove_action( 'woocommerce_proceed_to_checkout', array( WC_Stripe_Payment_Request::instance(), 'display_payment_request_button_separator_html' ), 2 );
	        break;
	    }
	}
}

add_action( 'woocommerce_review_order_before_submit', 'wilkinson_sword_age_verification_checkout_field', 9, 2); //Sits Below Terms and Conditions Checkbox and above estimated delivery date
function wilkinson_sword_age_verification_checkout_field( $checkout ) {
	$age_restricted_cart = false; //Set flag
	//Check the cart for age restricted items
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
	    $product = $cart_item['data'];
	    //Check product for 'age restricted' tag
	    if ( has_term( 'age restricted', 'product_tag', $product->id ) ) {
	        $age_restricted_cart = true;
	        break;
	    }
	}

	// Build label for checkbox
	$age_verification_label = sprintf(
		/* translators: 1: opening span tag 2: opening anchor tag */
		__('%1$sI confirm that I\'m over 18 years of age %2$sWhy\'s this?</a></span>', 'wilkinson-sword'),
		'<span class="woocommerce-terms-and-conditions-checkbox-text">',
		'<a href="/' . ws_return_lang_loc() . '/faqs/" class="age-restricted-faq-link" target="_blank">'
	);

	// Adds the 18+ tickbox
	if ( $age_restricted_cart ) {
	    echo '<div id="age-verification">';
	    woocommerce_form_field( 'age_verification', array(
	        'type'      	=> 'checkbox',
	        'label'     	=> $age_verification_label,
	        'required'		=> true,
	        'label_class'	=> array('woocommerce-form__label woocommerce-form__label-for-checkbox checkbox'),
	        'input_class'	=> array('input-checkbox', 'woocommerce-form__input', 'woocommerce-form__input-checkbox'),
	    ),  0 );
	    echo '</div>';
	}
}

//Check the box has been checked
add_action('woocommerce_checkout_process', 'wilkinson_sword_age_verification_check');
function wilkinson_sword_age_verification_check() {
	$age_restricted_cart = false; //Set flag
	// Loads the cart to check for a restricted item
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
	    $product = $cart_item['data'];
	    //Check product for 'age restricted' tag
	    if ( has_term( 'age restricted', 'product_tag', $product->id ) ) {
	        $age_restricted_cart = true;
	        break;
	    }
	}

	// Make sure we have 18+ tickbox
	if ( $age_restricted_cart == true && empty($_POST['age_verification']) ){
		wc_add_notice( __('A product in your cart is restricted to customers aged 18+, please confirm you are 18+.', 'wilkinson-sword'), 'error' );
	}
}

// Save to order meta
add_action( 'woocommerce_checkout_update_order_meta', 'wilkinson_sword_custom_checkout_field_update_order_meta', 10, 1 );
function wilkinson_sword_custom_checkout_field_update_order_meta( $order_id ) {
  if ( !empty($_POST['age_verification']) )
		update_post_meta( $order_id, 'age_verification', $_POST['age_verification'] );
}

// Display the custom field result on the order edit page
add_action( 'woocommerce_admin_order_data_after_billing_address', 'wilkinson_sword_display_custom_field_on_order_edit_pages', 10, 1 );
function wilkinson_sword_display_custom_field_on_order_edit_pages( $order ){
  $age_verification = get_post_meta( $order->get_id(), 'age_verification', true );
  if( $age_verification == 1 )
      echo '<p><strong>Confirmed over 18</strong></p>';
}
//End of age verfication

//force variable products to show price under pills
add_filter( 'woocommerce_show_variation_price', '__return_true');
