<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link       https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package    WordPress
 * @subpackage Lebe
 * @since      1.0
 * @version    1.0
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <script src="https://kit.fontawesome.com/139c2fea85.js"></script>
    <script type="text/javascript" data-channel="5c9a054a534c063054132033" src="//creator.zmags.com/channels.js"></script>

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Anton&display=swap" rel="stylesheet">

    <meta name="facebook-domain-verification" content="9q4a1utwlx473fnqydb7o3u1gmxhq0" />
    
    <!-- OneTrust Cookies Consent Notice start for www.wilkinsonsword.com -->
    <script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js" data-document-language="true" type="text/javascript" charset="UTF-8" data-domain-script="0ae9bbda-2b99-4fd2-bb40-50c950dc7867" ></script>
    <script type="text/javascript">
    function OptanonWrapper() { }
    </script>
    <!-- OneTrust Cookies Consent Notice end for www.wilkinsonsword.com -->
    
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5d25e256aca1ef0012990ac3&product=inline-share-buttons' async='async'></script>

	<?php wp_head(); ?>
</head>
<?php if ( is_category() || is_home()) {
    $extra_classes = 'blog';
}
if ( is_page_template('templates/landing-page.php') ){
    $settings               = get_field('settings');
    $extra_classes          = $settings['page_type'];
}
?>
<body <?php body_class($extra_classes ); ?> data-lang-loc="<?php print (ws_return_lang_loc() == 'top-level') ? '' : trailingslashit(ws_return_lang_loc()); ?>">

<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>

<?php
$enable_header_mobile = lebe_get_option( 'enable_header_mobile', false );
$wrapper_class        = '';
$menu_sticky          = lebe_get_option( 'enable_sticky_menu', 'none' );
$single_id            = lebe_get_single_page_id();
if ( $single_id > 0 ) {
	$enable_custom_header = false;
	$meta_data            = get_post_meta( $single_id, '_custom_metabox_theme_options', true );
	if ( $enable_custom_header ) {
		$menu_sticky = $meta_data['enable_sticky_menu'];
	}
}
if ( $menu_sticky == 'normal' ) {
	$wrapper_class = 'wrapper_menu-sticky-nomal';
} elseif ( $menu_sticky == 'smart' ) {
	$wrapper_class = ' wrapper_menu-sticky';
}
$sticky_info_w              = '';
$enable_info_product_single = lebe_get_option( 'enable_info_product_single', false );
if ( $enable_info_product_single ) {
	$sticky_info_w = 'sticky-info_single_wrap';
}
$lebe_blog_single_layout = lebe_get_option( 'lebe_blog_single_layout', 'layout1' );
$lebe_single_layout ='';
if ( is_single() ){
    if ( $lebe_blog_single_layout == 'layout1'){
        $lebe_single_layout = 'single-layout1';
    }else{
        $lebe_single_layout = 'single-layout2';
    }
}
$lebe_blog_style = lebe_get_option( 'blog-style', 'standard' );
$lebe_blog_style_layout = '';
if ( is_home() ) {
    if ($lebe_blog_style == 'modern') {
        $lebe_blog_style_layout = 'blog_style_modern';
    }
}
// WS blog styles 
if ( is_category() || is_home()) {
    $ws_blog_style = get_the_blog_style();
}
?>
<div id="page-wrapper"
     class="page-wrapper <?php echo esc_attr($wrapper_class); ?><?php echo esc_attr($sticky_info_w); ?> <?php echo esc_attr($lebe_single_layout); ?> <?php echo esc_attr($lebe_blog_style_layout); ?><?php echo esc_attr($ws_blog_style); ?>">
    <div class="body-overlay"></div>
    <div class="sidebar-canvas-overlay"></div>
	<?php if ( ! $enable_header_mobile || ( $enable_header_mobile && ! lebe_is_mobile() ) ) { ?>
        <div id="box-mobile-menu" class="box-mobile-menu full-height">
            <a href="javascript:void(0);" id="back-menu" class="back-menu"><i class="pe-7s-angle-left"></i></a>
            <span class="box-title"><?php echo esc_html__( 'Menu', 'lebe' ); ?></span>
            <a href="javascript:void(0);" class="close-menu"><i class="pe-7s-close"></i></a>
            <?php lebe_search_form(); ?>
            <div class="box-inner"></div>
        </div>
	<?php } ?>
	<?php lebe_get_header(); ?>
	<?php if ( is_search() && class_exists( 'WooCommerce' ) ) {
		get_template_part( 'template_parts/search', 'heading' );
	} ?>
	<?php
	if ( is_singular( 'product' ) ):
		do_action( 'lebe_product_toolbar' );
	endif;
	?>
