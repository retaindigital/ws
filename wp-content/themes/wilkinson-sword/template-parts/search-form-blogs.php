<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// $post_type                     = class_exists( 'WooCommerce' ) ? 'product' : '';
// $enable_instant_product_search = lebe_get_option( 'enable_instant_product_search', false );
// $product_search_place_holder   = $enable_instant_product_search ? esc_html__( 'Instant search ...', 'lebe' ) : esc_html__( 'Search ...', 'lebe' );
// $search_form_class             = 'instant-search';
// if ( ! $enable_instant_product_search ) {
//  $post_type = '';
// }

// if ( $post_type != 'product' ) {
//  $search_form_class .= ' instant-search-disabled';
// }
$obj            = get_queried_object();
$placeholder    = get_query_var('s');
$current_cat    = (is_category()) ? $obj->term_id : '' ;
$action_target  = esc_url( home_url( '/' ) ) . 'blog/';
$term           = ( get_query_var( 'blog_cat' ) ) ? get_category_by_slug( get_query_var( 'blog_cat' )) : get_queried_object();
?>
<div class="search-block blog_filter">
    <!-- <div class="search-icon"><span class="flaticon-magnifying-glass-browser icon"></span></div> -->
    <form autocomplete="off" method="get" class="search-form <?php echo esc_attr( $search_form_class ); ?>"
          action="<?php echo $action_target; ?>">
        <div class="filter_bar">
            <h4><?php esc_attr_e('I am interested in...','lebe'); ?></h4>

                      <?php 
            $cats_args = array(
                'exclude'           => 1,
                'hierarchical'      => 1,
                'hide_if_empty'     => true,
                'show_option_all'   => esc_attr__('All categories','lebe'),
                'value_field'       => 'slug',
                'name'              => 'blog_cat',
                'depth'             => 2,
                'selected'          => $term->slug,
                'order'             => 'ASC',
                'orderby'           => 'title'
        );
            // $blog_cats  = wp_list_categories($cats_args);
            wp_dropdown_categories($cats_args);
            ?>
           <script>
              document.getElementById('blog_cat').onchange = function(){
                // if value is category id
                if( this.value !== '-1' ){
                  this.form.submit()
                }
              }
            </script>

        </div>
        <div class="container">
            <div class="search-fields col-xs-12">
                <a class="cancel_filter" href="<?php echo get_site_url(); ?>/blog/"><?php esc_attr_e('Clear filters','lebe'); ?>&nbsp;<i class="far fa-times"></i></a>
                <div class="search-input">
                    <span class="reset-instant-search-wrap"></span>
                    <input type="search" class="search-field" placeholder="<?php if ($placeholder){ echo $placeholder; } else { echo esc_html__( 'Search ...', 'lebe' ); } ?>" value="" name="s">
                    <!-- <input type="hidden" name="post_type" value="post"> -->
                    <!-- <input type="hidden" name="category" value=cat_options> -->

                    <button type="submit" class="search-submit"><span class="flaticon-magnifying-glass-browser"></span>
                    </button>
                    <input type="hidden" name="lang" value="<?php echo get_locale(); ?>">
                    <div class="search-results-container search-results-croll">
                        <div class="search-results-container-inner">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<!-- <div id="header-widget-area" class="chw-widget-area widget-area" role="complementary">SEARCH
                <li id="search-4" class="widget widget_search"><form role="search" method="get" id="searchform" class="searchform" action="https://wilkinsonsword.lou.retain.digital/en-gb/">
    <div class="lebe-searchform"><label class="screen-reader-text" for="s">Search for:</label>
    <input type="text" value="" placeholder="Enter your keywords..." name="s" id="s">
    <button type="submit"><span class="flaticon-magnifying-glass-browser icons"></span></button>    
    </div>
    </form></li>
                </div> -->

