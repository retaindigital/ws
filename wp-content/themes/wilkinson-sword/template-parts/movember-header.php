	 	<?php
	 	// Dynamically find the page by url to determine the page links.
	 	$partnershaft 		= get_page_by_path('wir-sind-echte-mo-bros',OBJECT,'post');
	 	$movember 			= get_page_by_path('die-wissen-was-bei-maennern-geht',OBJECT,'post');
	 	$mannergesundheit	= get_page_by_path('das-geht-dich-an',OBJECT,'post');
	 	$faq 				= get_page_by_path('movember-faq',OBJECT,'post');

	 	echo  '<div class="movember-wrap">';
                   $mov_header_imgs  = array('/wp-content/themes/wilkinson-sword/assets/header1.jpg','/wp-content/themes/wilkinson-sword/assets/header2.jpg','/wp-content/themes/wilkinson-sword/assets/mogether-header.png');
                    echo '<div class="movember" style="background-image:url(' . $mov_header_imgs[0] . '), url(' . $mov_header_imgs[1] . '), url(' .  $mov_header_imgs[2] . ');"></div>';
                    echo '</div>';
                    // this is shown for less than 1500px wide
                    echo '<div class="movember-banner movember-mobile-banner">';
	                    echo '<div class="topstrip"><img src="/wp-content/themes/wilkinson-sword/assets/mo_team.png" alt="#moteam"/><span class="motext ">#mo\'gether forever</span></div>';
	                    echo '<div class="bottomstrip">';
		                    echo '<div class="nav">
		                    		<a href="' . get_the_permalink($partnershaft->ID) . '"><span></span>PARTNERSCHAFT</a>
		                    		<a href="' . get_the_permalink($movember->ID) . '"><span></span>Movember</a>
		                    		<a href="' . get_the_permalink($mannergesundheit->ID) . '"><span></span>Männergesundheit</a>
		                    		<a href="' . get_the_permalink($faq->ID) . '"><span></span>faq</a>';
							echo '</div>';
	                    echo '<div class="button"><a href="https://de.movember.com/donate/details?teamId=2237175">Jetzt&nbsp;spenden</a></div>';
	                echo '</div>';
	                echo '</div>';
                    // this is shown for more than 1500px wide
                    echo '<div class="movember-banner movember-desktop-banner">';
	                    echo '<div class="topstrip">
	                    		<div class="logo"><img src="/wp-content/themes/wilkinson-sword/assets/mo_team.png" alt="#moteam"/></div>
	                    		<div class="nav">
		                    		<a href="' . get_the_permalink($partnershaft->ID) . '"><span></span>PARTNERSCHAFT</a>
		                    		<a href="' . get_the_permalink($movember->ID) . '"><span></span>Movember</a>
		                    		<a href="' . get_the_permalink($mannergesundheit->ID) . '"><span></span>Männergesundheit</a>
		                    		<a href="' . get_the_permalink($faq->ID) . '"><span></span>faq</a>
								</div>
								<div class="button"><a href="https://de.movember.com/donate/details?teamId=2237175">Jetzt&nbsp;spenden</a></div>';
	                echo '</div>';
	                echo '</div>';
	         ?>