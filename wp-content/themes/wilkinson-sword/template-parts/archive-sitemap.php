
      <!-- <?php 
        $product_categories = get_terms($args = array(
          'taxonomy' => 'product_cat',
          'parent' => '0'
        ));
      ?>
      <?php foreach ( $product_categories  as $product_category ): ?>
        <ul>
          <li>
            <a href="<?php print get_term_link($product_category->term_id); ?>"><?php print $product_category->name; ?></a>
            <ul>
              <?php 
                $product_sub_categories = get_terms($args = array(
                  'taxonomy' => 'product_cat',
                  'parent' => $product_category->term_taxonomy_id
                ));
              ?>
              <?php foreach ($product_sub_categories as $product_sub_category): ?>
                <li><h3><a href="<?php print get_term_link($product_sub_category->term_id); ?>"><?php print $product_sub_category->name; ?></a></h3></li>
                <ul>
                  <?php
                    $sub_products = wc_get_products($args = array(
                      'catalog_visibility' => 'visible',
                      'limit' => '-1',
                      'category' => $product_sub_category->name
                    ));
                  ?>
                  <?php foreach ($sub_products as $sub_product): ?>
                    <li><a href="<?php print get_permalink($sub_product->get_id()); ?>"><?php print $sub_product->name; ?></a></li>
                  <?php endforeach; ?>
                </ul>


              <?php endforeach; ?>
            </ul>
            <ul>
              <?php 
                $products = wc_get_products($args = array(
                  'catalog_visibility' => 'visible',
                  'limit' => '-1',
                  'category' => $product_category->name
                ));
              ?>
              <?php foreach ($products as $product): ?>
                <li><a href="<?php print get_permalink($product->get_id()); ?>"><?php print $product->name; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </li>
        </ul>
      <?php endforeach; ?> -->