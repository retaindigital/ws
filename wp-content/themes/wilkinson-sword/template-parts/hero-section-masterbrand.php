<?php 
	$settings 				= get_field('settings');
	$page_type 				= $settings['page_type'];
	$header_style 			= $settings['header_style'] . '_style';
	$page_intro 			= get_field('page_intro');

	$page_title 			= $page_intro['main_heading'];
	$secondary_heading 		= $page_intro['secondary_heading'];
	$secondary_type 			= $secondary_heading['image_or_text'];
	$secondary_image 		= $secondary_heading['brand_logo'];
	$secondary_text 		= $secondary_heading['text'];
		// get output code for image
		if ( $secondary_type == 'img' ){
			list ($urlSmall,$urlMedium,$urlLarge) = get_image_sizes($secondary_image);
			$alt 			= get_post_meta($secondary_image, '_wp_attachment_image_alt', true);
			$alt_text		= ( $alt ) ? 'alt="' . $alt . '"' : '' ;
 			$cat_title 		= '<span class="sr-only sr-only-focusable">' . $alt . '</span><img class="landing_logo" src="' . $urlLarge . '" ' . $alt_text . '>';
		} elseif ( $secondary_type == 'text' ){
 			$cat_title 		='<span class="main_heading">' . $secondary_text . '</span>';
		}

		$intro_text			= $page_intro['intro_copy'];

	echo '<div class="container ' . $header_style . '">';
	echo '<div class="title_intro" style="text-align:center;">';
	echo $cat_title;
	echo '<div class="copy">' . $intro_text . '</div>';
	echo '</div>';
	echo '</div>';
	?>
