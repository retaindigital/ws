<?php

/*
 * Header banner (Hero section)
 LL 03/06/20 Apologies for this page it has got rather out of hand and needs a complete overhaul. Will be done in Picasso project. 
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( is_singular( 'post' ) ) {
	return;
}
// if (is_home()){
// 	echo '<h1>Home</h1>';
// }
if ( is_page() && ! ( is_front_page() || is_home() || is_page_template('templates/landing-page.php') ) ){ 
	$header_image_id 			= get_post_thumbnail_id( get_the_id() );
	$page_title           = '';
	$page_title = get_the_title( get_the_id() );

	if ($header_image_id) {
		$header_image 			 = wp_get_attachment_url( $header_image_id );
		$header_image_settings 	 = get_field('hero_image_positioning');
		$bg_positioning 		= ($header_image_settings) ? 'background-position:' . $header_image_settings . ';' : '';
		$hide_title 			= get_field('page_title_overlay');
		?>

			<div class="row">
			<?php	// Special movember header
				if ( is_page('movember')){
						get_template_part('template-parts/movember','header');

				} else { ?>
			    <div class="banner-page has_background hero-banner-page" style="background-image:url('<?php echo $header_image; ?>');<?php echo $bg_positioning; ?>">
			        <div class="content-banner">
  						<div class="container">
							<?php if ( ! is_front_page() &&  $hide_title != 1 && $header_image_id ) { ?>
			                    <h1 class="title-page page-title" style="color:#fff;"><?php echo esc_html( $page_title ); ?></h1>
							<?php } ?>
		           		 </div>
			        </div>
			    </div>
			<?php } ?> 

		    </div>

	<?php }

} else {


		$single_id            = lebe_get_single_page_id();
		$enable_custom_banner = false;
		$meta_data            = get_post_meta( $single_id, '_custom_metabox_theme_options', true );

		// CUSTOM WS page type
		$ws_page_type        	= get_post_meta(get_the_ID(), 'ws_page_type', true);


		$page_title           = '';
		if ( $single_id > 0 && !(is_home())) {
			$page_title = get_the_title( $single_id );
			if (is_home()){
				// $term = ( get_query_var( 'blog_cat' ) ) ? get_category_by_slug( get_query_var( 'blog_cat' )) : get_queried_object();
			}
		} else {
			if ( is_archive() || is_tag() || is_home()) {
				if ( is_category() ) {
					$page_title = single_cat_title( '', false );
				} else {
					$page_title = get_the_archive_title();
				}
				 // $term = ( get_query_var( 'blog_cat' ) ) ? get_category_by_slug( get_query_var( 'blog_cat' )) : get_queried_object();
			}
			if ( is_search() ) {
				if ( have_posts() ) {
					$page_title = sprintf( esc_html__( 'Search Results for: %s', 'lebe' ), get_search_query() );
				} else {
					$page_title = esc_html__( 'Nothing Found', 'lebe' );
				}
			}
		}

		$page_title = strip_tags( $page_title );

		if ( isset( $meta_data['enable_custom_banner'] ) ) {
			$enable_custom_banner = $meta_data['enable_custom_banner'];
			// Check request hero_section_type for custom banner
			if ( isset( $_GET['hero_section_type'] ) ) {
				$meta_data['hero_section_type'] = $_GET['hero_section_type'];
			}
		}

		if ( class_exists( 'WooCommerce' ) ) {
			if ( is_shop() ) {
				$enable_shop_mobile = lebe_get_option( 'enable_shop_mobile', true );
				if ( $enable_shop_mobile && lebe_is_mobile() ) {
					return;
				}
			}
			if ( is_product() || lebe_is_order_tracking_page() ) {
				return;
			}
			if ( is_woocommerce() ) {
				$page_title = woocommerce_page_title( false );
			}
		}

		$header_color = '';
		if ( $enable_custom_banner ) {
			$enable_header_mobile                = lebe_get_option( 'enable_header_mobile', false );
			$show_hero_section                   = true;
			
			// Check hero section on mobile is enabled or disabled
			if ( $enable_header_mobile && lebe_is_mobile() ) {
				$enable_hero_section_mobile = isset( $meta_data['show_hero_section_on_header_mobile'] ) ? $meta_data['show_hero_section_on_header_mobile'] : false;
				if ( ! $enable_hero_section_mobile ) {
					$show_hero_section = false;
				}
			}
			
			if ( ! $show_hero_section ) {
				return;
			}
			switch ( $meta_data['hero_section_type'] ) {
				case 'rev_background':
					if ( $meta_data['lebe_metabox_header_rev_slide'] != '' && shortcode_exists( 'rev_slider' ) ) {
						?>
		                <div class="slider-rev-wrap">
							<?php echo do_shortcode( '[rev_slider alias="' . esc_attr( $meta_data['lebe_metabox_header_rev_slide'] ) . '"][/rev_slider]' ); ?>
		                </div>
						<?php
					}
					break;
				case 'has_background':
				case 'no_background' :
					$page_banner_type 			= $meta_data['hero_section_type'];
					$page_img_banner             = $meta_data['bg_banner_page'];
					$lebe_page_heading_height    = $meta_data['page_height_banner'];
					$lebe_page_margin_top        = $meta_data['page_margin_top'];
					$lebe_page_margin_bottom     = $meta_data['page_margin_bottom'];
					$lebe_page_banner_breadcrumb = $meta_data['page_banner_breadcrumb'];
					$is_banner_full_width        = $meta_data['page_banner_full_width'];
					$css                         = '';
					if ( $page_banner_type == 'has_background' ) {
						if ( ( $page_img_banner['image'] ) !== '' ) {
							$css .= 'background-image:  url("' . esc_url( $page_img_banner['image'] ) . '");';
							$css .= 'background-repeat: ' . esc_attr( $page_img_banner['repeat'] ) . ';';
							$css .= 'background-position:   ' . esc_attr( $page_img_banner['position'] ) . ';';
							$css .= 'background-attachment: ' . esc_attr( $page_img_banner['attachment'] ) . ';';
							$css .= 'background-size:   ' . esc_attr( $page_img_banner['size'] ) . ';';
							$css .= 'background-color:  ' . esc_attr( $page_img_banner['color'] ) . ';';
						} else {
							$css .= 'background-image:  url("' . get_template_directory_uri() . '/assets/images/bn_blog.jpg' . '");';
							$css .= 'background-size: cover;';
							$css .= 'background-attachment: scroll;';
							$css .= 'background-repeat: no-repeat;';
							$css .= 'background-position: center center;';
							$css .= 'background-color: #f2f2f2;';
						}
						
						$header_color = $meta_data['colortext_banner_page'];
					}
					$css .= 'min-height:' . esc_attr( $lebe_page_heading_height ) . 'px;';
					$css .= 'margin-top:' . esc_attr( $lebe_page_margin_top ) . 'px;';
					$css .= 'margin-bottom:' . esc_attr( $lebe_page_margin_bottom ) . 'px;';
					
					if ( ! $is_banner_full_width ) { ?>
		                <div class="container">
		                <div class="row">
					<?php } ?>
					<!-- Showing this -->
		            <div class="rev_slider banner-page <?php echo esc_attr( $page_banner_type ); ?>"
		                 style='<?php echo esc_attr( $css ); ?>'>
		                 <?php //var_dump($css); ?>
		                <div class="content-banner" <?php if ( $page_banner_type == 'has_background' ) {
							echo 'style="color: ' . esc_attr( $header_color ) . ';"';
						} ?>>
		                    <div class="container">
								<?php if ( ! is_front_page() && $ws_page_type != 'brand-landing' ) { ?>

		                            <h1 class="title-page page-title" <?php if ( $page_banner_type == 'has_background' ) {
										echo 'style="color: ' . esc_attr( $header_color ) . ';"';
									} ?>><?php echo esc_html( $page_title ); ?></h1>

								<?php } ?>
								<?php if ( ! is_front_page() && $lebe_page_banner_breadcrumb ) {
									// get_template_part( 'template-parts/part', 'breadcrumb' );
								}; ?>
		                    </div>
		                </div>
		            </div>
					<?php
					if ( ! $is_banner_full_width ) { ?>
		                </div>
		                </div>
					<?php }
					break;
				case 'disable':
					break;
				default:
					break;
			}
		} else {

			$default_page_banner_height = 420;
			if ( is_front_page() && is_home() ) {
				$default_page_banner_height = 40;
			}
			
			$page_banner_type     = lebe_get_option( 'page_banner_type', 'no_background' );
			$page_banner_image    = lebe_get_option( 'page_banner_image' );
			$is_banner_full_width = lebe_get_option( 'page_banner_full_width', true );
			$page_banner_height   = lebe_get_option( 'page_height_banner', $default_page_banner_height );
			$page_margin_top      = lebe_get_option( 'page_margin_top', 0 );
			$page_margin_bottom   = lebe_get_option( 'page_margin_bottom', 0 );
			$header_color         = lebe_get_option( 'colortext_banner_page' );
			
			if ( class_exists( 'WooCommerce' ) ) {
				if ( is_shop() || is_product_category() || is_product_tag() ) {
					$page_banner_type     = lebe_get_option( 'shop_banner_type', 'no_background' );
					$page_banner_image    = lebe_get_option( 'shop_banner_image' );
					$is_banner_full_width = true;
					$page_banner_height   = lebe_get_option( 'shop_banner_height', 420 );
					$page_margin_top      = lebe_get_option( 'shop_margin_top', 0 );
					$page_margin_bottom   = lebe_get_option( 'shop_margin_bottom', 0 );
					$header_color         = lebe_get_option( 'colortext_shop_page' );
				}
			}
			$css = '';
			
			if ( $page_banner_type == 'has_background' ) {
				if ( ( $page_banner_image['image'] ) !== '' ) {

				  // Get the Category array
					$cat = get_term_by( 'id', get_queried_object_id(), 'product_cat');

					// Set default banner image (set in the theme options)
					$header_image_url = $page_banner_image['image'];

					// Check for specific category banner
					//First check if it is a post archive and if it has an image.
					if (is_category() || is_home()){
							// var_dump($term);
							// if ( $term->parent > 0 ) {
							//  	$term = get_term_by('id', $term->parent, 'category');
							//  }
							//  $header_image = get_field( 'hero_image',$term );
							//  $header_image_id = $header_image['ID'];
							//  $page_banner_image = array('position'=>'center','repeat'=>'no-repeat','size'=>'cover');
							//  $blog_style = get_field('blog_style',$term);
							//  if (get_field('display_title',$term) == 'text'){
							//  	$title = get_field('title_text',$term);
							//  	$cat_title = '<h1>' . $title . '</h1>';
							//  } else {
							//  	$title_graphic_id = get_field('title_image',$term);
							//  	$cat_title = wp_get_attachment_image($title_graphic_id,'large');
							//  }
							//  $intro_text = get_field('intro_text',$term);

					} elseif ( is_page_template('templates/landing-page.php')) {
								$header_image_id = get_post_thumbnail_id( get_the_id() );
								$page_banner_image = array('position'=>'center','repeat'=>'no-repeat','size'=>'cover');
								$page_banner_height 	= '300';
					}
					else {
						$header_image_id = get_woocommerce_term_meta( get_queried_object_id(), 'thumbnail_id', true );
					}
		      // If we have one, use it
		      if( $header_image_id > 0 ){
		     		$header_image_url = wp_get_attachment_url( $header_image_id );
		     	// if we don't, check the parent(s)
		      } else if ( $cat->parent > 0) {
		      	$cat_parent_id = $cat->parent;

		      	// Backup in case we get stuck
		      	$i = 1;

		       	// loop parents
		      	while ( $cat_parent_id > 0 ) {
		      		
		      		// backup break
		      		if($i == 10)
		      			break;

		      		// check for an image
		      		$parent_header_image_id = get_woocommerce_term_meta( $cat_parent_id, 'thumbnail_id', true );
		      		// If we don't have one, get the next parent
		      		if( $parent_header_image_id == 0 ){
						$cat_parent = get_term_by( 'id',  $cat->parent, 'product_cat');
		      			$cat_parent_id = $cat_parent->parent;
		      		// if we have one, use it
		      		} else{
		      			$cat_parent_id = 0;
						$header_image_url = wp_get_attachment_url( $parent_header_image_id );
		      		}
		      		$i++;
		      	}
		      }
// blog section
		     
if ( is_category() || is_home() || is_page_template('templates/landing-page.php')) {
		$settings 				= get_field('settings');
		$page_type 				= $settings['page_type'];
    // $term = ( get_query_var( 'blog_cat' ) ) ? get_category_by_slug( get_query_var( 'blog_cat' )) : get_queried_object();
	 if (is_page_template('templates/landing-page.php') || $page_type == 'female masterbrand'){

		$header_image_url 	= wp_get_attachment_image_url(get_post_thumbnail_id(),1440);
		$header_image_id 	= get_post_thumbnail_id();
		$page_intro 		= get_field('page_intro');

		$page_title 		= $page_intro['main_heading'];
		$secondary_heading 	= $page_intro['secondary_heading'];
		$secondary_type 	= $secondary_heading['image_or_text'];
		$secondary_image 	= $secondary_heading['brand_logo'];
		$secondary_text 	= $secondary_heading['text'];
		// get output code for image
		if ( $secondary_type == 'img' ){
			list ($urlSmall,$urlMedium,$urlLarge) = get_image_sizes($secondary_image);
			$alt 			= get_post_meta($secondary_image, '_wp_attachment_image_alt', true);
			$alt_text		= ( $alt ) ? 'alt="' . $alt . '"' : '' ;
 			$cat_title 		= '<span class="sr-only sr-only-focusable">' . $alt . '</span><img class="landing_logo" src="' . $urlLarge . '" ' . $alt_text . '>';
		} elseif ( $secondary_type == 'text' ){
 			$cat_title 		='<span class="main_heading">' . $secondary_text . '</span>';
		}

		$intro_text			= $page_intro['intro_copy'];

	} else {
		// get values from the term associated with the page or category
	    if ( is_home() ){
			$term = ( get_query_var( 'blog_cat' ) ) ? get_category_by_slug( get_query_var( 'blog_cat' )) : get_term_by('id', '1', 'category');
		} else {
			$term = get_queried_object();
		}

	    if ( $term->parent > 0 ) {
			 	$term = get_term_by('id', $term->parent, 'category');
		}
		$header_image = get_field( 'hero_image','category_' . $term->term_id );
		$header_image_id = $header_image['ID'];
		$header_image_url 	= $header_image['url'];
		$blog_style = get_field('blog_style','category_' . $term->term_id);
		 if (get_field('display_title','category_' . $term->term_id) == 'text'){
		 	$titleoverride = get_field('title_text','category_' . $term->term_id);
		 	$title 			= ($titleoverride) ? $titleoverride : $term->name ; 
		 	$cat_title = '<h1>' . $title . '</h1>';
		 } else {
		 	$title_graphic_id = get_field('title_image','category_' . $term->term_id);
		 	$cat_title = wp_get_attachment_image($title_graphic_id,'large');
		 }
		 // $intro_text = get_field('intro_text','category_' . $term->term_id);
		 $intro_text = category_description($term->term_id);
		}
		// create variables from the values

		$page_banner_image = array('position'=>'center','repeat'=>'no-repeat','size'=>'cover');
	}

					$css .= 'background-image:  url("' . esc_url( $header_image_url ) . '");';
					$css .= 'background-repeat: ' . esc_attr( $page_banner_image['repeat'] ) . ';';
					$css .= 'background-position:   ' . esc_attr( $page_banner_image['position'] ) . ';';
					$css .= 'background-attachment: ' . esc_attr( $page_banner_image['attachment'] ) . ';';
					$css .= 'background-size:   ' . esc_attr( $page_banner_image['size'] ) . ';';
					$css .= 'background-color:  ' . esc_attr( $page_banner_image['color'] ) . ';';
				} else {
					$css .= 'background-image:  url("' . get_template_directory_uri() . '/assets/images/bn_blog.jpg' . '");';
					$css .= 'background-size: cover;';
					$css .= 'background-attachment: scroll;';
					$css .= 'background-repeat: no-repeat;';
					$css .= 'background-position: center center;';
					$css .= 'background-color: #f2f2f2;';
				}
				
			}
			$css .= 'min-height:' . intval( $page_banner_height ) . 'px;';
			$css .= 'margin-top:' . intval( $page_margin_top ) . 'px;';
			$css .= 'margin-bottom:' . intval( $page_margin_bottom ) . 'px;';
			if ( ! $is_banner_full_width ) { ?>
		        <div class="container">
		        <div class="row">
			<?php } ?>
				<?php	// Special movember header
			if ( is_category('movember') || ( is_page('movember'))){
					get_template_part('template-parts/movember','header');

			} else { ?>
		    <div class="banner-page hero-banner-page <?php echo esc_attr( $page_banner_type ); echo ' ' . $blog_style; ?>"
		         style='<?php echo esc_attr( $css ); ?>'>
		         <?php //echo $css; ?>
		        <div class="content-banner" <?php if ( $page_banner_type == 'has_background' ) {
					echo 'style="color: ' . esc_attr( $header_color ) . ';"';
				} ?>>
		            <div class="container">

		            	<?php echo $page_img_banner['image']; ?>
						<?php if ( ! is_front_page() && $ws_page_type != 'brand-landing' ) { ?>
		                    <h1 class="title-page page-title" <?php if ( $page_banner_type == 'has_background' ) {
								echo 'style="color: ' . esc_attr( $header_color ) . ';"';
							} ?>><?php echo esc_html( $page_title ); ?></h1>
						<?php } ?>
		           
			           <?php if ($blog_style || $page_type == 'female masterbrand'){
				           		echo '<div class="title_intro">';
				           		echo $cat_title;
				           		echo '<div class="copy">' . $intro_text . '</div>';
				           		echo '</div>';
			           		} 
			           ?>
		            </div>
		        </div>
		    </div>
		<?php } ?>
			<?php
			if ( ! $is_banner_full_width ) { ?>
		        </div>
		        </div>
			<?php }
		}

}