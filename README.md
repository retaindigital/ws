Patches for plugins

woo_subs_plural_patch: 
This adds support for plural translations to a string in the woocommerce subscriptions plugin, 
the following translation text is required in the .po file for translations to work (german translations in this case)

#: includes/class-wc-subscriptions-product.php:370
msgid "every %s"
msgid_plural "every %s"
msgstr[0] "jeden %s"
msgstr[1] "alle %s"

